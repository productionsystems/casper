#!/bin/bash

app_path=/Applications/Microsoft\ Office\ 2011/Microsoft\ Outlook.app

if [[ $app_path ]]; then
	Version=`defaults read /Applications/Microsoft\ Office\ 2011/Microsoft\ Outlook.app/Contents/version.plist CFBundleVersion`
fi

if [[ -e $app_path ]]; then
	last_used=`mdls "$app_path" | grep kMDItemLastUsedDate | awk '{ $1 = $1 } { print }' | cut -c 23-41`


echo "<result>Office 2011 Installed V$Version | Last used $last_used</result>"

fi


