#!/bin/bash
bud='/usr/libexec/Plistbuddy'
plist=$HOME'/Library/Preferences/com.apple.sidebarlists.plist'

servers=('smb://hgpshared'
'smb://hgsl-svr01'
'smb://hgsl-svr02'
'smb://sagm-svr01'
'smb://jwkl-svr01'
'smb://grhl-svr01.hogarthww.prv/'
'smb://HGSL-RIP003')

killall cfprefsd
echo "Setting servers for $plist"
echo "Removing previous entries..."
${bud} -c "Delete favoriteservers" ${plist}

echo "Creating new list..."
${bud} -c "Add favoriteservers:Controller string CustomListItems" ${plist}
${bud} -c "Add favoriteservers:CustomListItems array" ${plist}

for i in "${!servers[@]}"
do
    echo "Adding to Favorite Servers: ${servers[$i]}..."
    ${bud} -c "Add favoriteservers:CustomListItems:$i:Name string ${servers[$i]}" ${plist}
    ${bud} -c "Add favoriteservers:CustomListItems:$i:URL string ${servers[$i]}" ${plist}
done

echo "Finalizing settings..."
killall cfprefsd
defaults read ${plist} favoriteservers > /dev/null