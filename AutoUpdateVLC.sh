#!/bin/bash

set -x

logfile="/Library/Logs/VLCInstallScript.log"
vlc_version=`/usr/bin/curl --silent http://mirror.wdc1.us.leaseweb.net/videolan/vlc/last/macosx/ | grep vlc- | cut -d \" -f 2 | awk '{printf("%s",$0);}' | cut -d . -f 1-6 | cut -c 4-`
url="http://mirror.wdc1.us.leaseweb.net/videolan/vlc/last/macosx/"$vlc_version""
volname=`echo "$vlc_version" | cut -c -9`
dmgfile="$vlc_version"

# Are we running on Intel?
if [ '`/usr/bin/uname -p`'="i386" -o '`/usr/bin/uname -p`'="x86_64" ]; then
		/bin/echo "--" >> ${logfile}
		/bin/echo "`date`: Downloading latest version." >> ${logfile}
		/usr/bin/curl -s -o /tmp/"${dmgfile}" "${url}"
		/bin/echo "`date`: Mounting installer disk image." >> ${logfile}
		/usr/bin/hdiutil attach /tmp/${dmgfile} -nobrowse -quiet
		/bin/echo "`date`: Installing..." >> ${logfile}
		ditto -rsrc "/Volumes/${volname}/VLC.app" "/Applications/VLC.app"
		/bin/sleep 10
		/bin/echo "`date`: Unmounting installer disk image." >> ${logfile}
		/usr/bin/hdiutil detach $(/bin/df | /usr/bin/grep "${volname}" | awk '{print $1}') -quiet
		/bin/sleep 10
		/bin/echo "`date`: Deleting disk image." >> ${logfile}
		/bin/rm /tmp/${dmgfile}
else
	/bin/echo "`date`: ERROR: This script is for Intel Macs only." >> ${logfile}
fi

exit 0