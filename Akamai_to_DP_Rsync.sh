#!/bin/bash

#This will copy all package data from Akamai to the local DP
#This should run out of hours via cron - 00 1 * * * /home/cssadmin/Akamai_to_DP_Rsync.sh
#Ensure jamdpro_netstorage_key is copied over

rsync -rltgoDuv -e "ssh -2 -i /root/.ssh/jamfpro_netstorage_key" --delete-excluded sshacs@jamfpro.upload.akamai.com:/596787/  /opt/hogarth/JamfProDistributionGSWS/Packages/ > /var/log/Akamai"_rsync-Log_"$(date +%Y%m%d).txt

echo "Rsync Akamai to Singapore DP Completed" | mutt -a /var/log/Akamai"_rsync-Log_"$(date +%Y%m%d).txt -s "Rsync Akamai to Singapore DP Completed" -- jason.flory@hogarthww.com, 
#frank.paternoster@hogarthww.com

#Find logs older than 5 days and delete
find /var/log/Akamai* -mtime +1 -exec rm {} \;

#exit 0