#!/bin/bash

# list of peripherals might need the api to push to the jss

jamf_binary="/usr/local/jamf/bin/jamf"


cat /dev/null > /tmp/currentPeripherals.xml
cat /dev/null > /tmp/peripherals.xml
cat /dev/null > /tmp/PeripheralsList.txt
cat /dev/null > /tmp/currentPeripherals.xml

JSSserver=https://10.252.32.155:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiuser # this user requires TBC access privs in API settings on JSS 
JSSapipw=PnZ-Nv5-E2P-CNR

tmpPeripheralsFile="/tmp/PeripheralsList.txt"

curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/peripheraltypes

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

#Get NB ID's from xml
Peripherals=`cat /tmp/all.xml | grep "<name>" | sort -t ">" -k2 -g | sed "s/^[ \t]*//" | sed 's*<name>**g' | sed 's*</name>**g' | sed '/13/d' | sed "s/$/,/g"`

IFS=$'\n'

#Iterate through XML array
for i in $Peripherals; do
    PeripheralsArray+=($i)
    echo $i >> "$tmpPeripheralsFile"
    chmod 777 "$tmpPeripheralsFile"
done

Peripherals=`/usr/bin/osascript <<EOT
    tell application "System Events"
    with timeout of 43200 seconds
    activate
--  Create an empty list called PeripheralsList
    set PeripheralsList to {}

--  Populate list with contents read from a file
    set PeripheralsFile to paragraphs of (read POSIX file "$tmpPeripheralsFile")

--  Iterate through each line in file to add to PeripheralsList
    repeat with i in PeripheralsFile
        if length of i is greater than 0 then
            copy i to the end of PeripheralsList
        end if
    end repeat

--  For testing to make sure the right number of items are counted in the list
--  display dialog count of PeripheralsList
    choose from list PeripheralsList with title "Peripherals List" with prompt "Please select all Peripherals to associate with this computer, hold command (⌘) to select multiple:" with multiple selections allowed as text
    end timeout
    end tell
EOT`

#Create file with results on each line
echo $Peripherals | tr "," "\n" > /tmp/peripherals.xml


# Testing api PUT
serialNumber=`/usr/sbin/system_profiler SPHardwareDataType | awk '/Serial/ { print $NF }'`
var2="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><peripheral_type><name>Audio Suite Equipment</name></peripheral_type>"
curl -sS -k -i --user ${JSSapiuser}:${JSSapipw} -X PUT -H "Content-Type: text/xml" -d "${var2}" "${JSSserver}/JSSResource/computers/serialnumber/${serialNumber}"


### Get Peripherals and send them to the jss

if [ `cat /tmp/peripherals.xml | grep "Audio Suite Equipment"` ]; then
	var1="<peripheral_type><id>36</id><name>Audio Suite Equipment</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var1" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "External Hard Disk"` ]; then
	var2="<peripheral_type><id>43</id><name>External Hard Disk</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var2" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "Graphics tablet"` ]; then
	var3="<peripheral_type><id>39</id><name>Graphics tablet</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var3" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "Headphone"` ]; then
	var4="<peripheral_type><id>27</id><name>Headphone</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var4" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "IP/Desk phones"` ]; then
	var5="<peripheral_type><id>50</id><name>IP/Desk phones</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var5" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "iPad"` ]; then
	var6="<peripheral_type><id>52</id><name>iPad</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var6" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "Magic Mouse"` ]; then
	var7="<peripheral_type><id>31</id><name>Magic Mouse</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var7" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "Mobile Device"` ]; then
	var8="<peripheral_type><id>57</id><name>Mobile Device</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var8" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi
	
if [ `cat /tmp/peripherals.xml | grep "RSA KeyFOB"` ]; then
	var9="<peripheral_type><id>17</id><name>RSA KeyFOB</name></peripheral_type>"
	curl -sS -k -i -u "$JSSapiuser":"$JSSapipw" -X PUT -H "Content-Type: text/xml" -d "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>$var9" "$JSSserver"/JSSResource/computers/serialnumber/$serialNumber
fi

