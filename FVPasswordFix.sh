#!/bin/bash

set -x

#Filevault keychain not getting updated during password change with Mobile Accounts.
#This must be run from within the locadmin account
#Dialog to request username to fix

jamf_binary="/usr/local/jamf/bin/jamf"
AdminPW="K0ntro11er!"

#Username to fix
UserToFix=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter the shortname of the account to be fixed:" default answer ""' -e 'text returned of result'`

#Users Password
UsersPW=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter the password of the account to be fixed:" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`

#Notification to state fix in progress
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "FileVault Password Fix" -description  "Please do not interrupt or power off during this process."&

fdesetup remove -user "$UserToFix"


expect <<- DONE
set timeout -1
spawn fdesetup add -usertoadd "$UserToFix"
expect "Enter the user name:"
send "locadmin\r"
expect "Enter the password for user 'locadmin':"
send "$AdminPW\r"
expect "Enter the password for the added user '$UserToFix':"
send "$UsersPW\r"
expect EOF
DONE


ps axco pid,command | grep jamfHelper | awk '{ print $1; }' | xargs kill -9