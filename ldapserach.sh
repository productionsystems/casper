#!/bin/bash

set -x

log()
{
  echo "$1" ; logger "$1"
}


fatal()
{
  log "$1" ; exit 2
}


WHOAMI=`stat -f "%Su" /dev/console`

#if [ "$1" ]; then
#   log "Setting WHOAMI to $1"
#    WHOAMI=$1
# fi

  echo $WHOAMI

QueryAD() 
{
  local ATTRIB=$1
  local VALUE=$2
  local OUTPUT=$3
  if [ -z "$ATTRIB" ]; then
    log "Search attribute not specified."
    return 1
  fi

  if [ -z "$VALUE" ]; then
    log "Search value not specified."
    return 1
  fi

  if [ -z "$OUTPUT" ]; then
    log "Attribute for output not specified."
    return 1
  fi

  RESULT=`ldapsearch -LLL -H LDAPS://gswlvmva14.hogarthww.prv -x -b "DC=hogarthww,DC=prv" -D "CN=Casper JSS,OU=ServiceUsers,OU=HogarthWW,DC=hogarthww,DC=prv" -w Jasper99 "($ATTRIB=$VALUE)" "$OUTPUT" | grep -i "$OUTPUT:" | sed -e "s|$OUTPUT: ||"`
  if [ `echo $RESULT | grep -o "::"` ]; then
    RESULT=`echo $RESULT | awk -F":: " '{print $2}' | base64 -D`
  fi
  if [ `echo $RESULT | grep -o ":"` ]; then
    RESULT=`echo $RESULT | awk -F": " '{print $2}'`
  fi
  echo $RESULT ;
}


log "Finding User information with QueryAD..."

DISPLAYNAME=`QueryAD "samaccountname" $WHOAMI displayName`
TITLE=`QueryAD "samaccountname" $WHOAMI title`
OTHERTELEPHONE=`QueryAD "samaccountname" $WHOAMI otherTelephone`
STREETADDRESS=`QueryAD "samaccountname" $WHOAMI streetAddress`
L=`QueryAD "samaccountname" $WHOAMI l`
POSTALCODE=`QueryAD "samaccountname" $WHOAMI postalCode`
C=`QueryAD "samaccountname" $WHOAMI c`
EMAIL=`QueryAD "samaccountname" $WHOAMI mail`
MOBILE=`QueryAD "samaccountname" $WHOAMI mobile`

osascript <<EOD
tell application "Microsoft Outlook"	make new signature with properties {name:"Hogarth Worldwide Limited", content:"-- <br />
	<p><font size=\"2\"><font face=\"Fancy Font\"><font color=\"000000\">Kind regards</font></p>
	<strong>$DISPLAYNAME</strong></p>
	<strong>$TITLE</strong><br />
	<br>
	Hogarth Worldwide Limited<br />
	$STREETADDRESS<br />
	$L $POSTALCODE</p>
	$C</p>
	<a href=\"http://www.Hogarthww.com\">www.Hogarthww.com</a></p>
"}
end tell
EOD

echo $DISPLAYNAME
echo $TITLE
echo $OTHERTELEPHONE
echo $STREETADDRESS
echo $L
echo $POSTALCODE
echo $C




# Need it set to default

exit 0
