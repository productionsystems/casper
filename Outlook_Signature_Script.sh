#!/bin/bash

#set -x

log()
{
  echo "$1" ; logger "$1"
}


fatal()
{
  log "$1" ; exit 2
}


WHOAMI=`stat -f "%Su" /dev/console`

#if [ "$1" ]; then
#   log "Setting WHOAMI to $1"
#    WHOAMI=$1
# fi

  echo $WHOAMI

QueryAD() 
{
  local ATTRIB=$1
  local VALUE=$2
  local OUTPUT=$3
  if [ -z "$ATTRIB" ]; then
    log "Search attribute not specified."
    return 1
  fi

  if [ -z "$VALUE" ]; then
    log "Search value not specified."
    return 1
  fi

  if [ -z "$OUTPUT" ]; then
    log "Attribute for output not specified."
    return 1
  fi

  RESULT=`ldapsearch -LLL -H LDAPS://gswlvmva14.hogarthww.prv -x -b "DC=hogarthww,DC=prv" -D "CN=Casper JSS,OU=ServiceUsers,OU=HogarthWW,DC=hogarthww,DC=prv" -w Jasper99 "($ATTRIB=$VALUE)" "$OUTPUT" | grep -i "$OUTPUT:" | sed -e "s|$OUTPUT: ||"`
  if [ `echo $RESULT | grep -o "::"` ]; then
    RESULT=`echo $RESULT | awk -F":: " '{print $2}' | base64 -D`
  fi
  if [ `echo $RESULT | grep -o ":"` ]; then
    RESULT=`echo $RESULT | awk -F": " '{print $2}'`
  fi
  echo $RESULT ;
}


log "Finding User information with QueryAD..."

DISPLAYNAME=`QueryAD "samaccountname" $WHOAMI displayName`
TITLE=`QueryAD "samaccountname" $WHOAMI title`
OTHERTELEPHONE=`QueryAD "samaccountname" $WHOAMI otherTelephone`
STREETADDRESS=`QueryAD "samaccountname" $WHOAMI streetAddress`
L=`QueryAD "samaccountname" $WHOAMI l`
C=`QueryAD "samaccountname" $WHOAMI c`
EMAIL=`QueryAD "samaccountname" $WHOAMI mail`
MOBILE=`QueryAD "samaccountname" $WHOAMI mobile`

osascript <<EOD
tell application "Microsoft Outlook"
	make new signature with properties {name:"Hogarth Worldwide Limited", content:"-- <br />
	<p style="margin: 0in 0in 0.0001pt; font-size: medium; font-family: 'Helvetica Neue'; color: #40506e; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-family: Helvetica; color: gray;">--</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #40506e;">$DISPLAYNAME</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #40506e;">$TITLE</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #40506e;">$L</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #5eb1bf;">&nbsp;</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #8f8e8e;">+44 (0) 7000 000 000 &ndash; Mobile</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #8f8e8e;">+44 (0) 7000 000 000 &ndash; Mobile </span></p>
    <p style="margin: 0in 0in 0.0001pt; font-size: medium; font-family: 'Helvetica Neue'; color: #40506e; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #8f8e8e;">$STREETADDRESS</span></p>
    <p style="margin: 0in 0in 0.0001pt; font-size: medium; font-family: 'Helvetica Neue'; color: #40506e; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica;">hogarthww.com</span></p>
    <p style="margin: 0in 0in 0.0001pt; line-height: normal; font-size: medium; font-family: MinionPro-Regular, serif; color: #000000; caret-color: #000000; font-variant-caps: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; text-decoration: none;"><span style="font-size: 10pt; font-family: Helvetica; color: #5eb1bf;">&nbsp;</span></p>
"}
end tell
EOD

# Need it set to default

exit 0