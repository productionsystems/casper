#!/bin/sh

#Alternet source: https://derflounder.wordpress.com/2015/03/13/deploying-a-pre-configured-junos-pulse-vpn-client-on-os-x/

# Install Pulse Secure Package

/usr/sbin/installer -dumplog -verbose -pkg "/Library/Application Support/JAMF/tmp/Pulse Secure 5.2.5.869.pkg" -target /

# Check for installed Pulse Secure and if found install Sapient config

if [[ -d "/Applications/Pulse Secure.app" ]]; then

    echo "Pulse Secure VPN Client Installed"
    /Applications/Pulse\ Secure.app/Contents/Plugins/JamUI/jamCommand -importFile /Library/Application\ Support/JAMF/tmp/ourconfig.jnprpreconfig
    echo "VPN Configuration Installed"
else 
    echo "Pulse Secure Client Not Installed"  
fi

# Clean up install files

rm -Rf "/Library/Application\ Support/JAMF/tmp/Pulse Secure 5.2.5.869.pkg"
rm /Library/Application\ Support/JAMF/tmp/ourconfig.jnprpreconfig

user=`ls -l /dev/console | cut -d " " -f 4`

sudo -u "$user" launchctl load -w /Library/LaunchAgents/net.pulsesecure.pulsetray.plist