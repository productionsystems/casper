#!/bin/bash

#User runs something from Self Service notifying them their Mac is going to reboot, Mac reboots, gets to login window and immediately jamf helper takes over the entire screen. Script does its thing, then deletes the LaunchAgent and either kills the jamf helper process or reboots again.

cat << EOF > /Library/LaunchAgents/org.yourorg.loginJamfHelper.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>org.yourorg.loginJamfHelper</string>
    <key>RunAtLoad</key>
    <true/>
    <key>LimitLoadToSessionType</key>
    <string>LoginWindow</string>
    <key>ProgramArguments</key>
    <array>
        <string>/Library/Application Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper</string>
        <string>-windowType</string>
        <string>fs</string>
        <string>-heading</string>
        <string>"$heading"</string>
        <string>-title</string>
        <string>"$title"</string>
        <string>-description</string>
        <string>"$description"</string>
        <string>-icon</string>
        <string>"$icon"</string>
    </array>
</dict>
</plist>
EOF