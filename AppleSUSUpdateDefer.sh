#!/bin/sh

##############################################################################################################################################
# This script checks to see if there is a logged in user, if no user is logged in software update will run and install all udpates.			 #
# If a user is logged in the script checks if the updates need a restart - if so the user is prompted whether this is ok before proceeding.  #
# Where no restart is required software update will run without interrupting the user.														 #
# Orgiginal Ref below:														 																 #
# https://jamfnation.jamfsoftware.com/discussion.html?id=5404#responseChild27123                                                             #
##############################################################################################################################################

# Change Log
# V1.0 JF 03/2017 - Testing
# V1.1 JF 03/2017 - Added deferral option
# V1.2 JF 03/2017 - Added run once per month structure
# V1.3 JF 04/2017 - Added "Current Status" to notification

# Clean SUS
sudo jamf removeSWUSettings
sudo softwareupdate --schedule off

# Set Date Variables
RunDate=$(date | awk '{print $3 $4}')
date=$(date | awk '{print $3 $4}')

# Create dir and file for monthly timer
if [[ ! /Library/"Application Support"/Hogarth ]]; then
	/bin/mkdir /Library/"Application Support"/Hogarth
	/usr/bin/chown root:wheel /Library/"Application Support"/Hogarth
	/bin/chmod 755 /Library/"Application Support"/Hogarth
fi

if [[ ! -f /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist ]]; then
	/usr/bin/touch /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	/bin/chmod 755 /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	echo FirstRun > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
fi

# Create deferral counter file
if [[ ! -f /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist ]]; then
touch /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
chown root:wheel /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
chmod 755 /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
fi

defer=/Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
COUNTER=$[$(cat "$defer") + 1]
echo "$COUNTER" > "$defer"

# Test if this has been run once this month
if [[ $(cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist) != $date ]]; then
	echo "Running SUS Update"

	if [[ $(cat "$defer") -lt 5 ]]; then
		#see if user logged in
		USER=$(/usr/bin/who | /usr/bin/grep console | /usr/bin/cut -d " " -f 1);
		echo "logged in user is $USER...";

			#check if user logged in
			if [[ -n "$USER" ]]; then

	   		 	#Check if restarts required
	   		 	AVAILABLEUPDATES=$(/usr/sbin/softwareupdate -l);
	   		 	NOUPDATES=$(echo $AVAILABLEUPDATES | wc -l | cut -d " " -f 8);
	   		 	RESTARTREQUIRED=$(echo $AVAILABLEUPDATES | /usr/bin/grep restart | /usr/bin/cut -d "," -f 1);

	   		 		if [[ -n "$RESTARTREQUIRED" ]]; then
	      			  	echo "$RESTARTREQUIRED needs a restart";

	      			  	#ask user whether ok to restart
	      				OKTORESTART=$(/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility  -title "Apple Software updates are available" -description "Your computer will need to restart, would you like to install the updates now?
      
			$AVAILABLEUPDATES" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "Yes" -button2 "No" -cancelButton "2")

		  	  	echo "ok to restart was $OKTORESTART";

	      	  			if [[ "$OKTORESTART" = "0" ]]; then
			  			  	while :
			  				do
			  				Status_Message=`awk 'NF{p=$0}END{print p}' /tmp/SUS.log`
			  				/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
Your Mac will restart once all of recommend software has been installed, please save your work now.

Current Status: "$Status_Message"" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD -timeout 10 &
			  				sleep 9
		  			  		done &
							echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	        				/usr/sbin/softwareupdate -ia > /tmp/SUS.log
							/sbin/reboot
	      					exit 0
	      			  else
	         			  echo "User said NO to updates";
			 			  /Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Deferral Request" -description "You are allowed to defer for 5 days, after that it will be mandatory! You have deferred `cat "$defer"` times." -button1 "OK"
	     	   			  exit 1
	      			fi
	   	  	  		else
	      				echo "No restart is needed";

	      	  			if [[ "$NOUPDATES" != "1" ]]; then
	         			   echo "So running Software Update"; 
	         			  /usr/sbin/softwareupdate -ia
	         			 exit 0
	      		   else
	         		  echo "because there were no updates";
	         		 exit 0
	      	   fi
	   	fi

		else
	   	 	#No logged in user
	   		/usr/sbin/softwareupdate -ia
		fi
	
	elif [[ $(cat "$defer") = 5 ]]; then

		#see if user logged in
		USER=$(/usr/bin/who | /usr/bin/grep console | /usr/bin/cut -d " " -f 1);
		echo "logged in user is $USER...";

			#check if user logged in
			if [[ -n "$USER" ]]; then

	   		 	#Check if restarts required
	   	  		AVAILABLEUPDATES=$(/usr/sbin/softwareupdate -l);
	   		 	NOUPDATES=$(echo $AVAILABLEUPDATES | wc -l | cut -d " " -f 8);
	   		 	RESTARTREQUIRED=$(echo $AVAILABLEUPDATES | /usr/bin/grep restart | /usr/bin/cut -d "," -f 1);

	   			if [[ -n "$RESTARTREQUIRED" ]]; then
	      	  		echo "$RESTARTREQUIRED needs a restart";

	      	  		#ask user whether ok to restart

	      	  		OKTORESTART=$(/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility  -title "Apple Software updates are available" -description "Your computer will need to restart, you are required to install the updates now.
      
	  	  			$AVAILABLEUPDATES" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "OK")

		  		echo "ok to restart was $OKTORESTART";

	      	  		if [[ "$OKTORESTART" = "0" ]]; then
			  		  	while :
			  		  	do
			  			Status_Message=`awk 'NF{p=$0}END{print p}' /tmp/SUS.log`
			  		  	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
Your Mac will restart once all of recommend software has been installed, please save your work now.

Current Status: "$Status_Message"" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD -timeout 10 &
			  			sleep 9
			  	  		done &
						echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
						/usr/sbin/softwareupdate -ia > /tmp/SUS.log
	        			/sbin/reboot
	      				exit 0
	      		  	fi
	   		 		else
	      			  	echo "No restart is needed";

	      			if [[ "$NOUPDATES" != "1" ]]; then
	         		   	echo "So running Software Update";
			 		  	echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist 
	         		   	/usr/sbin/softwareupdate -ia
	         		   	exit 0
	      		  	else
	         		   	echo "because there were no updates";
	         		   	exit 0
	      		  	fi
	   		 fi

	else
	   #No logged in user
	   echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	   /usr/sbin/softwareupdate -ia
	   rm /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	fi
fi

elif [[ `cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist` = "$date" ]]; then
	echo "No Update Required"
	exit 0
fi

exit 0
