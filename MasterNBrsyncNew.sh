#! /bin/bash

set -x

# This script will rsync the Netboot nbi to all avaiable global NetBoot servers
# Ensure when run that the exact name for the .nbi is used 

JSSserver=https://10.252.32.155:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiuser # this user requires TBC access privs in API settings on JSS 
JSSapipw=PnZ-Nv5-E2P-CNR

curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/netbootservers

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

#Get NB ID's from xml
cat /tmp/all.xml | grep "<id>" | sort -t ">" -k2 -g | sed "s/^[ \t]*//" | sed 's*<id>**g' | sed 's*</id>**g' | sed '/13/d' > /tmp/nbid.txt

echo "Hello $USER, please enter the full name of the new NetBoot image [ENTER]: "
read NEWNBINAME

NEWNBILOC="/srv/NetBoot/NetBootSP0/$NEWNBINAME"

while read id 
do
curl -H "Accept: application/xml" -s -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/netbootservers/id/$id | xmllint --format - > /tmp/nb.txt 
IP=`grep "<ip_address>" /tmp/nb.txt | sed 's*<ip_address>**g' | sed 's*</ip_address>**g' | sed 's/^.\{2\}//'`

ping -c 1 $IP	
if [ $? -eq 0 ]; then
		rsync -rvu --progress "$NEWNBILOC" casperadmin@"$IP":/srv/NetBoot/NetBootSP0/
fi

done </tmp/nbid.txt

exit 0





