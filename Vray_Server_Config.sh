#!/bin/bash

#set -x

#This script is to run when a user logs on, to create the necessary Lic file for V-Ray for C4D R20 plugin.

#LaunchAgents
#Create launchd item to call at login
cat <<\EOF > /Library/LaunchAgents/com.V-Ray_For_C4D.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>com.flexlmrc</string>
	<key>ProgramArguments</key>
	<array>
		<string>/usr/local/scripts/V-Ray_For_C4D_R20_London_Lic.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>

EOF

#Define machine locations
LOC=`echo $HOSTNAME | cut -c 1-4`

#Define servers
case $LOC in
	OGAB | ogab | TMCN | tmcn | YRMM | yrmm | HGPN | hgpn | YRCN | yrcn | JWLN | jwln | HGAS | hgas | CHTC | chtc | ELMN | elmn | HGPN | hgpn | HGPA | hgpa | LCMM | lcmm)
	server="AMER"
	;;
	HGPC | hgpc)
	server="Chennai"
	;;
	BUOD | buod | HGBH | hgbh | GRID | grid | HGSL | hgsl | HGPN | hgpn | HGGL | hggl | HOUL | houl | HGME | hgme | HGSC | hgsc | HGWJ | hgwj | RFAP | rfap | GRHL | grhl | GRUW | gruw | GRIM | grim | WMHL | wmhl | YRHL | yrhl | YRNP | yrnp | HGWJ | hgwj | OGGB | oggb | JWKL | jwkl | SAGM | sagm | METL | metl | GEWL | gewl | EYML | eyml | TJBL | tjbl | MEUL | meul | BAWL | bawl)
	server="EMEA"
	;;
	JWHS | jwhs | HGSK | hgsk | HGSS | hgss | HGCS | hgcs | HGKH | hgkh | HGUG | hgug | GRMS | grms | YRCN | yrcn | OGQH | ogqh | OGRS | ogrs | REBJ | rebj | GRJS | grjs | OGRB | ogrb)
	server="APAC"
esac

#Write file acording to server
if [ $server = EMEA ]; then
cat <<\EOF > /usr/local/scripts/vrlclient.xml
  <VRLClient>
          <LicServer>
                  <Host>ixllvmva90</Host>
                  <Port>30304</Port>
                  <Host1></Host1>
                  <Port1>30304</Port1>
                  <Host2></Host2>
                  <Port2>30304</Port2>
                  <User></User>
                  <Pass></Pass>
          </LicServer>
  </VRLClient>
EOF
fi

#Write file acording to server
if [ $server = Chennai ]; then
cat <<\EOF > /usr/local/scripts/vrlclient.xml
  <VRLClient>
          <LicServer>
                  <Host>????</Host>
                  <Port>30304</Port>
                  <Host1></Host1>
                  <Port1>30304</Port1>
                  <Host2></Host2>
                  <Port2>30304</Port2>
                  <User></User>
                  <Pass></Pass>
          </LicServer>
  </VRLClient>
EOF
fi

#Create the script
cat <<\EOF > /usr/local/scripts/V-Ray_For_C4D_R20_London_Lic.sh
#!/bin/bash

#Current User - check and create file if needed.
CUSERNAME=`who |grep console| awk '{print $1}'`

#Create DIR
if [ -d /Users/$CUSERNAME/.ChaosGroup ]; then
  exit
else
mkdir /Users/$CUSERNAME/.ChaosGroup
chown admin:wheel /Users/$CUSERNAME/.ChaosGroup
chmod 755 /Users/$CUSERNAME/.ChaosGroup
fi

if [ -f /usr/local/scripts/$CUSERNAME.vrlclient.done.txt ]; then
exit
else
cp /usr/local/scripts/vrlclient.xml /Users/$CUSERNAME/.ChaosGroup/
chown admin:wheel /Users/$CUSERNAME/.ChaosGroup/vrlclient.xml
chmod 755 /Users/$CUSERNAME/.ChaosGroup/vrlclient.xml
touch /usr/local/scripts/$CUSERNAME.vrlclient.done.txt
fi

EOF

#Change permissions and owner
chown root:wheel /Library/LaunchAgents/com.V-Ray_For_C4D.plist
chmod 644 /Library/LaunchAgents/com.V-Ray_For_C4D.plist
chown admin:wheel /usr/local/scripts/V-Ray_For_C4D_R20_London_Lic.sh
chmod 755 /usr/local/scripts/V-Ray_For_C4D_R20_London_Lic.sh

exit 0
