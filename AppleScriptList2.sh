#!/bin/bash

#clear # turn on debug mode 
#set -x 
#for f in *
#do
#    file $f 
#done 

# Hostname requirments - 
# Ask user for config information: Country, Building, Floor, Job role
# Need a check at the end to ensure they are happy with their choices, if not restart process

Reagion=`/usr/bin/osascript << EOT
tell application "Finder"
activate
display dialog "As part of Hogarths user enrolment policy you will now be asked a few questions to start the configuration of your new machine.

Please ensure you complete this process, as failure to do so will result in no compliance with Hogarths policies." buttons {"Continue"} with icon {"/Library/Application Support/Hogarth/hogarth.jpeg"}
set Reagion to (choose from list {"EMEA", "AMER", "APAC"} with prompt "Please select your current region" default items "OK" OK button name {"OK"})
-- choose from list cancel button returns false, not -128, as do other choose syntax do.
if Reagion is false then error number -128 -- user canceled
end tell
return (posix path of Reagion)
EOT
exit`

if [[ $Reagion = /EMEA ]]; then
City=`/usr/bin/osascript << EOT
tell application "Finder"
activate
set City to (choose from list {"London", "Edinburgh", "Milton Keynes", "Dusseldorf", "Istanbul", "Prague", "Bucharest", "Dubai", "Cape Town", "Johannesburg", "Hamburg", "Paris", "Warsaw"} with prompt "Please select your City" default items "None" OK button name {"OK"} cancel button name {"Cancel"})
-- choose from list cancel button returns false, not -128, as do other choose syntax do.
if City is false then error number -128 -- user canceled
end tell
return (posix path of City)
EOT
exit`
fi

if [[ $Reagion = /AMER ]]; then
	City=`/usr/bin/osascript << EOT
	tell application "Finder"
	activate
	set City to (choose from list {"Sao Paulo", "New York", "Miami", "New Jersey", "Costa Mesa"} with prompt "Please select your City" 	default items "None" OK button name {"OK"} cancel button name {"Cancel"})
	-- choose from list cancel button returns false, not -128, as do other choose syntax do.
	if City is false then error number -128 -- user canceled
	end tell
	return (posix path of City)
EOT
quit`
fi

if [[ $Reagion = /APAC ]]; then
	City=`/usr/bin/osascript << EOT
	tell application "Finder"
	activate
	set City to (choose from list {"Sao Paulo", "New York", "Miami"} with prompt "Please select your City" default items "None" OK button 	name {"OK"} cancel button name {"Cancel"})
	-- choose from list cancel button returns false, not -128, as do other choose syntax do.
	if City is false then error number -128 -- user canceled
	end tell
	return (posix path of City)
EOT
quit`
fi

if [[ $City = /London ]]; then
	Building=`/usr/bin/osascript << EOT
	tell application "Finder"
	activate
	set Building to (choose from list {"Shaftesbury Avenue", "Great Pulteney Street"} with prompt "Please select your City" default items "None" OK button name {"OK"} cancel button name {"Cancel"})
	-- choose from list cancel button returns false, not -128, as do other choose syntax do.
	if Building is false then error number -128 -- user canceled
	end tell
	return (posix path of Building)
EOT
quit`
fi	

if [[ $City = /New York ]]; then
	Building=`/usr/bin/osascript << EOT
	tell application "Finder"
	activate
	set Building to (choose from list {"230 Park Avenue South", "Great Pulteney Street"} with prompt "Please select your City" default items "None" OK button name {"OK"} cancel button name {"Cancel"})
	-- choose from list cancel button returns false, not -128, as do other choose syntax do.
	if Building is false then error number -128 -- user canceled
	end tell
	return (posix path of Building)
EOT
quit`
fi	


