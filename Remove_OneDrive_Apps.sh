#!/bin/bash


#Check and remove

#Check if OneDriveDF is open and close
ps aux | pgrep OneDriveDF
if [ $? -eq 0 ]; then
	echo "OneDrive is active exiting"
	#quit application
	rm -rf /Applications/OneDriveDF.app
fi

#Check if OneDrive for Business is open and close
ps aux | pgrep OneDrive\ for\ Business
if [ $? -eq 0 ]; then
	echo "OneDrive for Business is active exiting"
	#quit application
	rm -rf /Applications/OneDrive\ for\ Business.ap
fi

