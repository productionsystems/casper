#!/bin/bash

#Adobe Acrobat DC last used

USERNAME=`who |grep console| awk '{print $1}'`

Initializing /Library/Application Support/Adobe/Acrobat DC AMT

CCUsage=`cat /Users/$USERNAME/Library/logs/amt3.log | grep -E 'Initializing /Library/Application Support/Adobe/Acrobat DC AMT' | tail -1 | awk '{ print $1 }'`

if [ $? = 0 ]; then
		echo "$CCUsage"
	elif [ `$CCUsage | wc -c` -eq 0 ] || [ ! -d /Applications/Adobe*/Adobe*.app ]; then
	CCUsage="Null"
	fi
echo "<result>"$CCUsage"</result>"