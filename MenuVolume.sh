#!/bin/sh

currentUser=$( ls -l /dev/console | awk '{print $3}' )
userHome=$( dscl . read /Users/$currentUser NFSHomeDirectory | awk '{print $NF}' )

MenuExtras=$( defaults read "$userHome/Library/Preferences/com.apple.systemuiserver.plist" menuExtras | awk -F'"' '{print $2}' )

if [[ $( echo "${MenuExtras}" | grep "Keychain.menu" ) ]]; then
	Volume=$(grep "Volume.menu" /Users/$currUser/Library/Preferences/com.apple.systemuiserver.plist -c)
	if [ $Volume == 0 ]; then
	    echo "Adding Volume status to menu" 
	    open '/System/Library/CoreServices/Menu Extras/Volume.menu'
	fi
fi