#!/bin/bash

function TempAdmin {
    
    set +x

/bin/chmod 777 /etc/hosts
/usr/local/jamf/bin/jamf displayMessage -message "The hosts file is editable for five minutes"
    
secs=$((5 * 60))
while [ $secs -gt 0 ]; do
	   	echo -ne "$secs\033[0K\r"
	   	sleep 1
	   	: $((secs--))
	   	if [ $secs == 60 ]; then
	   	    /usr/local/jamf/bin/jamf displayMessage -message "Access to the hosts file has one minute remaining"
	   	fi
        if [ $secs == 0 ]; then
	        /bin/chmod 644 /etc/hosts
			/usr/local/jamf/bin/jamf displayMessage -message "Access to the hosts file has been reset"
   fi   
done
exit 0
}

TempAdmin &