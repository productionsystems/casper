#!/bin/bash

(crontab -l && echo "0 */4 * * * /usr/local/scripts/AutoupdateJabberVideo.sh") | crontab -

# Create Hogarth folder structure
if [[ ! -d /usr/local/scripts/ ]]; then
mkdir /usr/local/scripts/ 
chown root:wheel /usr/local/scripts/
chmod 777 /usr/local/scripts/
fi

cat <<EOF > /usr/local/scripts/AutoupdateJabberVideo.sh
#!/bin/bash
set -x

dmgfile="JabberVideo-5.dmg"
volname="Jabber Video"
logfile="/Library/Logs/JabberVideoInstallScript.log"
url='http://17.106.105.180/jabbervideo/jabbervideoapac/JabberVideo.dmg'

#test for Apple network
if [[ ! $(ping -t1 17.106.105.180) = "0" ]]; then
	exit
fi

while [[ $(ps aux | grep Jabber) = "0" ]]; do
	## Get the logged in user's name
	loggedInUser=$( ls -l /dev/console | awk '{print $3}' )
	## Get the UID of the logged in user
	loggedInUID=$(id -u "$loggedInUser")
	/bin/launchctl asuser $loggedInUID sudo -iu "$loggedInUser" /usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please close the Jabber Video application and then select ok"'
done

if [[ $(ping -t1 17.106.105.180) = "0" ]]; then
		/bin/echo "--" >> ${logfile}
		/bin/echo "`date`: Downloading latest version." >> ${logfile}
		/usr/bin/curl -s -o /tmp/${dmgfile} ${url}
		/bin/echo "`date`: Mounting installer disk image." >> ${logfile}
		/usr/bin/hdiutil attach /tmp/${dmgfile} -nobrowse -quiet
		#Version check
		new=$(defaults read /Volumes/Jabber\ Video/Jabber\ Video.app/Contents/Info.plist | grep CFBundleShortVersionString | awk '{print $3}' | cut -d'"' -f2)
		current=$(defaults read /Applications/Jabber\ Video.app/Contents/Info.plist | grep CFBundleShortVersionString | awk '{print $3}' | cut -d'"' -f2)
		if [[ ! $new = $current ]]; then
			/bin/echo "`date`: Installing..." >> ${logfile}
			ditto -rsrc "/Volumes/${volname}/Jabber Video.app" "/Applications/Jabber Video.app" >> ${logfile}
			/bin/sleep 10
			/bin/echo "`date`: Unmounting installer disk image." >> ${logfile}
			/usr/bin/hdiutil detach $(/bin/df | /usr/bin/grep "${volname}" | awk '{print $1}') -quiet
			/bin/sleep 10
			/bin/echo "`date`: Deleting disk image." >> ${logfile}
			/bin/rm /tmp/"${dmgfile}"
		else 
			/bin/echo "`date`: No update required." >> ${logfile}
			/bin/echo "`date`: Unmounting installer disk image." >> ${logfile}
			/usr/bin/hdiutil detach $(/bin/df | /usr/bin/grep "${volname}" | awk '{print $1}') -quiet
			/bin/sleep 10
			/bin/echo "`date`: Deleting disk image." >> ${logfile}
			/bin/rm /tmp/"${dmgfile}"
		exit
	fi
fi
exit 0
EOF

cat <<EOF > /Library/LaunchDaemons/com.hogarthww.AutoupdateJabberVideo.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>com.JabberVideo.app</string>
		<key>Program</key>
		<string>/usr/local/scripts/AutoupdateJabberVideo.sh</string>
		<key>RunAtLoad</key>
		<true/>
	</dict>
</plist>
EOF

#Set permissions
/usr/sbin/chown root:wheel /usr/local/scripts/AutoupdateJabberVideo.sh
/bin/chmod 755 /usr/local/scripts/AutoupdateJabberVideo.sh

/usr/sbin/chown root:wheel /Library/LaunchDaemons/com.hogarthww.AutoupdateJabberVideo.plist
/bin/chmod 644 /Library/LaunchDaemons/com.hogarthww.AutoupdateJabberVideo.plist