#!/bin/bash

set -x

cat <<EOF > /Library/LaunchDaemons/net.Hogarthww.PathFinder.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>net.Hogarthww.PathFinder</string>
		<key>ProgramArguments</key>
		<array>
			<string>/bin/sh</string>
			<string>/usr/local/scripts/PathFinder.sh</string>
		</array>
		<key>RunAtLoad</key>
		<true/>
	</dict>
</plist>
EOF

cat <<EOF > /usr/local/scripts/PathFinder.sh
#!/bin/bash

cd /Users/
for path in * ;
do
cp -rp /Users/locadmin/Library/Application\ Support/Path\ Finder  $path/Library/Application\ Support/ > /dev/null 2>&1

done
EOF

/usr/sbin/chown root:wheel /Library/LaunchDaemons/net.Hogarthww.PathFinder.plist
/bin/chmod 644 /Library/LaunchDaemons/net.Hogarthww.PathFinder.plist
/usr/sbin/chown root:wheel /usr/local/scripts/PathFinder.sh
/bin/chmod 755 /usr/local/scripts/PathFinder.sh

/bin/chmod +x /usr/local/scripts/PathFinder.sh

sh /usr/local/scripts/PathFinder.sh