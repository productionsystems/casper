#!/bin/bash

AdobeApp="$4"

# Close App
while true; 
do
	activeCC=`pgrep -l "$AdobeApp" | awk '{$1=""; print $0}'`
	if [ ! -z "$activeCC" ]; then
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Adobe\ Creative\ Cloud\ Experience/CCXProcess.app/Contents/Resources/cc_app.icns -heading "Adobe CC Installations" -description "Please close all of the following applications to allow for the installation to continuue
	
	$activeCC" -button1 'OK' > /dev/null 2>&1
else
    jamf policy -event "$5"
	break
fi
done