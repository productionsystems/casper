#!/bin/bash

# Adobe CC Extensions Attributes usage data

CCUsage=`mdls /Applications/Adobe*/Adobe*.app | grep kMDItemLastUsedDate | awk '{ $1 = $1 } { print }' | cut -c 23-41 | tail -c 20 | cut -c 1-10 | grep -v /Applications/Adobe/Adobe\ Acrobat\ DC | grep -v /Applications/Adobe\ Acrobat\ XI\ Pro`
mdls /Applications/Adobe*/Adobe*.app | grep kMDItemLastUsedDate | awk '{ $1 = $1 } { print }' | cut -c 23-41 | tail -c 20 | cut -c 1-10 | grep -v /Applications/Adobe/Adobe\ Acrobat\ DC | grep -v /Applications/Adobe\ Acrobat\ XI\ Pro
if [ $? = 0 ]; then
		echo "$CCUsage"
	elif [ `$CCUsage | wc -c` -eq 0 ] || [ ! -d /Applications/Adobe*/Adobe*.app ]; then
	CCUsage="Null"
	fi
echo "<result>"$CCUsage"</result>"



CCUsage=$(mdls /Applications/Adobe\ Acrobat*/Adobe\ Acrobat*.app | grep kMDItemLastUsedDate | awk '{ $1 = $1 } { print }' | cut -c 23-41 | tail -c 20 | cut -c 1-10)
if [[ $("$CCUsage" | wc -c) != 0 ]] || [[ ! -d /Applications/Adobe\ Acrobat*/Adobe\ Acrobat*.app ]]; then
	CCUsage="Null"
elif [ $? = 0 ]; then
	echo "$CCUsage"
	fi
echo "<result>$CCUsage</result>"


old data:
UsageDate=`mdls /Applications/Adobe\ Acrobat*/Adobe\ Acrobat*.app | grep kMDItemLastUsedDate | awk '{ $1 = $1 } { print }' | cut -c 23-41 | tail -c 20 | cut -c 1-10`
OlderDate=`date -v-180d +%F`	
	if [ $UsageDate > $OlderDate ]; then
		CCUsage="Adobe CC last used"
	else
		CCUsage="Adobe CC not used in the last 6 months"
	fi
echo "<result>"$CCUsage $UsageDate"</result>"