#!/bin/bash

# Need to define machines with _MAS Office installations

# Apps to delete
appsArray=(Excel Outlook PowerPoint Word)

findapps()
{

    # Delete the apps in appsArray, if installed.
        for appTitle in "${appsArray[@]}"
        do
            if [ -d /Applications/"Microsoft ${appTitle}".app ]
            then
            	/bin/echo "Found Microsoft ${appTitle}.app..."
            	if [ -d /Applications/"Microsoft ${appTitle}".app/Contents/_MASReceipt ]
            	then
            		result=`/bin/echo "Office is Managed by VPP"`
                fi
            else
                /bin/echo "Cannot find Microsoft ${appTitle}.app..."
            fi
        done
}

setup
findapps

echo "<result>$result</result>"