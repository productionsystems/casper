#!/bin/bash

#This will delete the depadmin account created at build time, and will also define and delete any admin account created by the User Initiated Enrolment method "UIE"

if [ /Users/depadmin ]; then

cat <<EOF > /Library/LaunchDaemons/net.Hogarthww.DeleteAdminAccount.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>net.Hogarthww.DeleteAdminAccount</string>
	<key>ProgramArguments</key>
	<array>
		<string>/bin/sh</string>
		<string>/usr/local/scripts/DeleteAdminAccount.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>
EOF

cat <<EOF > /usr/local/scripts/DeleteAdminAccount.sh
#!/bin/bash

#Define first login use for "UIE"

#Current user
CurrentUser=`cat /usr/local/scripts/FirstDEPUser.txt`

/usr/local/jamf/bin/jamf deleteAccount -username $CurrentUser
/usr/local/jamf/bin/jamf deleteAccount -username depadmin

EOF

/usr/sbin/chown root:wheel /Library/LaunchDaemons/net.Hogarthww.DeleteAdminAccount.plist
/bin/chmod 644 /Library/LaunchDaemons/net.Hogarthww.DeleteAdminAccount.plist
/bin/chmod +x /usr/local/scripts/DeleteAdminAccount.sh

fi

