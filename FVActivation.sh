#!/bin/bash

################################################################################################################################
# This script is esigned to activate Filevault as part of the build process and also add new user to Filevault when they login #
################################################################################################################################

# Change Log
# V1.0 JF 04/2019 - Testing

# Requirements:
# 	New machines: 
#		1. To activate FV at build time for all machines using locadmin, called by trigger as part of tier one
#		2. AD users - 
#  
#
# Tests and workflow:
# Check if machine has FV active, if true exit
# Check if locadmin has Secure Token - what to do if it doesn't?
# If true then enable FV for locadmin
# will need to convert mobile accounts to local - https://derflounder.wordpress.com/2016/12/21/migrating-ad-mobile-accounts-to-local-user-accounts/
# Jamf Connect should add users to FV when they login - will this happen for existing users
# Will need to capture the key once set
# JC Ref: https://www.jamf.com/jamf-nation/feature-requests/7900/jamf-connect-login-automatically-enable-new-account-for-filevault#responseChild25602
# JC Ref: https://www.jamf.com/jamf-nation/feature-requests/7516/add-ability-to-suppress-display-of-the-filevault-key-when-encrypting-via-configuration-profile#responseChild23165



# Debug mode
set -x

####################################
# Activate FV and upload key to JP #
####################################
																
AdminPW="$4"
AdminName="locadmin"
encryptCheck=`fdesetup status`
statusCheck=$(echo "${encryptCheck}" | grep "FileVault is On.")
expectedStatus="FileVault is On."

# Check if FV is already active and exit if true
if [[ $encryptCheck == "FileVault is On." ]]; then
	echo "Filevault already active"
	exit
fi

# Secure Token Status
STStatus=`sysadminctl interactive -secureTokenStatus locadmin 2> /tmp/STSatus.txt ; cat /tmp/STSatus.txt | awk '{print $7}'`

# Auto Activate FV
FuncFVKey()
{
if [[ $STStatus = "ENABLED" ]]; then
		expect <<- DONE
		set timeout -1
		spawn fdesetup enable
		expect "Enter the user name:"
		send "$AdminName\r"
		expect "Enter the password for user 'locadmin':"
		send "$AdminPW\r"
		expect EOF
		DONE
fi
}

FuncFVKey 2&> /tmp/FVKey.txt

FVKey=`cat /tmp/FVKey.txt | grep "Recovery key" | awk '{print $4}' | awk '{print substr($0, 2, length($0) - 3)}'`

#get the # from your JSS
JSSDiskEncryptionConfigurationID="1"

#either create a new one with "fdesetup changerecovery -personal -outputplist" and parse the key out with PListBuddy
#or perhaps you have a spreadsheet of the legacy keys and just need to import them to your new JSS
personalKey="$FVKey"

function ensureJSSUpdated
{
#some old code from JAMF (paths are wrong) - https://github.com/JAMFSupport/OSX_Scripts/blob/master/IndividualFV2KeyImport.sh

file_vault_2_id_xml='<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>FV2_ID</key>
        <string>'"$JSSDiskEncryptionConfigurationID"'</string>
    </dict>
</plist>'

file_vault_2_recovery_key_xml='<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>RecoveryKey</key>
        <string>'"$personalKey"'</string>
    </dict>
</plist>'

echo "${file_vault_2_recovery_key_xml}" > /Library/Application\ Support/JAMF/run/file_vault_2_recovery_key.xml
echo "${file_vault_2_id_xml}" > /Library/Application\ Support/JAMF/run/file_vault_2_id.xml

}

ensureJSSUpdated

jamf recon

