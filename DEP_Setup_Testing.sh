#!/bin/bash

mkdir -p /usr/local/scripts

cat > /usr/local/scripts/DEP_Setup.sh << "EOF"

#!/bin/bash

##########################################
# Base setup script for all Hogarth Macs #
##########################################

# Dont change this unless you know what your doing..you will break things... #

# V1.0 JF 04/2017 - testing
# V2.0 JF 02/2018 - Major re-write for 10.13
# New changes are designated by "# *** Change below"


clear # turn on debug mode 
set -x 

# Info:
# AppleScriptList.sh needs to run prior to this to allow for hostaname to be set
# The hostname needs to have been set prior to this running
# This will run the first time a user logs in
# Need to lock screen while this runs
# If possible use a "while" satament to refresh the lock screen to update status

tmutil snapshot

JSSURL="https://jamfpro.hogarthww.com:8443/"
JSSCONTACTTIMEOUT=10

jamf_binary="/usr/local/jamf/bin/jamf"

# Test the connection to the JSS
echo 'JSS - --- testing jss connection' >> /var/log/jamf.log
loop_ctr=1
while ! /usr/bin/curl --silent -o /dev/null --insecure ${JSSURL} ; do
   sleep 1;
    loop_ctr=$((loop_ctr+1))
    if [ $((loop_ctr % 10 )) -eq 0 ]; then
        echo "${loop_ctr} attempts" >> /var/log/jamf.log
    fi

    if [ ${loop_ctr} -eq ${JSSCONTACTTIMEOUT} ]; then
        echo "I'm bored ... giving up after ${loop_ctr} attempts" >> /var/log/jamf.log
          /bin/date
        exit 1
    fi
done

testjss

touch /Library/"Application Support"/Hogarth/lock.txt


# Flush Policy History
$jamf_binary flushPolicyHistory

# Deploy Recon, Apple and Hogath Images
$jamf_binary policy -event DeployHogarthLogo

# Create Hogarth folder structure
mkdir /usr/local/scripts/ 
chown root:wheel /usr/local/scripts/
chmod 777 /usr/local/scripts/

# Ask engineer for hostname
NEWHOSTNAME=`osascript -e 'Tell application "System Events" to display dialog "Please enter the hostname of the machine:" default answer ""' -e 'text returned of result'`

while [ 1 ]
do
if [ -z $NEWHOSTNAME ]; then
	NEWHOSTNAME=`osascript -e 'Tell application "System Events" to display dialog "You hit Cancel... Please enter the hostname of the machine again:" default answer ""' -e 'text returned of result'`
elif [ ! -z $NEWHOSTNAME ]; then
	break
fi
sleep 2
done

# Record hostname
touch /usr/local/scripts/originalhostname.txt
chmod 775 /usr/local/scripts/originalhostname.txt
chown root:admin /usr/local/scripts/originalhostname.txt
echo $NEWHOSTNAME > /usr/local/scripts/originalhostname.txt

echo "The hostname has been set to - $NEWHOSTNAME" > /var/log/jamf.log

scutil --set ComputerName $NEWHOSTNAME
scutil --set LocalHostName $NEWHOSTNAME
scutil --set HostName $NEWHOSTNAME
dscacheutil -flushcache

# Lock screen with active updates displayed
while :
do
# Status_Message=`tail -1 /var/log/jamf.log | head -1 | awk '{print $7, $8, $9}'`
Status_Message=`tail -1 /var/log/jamf.log | head -1`
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType fs -heading "Your Mac is being configured, please wait..." -description "Current Status: $Status_Message" -icon /usr/local/logos/Hogarth-White-Logo.png -timeout 10 &
sleep 9
kill -9 $(ps cx -o command,etime,pid | awk '/^jamfHelper/ {if (sec($2)>180) {print $3}}                               function sec(T){C=split(T,A,"[:-]"); return A[C>3?C-3:99]*86400 + A[C>2?C-2:99]*3600 + A[C>1?C-1:99]*60 + A[C>0?C-0:99]*1}')
done &

$jamf_binary recon

# Computer sleep: Never
/usr/bin/pmset -c sleep 0

# Set the locadmin back to Admin - to fix bug in 10.13
/usr/sbin/dseditgroup -o edit -a locadmin -t user admin

# Bind to the domain
dsconfigad -force -remove hogarthww.prv -username casper.jss -password Jasper99
dsconfigad -force -add hogarthww.prv -username casper.jss -password Jasper99 -ou "OU=NewComputers,OU=Computers,OU=HogarthWW,DC=hogarthww,DC=prv" -localhome enable -useuncpath enable -groups "Domain Admins,Enterprise Admins" -alldomains enable -mobile enable -mobileconfirm disable

# Enable location services
#launchctl unload /System/Library/LaunchDaemons/com.apple.locationd.plist

# Write enabled value to locationd plist
#defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd LocationServicesEnabled -int 1

# Fix Permissions for the locationd folder
chown -R _locationd:_locationd /var/db/locationd

# Reload locationd
launchctl load /System/Library/LaunchDaemons/com.apple.locationd.plist

# Get Current IP Adresses first two octets
IPAddress=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | cut -d'.' -f 1-2)

# Set Location variable
LOC=`echo $NEWHOSTNAME | cut -c 1-4`

# Define Region by Building Name
case $LOC in
	OGAB | ogab | TMCN | tmcn | YRMM | yrmm | HGPN | hgpn | YRCN | yrcn | JWLN | jwln | HGAS | hgas | CHTC | chtc | ELMN | elmn | HGPN | hgpn | HGPA | hgpa | LCMM | lcmm) 
	region="AMER"
	;;
	HGMD | hgmd | HGBH | hgbh | GRID | grid | HGSL | hgsl | HGPN | hgpn | HGGL | hggl | HOUL | houl | HGME | hgme | HGSC | hgsc | HGWJ | hgwj | RFAP | rfap | GRHL | grhl | GRUW | gruw | GRIM | grim | WMHL | wmhl | YRHL | yrhl | YRNP | yrnp | HGWJ | hgwj | OGGB | oggb | JWKL | jwkl | SAGM | sagm | METL | metl | GEWL | gewl | EYML | eyml | TJBL | tjbl | MEUL | meul | BAWL | bawl) 
	region="EMEA"
	;;
	OGRB | ogrb | JWHS | jwhs | HGSK | hgsk | HGSS | hgss | HGCS | hgcs | HGKH | hgkh | HGUG | hgug | GRMS | grms | YRCN | yrcn | OGQH | ogqh | OGRS | ogrs | OGBJ | ogbj | GRJS | grjs | OGRB | ogrb) 
	region="APAC"
esac

# Define Network Time
case $IPAddress in
	10.252 | 10.253 | 10.6 | 10.0 | 10.253 | 10.105 | 10.138 | 10.108 | 10.107 | 10.101 | 10.121 | 10.111 | 10.103 | 10.109 | 10.110 | 10.181) #London, Edinburgh, Milton Keynes
	timeZone="Europe/London"
	;;
	10.156) #Hamburg
	timeZone="Europe/Hamburg"
	;;
	10.152) #Paris
	timeZone="Europe/Paris"
	;;
	10.155) #Warsaw
	timeZone="Europe/Warsaw"
	;;
	10.157) #Dusseldorf
	timeZone="Europe/Dusseldorf"
	;;
	10.158) #Istanbul
	timeZone="Europe/Istanbul"
	;;
	10.154) #Prague
	timeZone="Europe/Prague"
	;;
	10.153) #Bucharest
	timeZone="Europe/Bucharest"
	;;
	10.210 | 10.211) #Cape Town, Johannesburg
	timeZone="Africa/Johannesburg"
	;;
	10.136 | 10.131 | 10.139 | 10.138) #New York, New Jersey
	timeZone='America/New_York'
	;;
	10.137) #Costa Mesa
	timeZone='America/Los_Angeles'
	;;
	10.1) #Miami
	timeZone="America/Miami"
	;;
	10.134) #Toronto
	timeZone="America/Toronto"
	;;
	10.202) #Sao Paulo
	timeZone='America/Sao_Paulo'
	;;
	10.170 | 10.171) #Mexico
	timeZone="America/Mexico"
	;;
	10.201) #Buenos Aires
	timeZone='America/Buenos_Aires'
	;;
	10.190) #Dubai
	timeZone="Asia/Dubai"
	;;
	10.143 | 10.141 | 10.147 | 10.144) #Singapore
	timezone="Asia/Singapore"
	;;
	10.149) #Kuala Lumpur
	timezone='Asia/Kuala_Lumpur'
	;;
	10.146) #Shanghai
	timezone="Asia/Shanghai"
	;;
	10.148 | 10.161) #Hong Kong
	timezone='Asia/Hong_Kong'
	;;
	10.2) #New Delhi
	timezone="Asia/Calcutta"
	;;
	10.145) #Jakarta
	timezone"Asia/Jakarta"
	;;
	*) 
	REGION='Unknown'
	echo $REGION
	;;
esac

# Set Time zones
systemsetup -settimezone $timeZone
systemsetup -setnetworktimeserver time.apple.com
systemsetup -setusingnetworktime on
sudo /usr/bin/defaults write /Library/Preferences/com.apple.timezone.auto Active -bool true
logger "Set Network Time Set"
defaults write com.apple.menuextra.clock "DateFormat" 'EEE d MMM hh:mm:ss'

# Show displays in menu bar
defaults write com.apple.airplay showInMenuBarIfPresent -bool false

# This will allow access to certain prefs for the user base
security authorizationdb write system.preferences allow
security authorizationdb write system.preferences.datetime allow
security authorizationdb write system.preferences.printing allow
security authorizationdb write system.preferences.energysaver allow
security authorizationdb write system.preferences.network allow
security authorizationdb write system.services.systemconfiguration.network allow

# Turn off DS_Store file creation on network volumes
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.desktopservices DSDontWriteNetworkStores true

# Disable external accounts (i.e. accounts stored on drives other than the boot drive.)
defaults write /Library/Preferences/com.apple.loginwindow EnableExternalAccounts -bool false

# Disable OS X OS Pre-release downloads for all users 
defaults write /Library/Preferences/com.apple.SoftwareUpdate AllowPreReleaseInstallation -bool false

# Disable “Application Downloaded from the internet” message
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.LaunchServices LSQuarantine -bool NO
defaults write com.apple.LaunchServices LSQuarantine -bool NO

# Disable the “Are you sure you want to open this application?” dialog
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.LaunchServices LSQuarantine -bool false

# Disable the save window state at logout
/usr/bin/defaults write com.apple.loginwindow 'TALLogoutSavesState' -bool false

# Disable Gatekeeper
spctl --master-disable

# Disable Quarantine Message
defaults write com.apple.LaunchServices LSQuarantine -bool NO

# Enable AirDrop over on all machines on all interfaces - confirm if required, off at present
sudo /usr/bin/defaults write com.apple.NetworkBrowser BrowseAllInterfaces 0

# Disable background application sleep
defaults write NSGlobalDomain NSAppSleepDisabled -bool YES

# Enable VNC
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -on -users locadmin -restart -agent -privs -all -clientopts -setvncpw -setmenuextra -menuextra no

# Enable SSH
systemsetup -setremotelogin on

# Desfine User prefs
defaults write /Library/Preferences/com.apple.loginwindow Hide500Users -bool true
defaults write /Library/Preferences/com.apple.loginwindow SHOWOTHERUSERS_MANAGED -bool FALSE
defaults write /Library/Preferences/com.apple.loginwindow HiddenUsersList -array
defaults write /Library/Preferences/com.apple.loginwindow HideAdminUsers -bool Yes
defaults write /Library/Preferences/com.apple.loginwindow Hide500Users -bool TRUE
defaults write /Library/Preferences/com.apple.loginwindow SHOWOTHERUSERS_MANAGED -bool FALSE
defaults write /Library/Preferences/com.apple.loginwindow HiddenUsersList -array
defaults write /Library/Preferences/com.apple.loginwindow HideAdminUsers -bool Yes
defaults write /Library/Preferences/com.apple.loginwindow MasterPasswordHint -string "Contact Servicedesk"
defaults write /Library/Preferences/com.apple.loginwindow showInputMenu -bool No
defaults write /Library/Preferences/com.apple.loginwindow SHOWFULLNAME -bool YES
defaults write /Library/Preferences/com.apple.loginwindow EnableExternalAccounts -bool false
defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName

# Trackpad & Mouse: Map bottom right corner to right-click and secondary button for Mouse
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true
defaults write "/System/Library/User Template/English.lproj/Library/Preferences/com.apple.driver.AppleBluetoothMultitouch.mouse" MouseButtonMode -string TwoButton
defaults write "/System/Library/User Template/English.lproj/Library/Preferences/com.apple.driver.AppleHIDMouse" Button1 -integer 1
defaults write "/System/Library/User Template/English.lproj/Library/Preferences/com.apple.driver.AppleHIDMouse" Button2 -integer 2

# Disable iTunes auto update
defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticCheckEnabled NO

# Hide Menu items
defaults write com.apple.systemuiserver menuExtras -array “/System/Library/CoreServices/Menu Extras/Bluetooth.menu” “/System/Library/CoreServices/Menu Extras/AirPort.menu” “/System/Library/CoreServices/Menu Extras/Battery.menu” “/System/Library/CoreServices/Menu Extras/Clock.menu”

# Set Safari "Auto open Downloads" off
defaults write /Library/Preferences/com.apple.Safari AutoOpenSafeDownloads -bool false

# Disable Time Machine snapshots on local disk
sudo tmutil disablelocal

# Expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

# Expand print panel by default
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

# Turn off Automatic updates
sudo softwareupdate --schedule off

# Disable the crash reporter
defaults write com.apple.CrashReporter DialogType -string "none"

# Disable disk image verification
defaults write com.apple.frameworks.diskimages skip-verify -bool true
defaults write com.apple.frameworks.diskimages skip-verify-locked -bool true
defaults write com.apple.frameworks.diskimages skip-verify-remote -bool true

# Give all end-users permissions full access to "Print & Scan, Network, Time" Preference Pane
security authorizationdb write system.preferences allow
security authorizationdb write system.preferences.sharing deny
security authorizationdb write system.preferences.printing allow
/usr/bin/security authorizationdb write system.print.operator allow
/usr/sbin/dseditgroup -o edit -n /Local/Default -a everyone -t group lpadmin
/usr/sbin/dseditgroup -o edit -n /Local/Default -a everyone -t group _lpadmin
/usr/sbin/dseditgroup -o edit -n /Local/Default -a 'Domain Users' -t group lpadmin

# Power Settings for all Users (Display Sleep, Workstation Sleep, Wake for network access) (Pmset -a = All Power modes | Pmset -c = A/C Power | Pmset -b = Battery Power)
pmset -a halfdim 1 gpuswitch 2 hibernatemode 1 lidwake 1 sms 1

# Automatically illuminate built-in MacBook keyboard in low light and turn off in idle after 5 minutes
defaults write com.apple.BezelServices kDim -bool true
defaults write com.apple.BezelServices kDimTime -int 300

# Terminal command-line access warning
/usr/bin/touch /etc/motd
/bin/chmod 644 /etc/motd
/bin/echo "" >> /etc/motd
/bin/echo "This Apple Workstation, including all related equipment belongs to Hogarth. Unauthorized access to this workstation is forbidden and will be prosecuted by law. By accessing this system, you agree that your actions may be monitored if unauthorized usage is suspected." >> /etc/motd
/bin/echo "" >> /etc/motd

# Turn Fast User Switching OFF
defaults write /Library/Preferences/.GlobalPreferences MultipleSessionEnabled -bool NO

# Configure Finder settings (List View, Show Status Bar, Show Path Bar)
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.finder "AlwaysOpenWindowsInListView" -bool true
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.finder ShowStatusBar -bool true
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.finder ShowPathbar -bool true

# Determine OS version
osvers=$(sw_vers -productVersion | awk -F. '{print $2}')
sw_vers=$(sw_vers -productVersion)

# Prohibit Go To Disk
defaults write "/System/Library/User Template/English.lproj/Library/Preferences/com.apple.finder.plist" ProhibitGoToiDisk -bool YES

# Expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode2 -bool true

# Expand print panel by default
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint -bool true
defaults write NSGlobalDomain PMPrintingExpandedStateForPrint2 -bool true

# Disable disk image verification
defaults write com.apple.frameworks.diskimages skip-verify -bool true
defaults write com.apple.frameworks.diskimages skip-verify-locked -bool true
defaults write com.apple.frameworks.diskimages skip-verify-remote -bool true

# Hide the following applications: Game Center, Time Machine, Boot Camp
sudo chflags hidden /Applications/Game\ Center.app/
sudo chflags hidden /Applications/Time\ Machine.app/
sudo chflags hidden /Applications/Utilities/Boot\ Camp\ Assistant.app/

# Setup Hogarth Network locations
networksetup -createlocation Hogarth populate
networksetup -switchtolocation Hogarth
networksetup -deletelocation Automatic

# Disable iCloud & Apple Assistant Popup for new user creation
# Determine OS version
osvers=$(sw_vers -productVersion | awk -F. '{print $2}')
sw_vers=$(sw_vers -productVersion)

# Determine OS build number

sw_build=$(sw_vers -buildVersion)

# Checks first to see if the Mac is running 10.7.0 or higher.
# If so, the script checks the system default user template
# for the presence of the Library/Preferences directory. Once
# found, the iCloud, Data & Privacy, Diagnostic and Siri pop-up
# settings are set to be disabled.

if [[ ${osvers} -ge 7 ]]; then

 for USER_TEMPLATE in "/System/Library/User Template"/*
  do
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeCloudSetup -bool TRUE
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant GestureMovieSeen none
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant LastSeenCloudProductVersion "${sw_vers}"
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant LastSeenBuddyBuildVersion "${sw_build}"
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeePrivacy -bool TRUE
    /usr/bin/defaults write "${USER_TEMPLATE}"/Library/Preferences/com.apple.SetupAssistant DidSeeSiriSetup -bool TRUE
  done

 # Checks first to see if the Mac is running 10.7.0 or higher.
 # If so, the script checks the existing user folders in /Users
 # for the presence of the Library/Preferences directory.
 #
 # If the directory is not found, it is created and then the
 # iCloud, Data & Privacy, Diagnostic and Siri pop-up settings
 # are set to be disabled.

 for USER_HOME in /Users/*
  do
    USER_UID=`basename "${USER_HOME}"`
    if [ ! "${USER_UID}" = "Shared" ]; then
      if [ ! -d "${USER_HOME}"/Library/Preferences ]; then
        /bin/mkdir -p "${USER_HOME}"/Library/Preferences
        /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library
        /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library/Preferences
      fi
      if [ -d "${USER_HOME}"/Library/Preferences ]; then
        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeeCloudSetup -bool TRUE
        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant GestureMovieSeen none
        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant LastSeenCloudProductVersion "${sw_vers}"
        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant LastSeenBuddyBuildVersion "${sw_build}"
        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeePrivacy -bool TRUE
        /usr/bin/defaults write "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant DidSeeSiriSetup -bool TRUE
        /usr/sbin/chown "${USER_UID}" "${USER_HOME}"/Library/Preferences/com.apple.SetupAssistant.plist
      fi
    fi
  done
fi

# Disable Apple Diagnostics window prompt
# Set whether you want to send diagnostic info back to
# Apple and/or third party app developers. If you want
# to send diagonostic data to Apple, set the following 
# value for the SUBMIT_DIAGNOSTIC_DATA_TO_APPLE value:
#
# SUBMIT_DIAGNOSTIC_DATA_TO_APPLE=TRUE
# 
# If you want to send data to third party app developers,
# set the following value for the
# SUBMIT_DIAGNOSTIC_DATA_TO_APP_DEVELOPERS value:
#
# SUBMIT_DIAGNOSTIC_DATA_TO_APP_DEVELOPERS=TRUE
# 
# By default, the values in this script are set to 
# send no diagnostic data: 

SUBMIT_DIAGNOSTIC_DATA_TO_APPLE=FALSE
SUBMIT_DIAGNOSTIC_DATA_TO_APP_DEVELOPERS=FALSE

if [[ ${osvers} -eq 10 ]]; then
  VERSIONNUMBER=4
elif [[ ${osvers} -ge 11 ]]; then
  VERSIONNUMBER=5
fi


# Checks first to see if the Mac is running 10.10.0 or higher. 
# If so, the desired diagnostic submission settings are applied.

if [[ ${osvers} -ge 10 ]]; then

  CRASHREPORTER_SUPPORT="/Library/Application Support/CrashReporter"
 
  if [ ! -d "${CRASHREPORTER_SUPPORT}" ]; then
    mkdir "${CRASHREPORTER_SUPPORT}"
    chmod 775 "${CRASHREPORTER_SUPPORT}"
    chown root:admin "${CRASHREPORTER_SUPPORT}"
  fi

 /usr/bin/defaults write "$CRASHREPORTER_SUPPORT"/DiagnosticMessagesHistory AutoSubmit -boolean ${SUBMIT_DIAGNOSTIC_DATA_TO_APPLE}
 /usr/bin/defaults write "$CRASHREPORTER_SUPPORT"/DiagnosticMessagesHistory AutoSubmitVersion -int ${VERSIONNUMBER}
 /usr/bin/defaults write "$CRASHREPORTER_SUPPORT"/DiagnosticMessagesHistory ThirdPartyDataSubmit -boolean ${SUBMIT_DIAGNOSTIC_DATA_TO_APP_DEVELOPERS}
 /usr/bin/defaults write "$CRASHREPORTER_SUPPORT"/DiagnosticMessagesHistory ThirdPartyDataSubmitVersion -int ${VERSIONNUMBER}
 /bin/chmod a+r "$CRASHREPORTER_SUPPORT"/DiagnosticMessagesHistory.plist
 /usr/sbin/chown root:admin "$CRASHREPORTER_SUPPORT"/DiagnosticMessagesHistory.plist

fi

# Disable Siri setup assistant
/usr/bin/defaults write com.apple.SetupAssistant "DidSeeSiriSetup" -bool true

### Install all base applications 

# install Jamf Pro Recon
$jamf_binary policy -event DeployCasperRecon

# install Avid QuickTime Codecs LE 2.5
$jamf_binary policy -event DeployAvidQTCodecs

# install Cocoa Dialog - is this required
$jamf_binary policy -event DeployCocoaDialog

# install IPinMenuBar
$jamf_binary policy -event DeployIPinMenuBar

# install Microsoft Remote
$jamf_binary policy -event DeployMicrosoftRemote

# install Microsoft Silverlight
$jamf_binary policy -event DeployMicrosoftSilverlight

# install MPEG Streamclip
$jamf_binary policy -event DeployMPEGStreamclip

# install Perian
$jamf_binary policy -event DeployPerian

# install Skype
$jamf_binary policy -event DeploySkype

# install Firefox
#$jamf_binary policy -event DeployMozillaFirefox

# install Chrome
#$jamf_binary policy -event DeployGoogleChrome

# install Spotify
$jamf_binary policy -event DeploySpotify

# install Casper Recon
$jamf_binary policy -event DeployCasperRecon

# install Microsoft Lync
$jamf_binary policy -event DeployMicrosoftSkypeForBusiness

# install Oracle Java Plugin
$jamf_binary policy -event DeployOracleJavaPlugin

# install Oracle Java 6
$jamf_binary policy -event DeployJava6Update

# install Maconomy Webloc
if [ ! $region = "APAC" ]; then
    $jamf_binary policy -event DeployMaconomyWebloc
fi

# Install MDS Network Utility
if [ ! $region = "AMER"]; then
$jamf_binary policy -event DeployMDSUtility
fi

# Install Dock Utility
$jamf_binary policy -event DeployDockutil

# Install Xerox Drivers Utility
$jamf_binary policy -event InstallXeroxPrintDrivers

# Install Toshiba Drivers Utility
$jamf_binary policy -event InstallToshiba

# Install Canon Drivers Utility
$jamf_binary policy -event InstallCanon

# Install HP Drivers Utility
$jamf_binary policy -event InstallHPPrintDrivers

# Install Epson Drivers Utility
$jamf_binary policy -event InstallEpsonPrintDrivers

# Install Outlook Config
$jamf_binary policy -event DeployOutlookConfig

# Install Webex
$jamf_binary policy -event DeployWebEx

# Install Adobe Reader
$jamf_binary policy -event DeployAdobeReaderDC

# Install Build data files
$jamf_binary policy -event DeployBuildDataFiles

# Install Signiant
$jamf_binary policy -event DeploySigniant

# Install EasyFind
$jamf_binary policy -event DeployEasyFind

# install Quicktime
$jamf_binary policy -event DeployQuicktime




# *** Changes below

# install Microsoft Office
$jamf_binary policy -event DeployMicrosoftOffice

# Install Update MAU
$jamf_binary policy -event DeployMAU4

# install Microsoft Office Updates
$jamf_binary policy -event AutoUpdateOffice

# install Microsoft Office Signin Config
$jamf_binary policy -event AutoConfigOfficeSignIn

# install Sophos AntiVirus
if [ $region = "EMEA" ]; then
$jamf_binary policy -event DeploySophosAntiVirusEMEA
fi

# install Sophos AntiVirus
if [ $region = "APAC" ]; then
$jamf_binary policy -event DeploySophosAntiVirusAPAC
fi

# Cancelling an unwanted FileVault deferred enablement
system_model=$(system_profiler SPHardwareDataType | grep 'Model Identifier:' | cut -d':' -f2 | sed 's/^ *//g')
if [[ "$system_model" == !*"MacBook"* ]]; then
    fdesetup disable
fi


# Set Hardware Variable
HW=`echo $NEWHOSTNAME | cut -c 7-8`
if [ $HW = "mb" ]; then
# install Junos Pulse
jamf policy -event DeployJunosPulse
# install RSA Secure ID
jamf policy -event DeployRSASecureID
fi

echo "Hogarth Baseprep Complete..." >> /var/log/jamf.log

# Sleep display to stop Screen Burn, until build completion.
sudo pmset displaysleepnow

# Set Deptartment Variable
DEPT=`echo $NEWHOSTNAME | cut -c 9-10`

# Set Location variable
LOC=`echo $NEWHOSTNAME | cut -c 1-4`

########## EMEA Build #############
### Install Elements Build ###
if [[ $DEPT == [Ee][Ll] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEAElementsBuild
echo "Configuring the EMEA Elements Build" >> /var/log/jamf.log

### Install Broadcast Build ###
elif [[ $DEPT == [Bb][Rr] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEABroadcastBuild &&
echo "Configuring the EMEA Broadcast Build" >> /var/log/jamf.log

### Install Computer Graphics Build ###
elif [[ $DEPT == [Cc][Gg] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEAComputerGraphicsBuild &&
echo "Configuring the EMEA Computer Graphics Build" >> /var/log/jamf.log

### Install Digital Build ###
elif [[ $DEPT == [Dd][Ii] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEADigitalBuild &&
echo "Configuring the EMEA Digital Build" >> /var/log/jamf.log

### Install Print Production Build ###
elif [[ $DEPT == [Pp][Pp] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEAPrintProductionBuild &&
echo "Configuring the EMEA Print Production Build" >> /var/log/jamf.log

### Install Audio Build ###
elif [[ $DEPT == [Aa][Uu] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEAAudioBuild &&
echo "Configuring the EMEA Audio Build" >> /var/log/jamf.log

### Install Subtitling Build ###
elif [[ $DEPT == [Ss][Tt] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEASubtitlingBuild
echo "Configuring the EMEA Subtitling Build" >> /var/log/jamf.log

### Install Admin Build ###
elif [[ $DEPT == [Aa][Dd] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event GlobalAdminBuild
echo "Configuring the EMEA Admin Build" >> /var/log/jamf.log

### Install IT Build ###
elif [[ $DEPT == [Ii][Tt] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEAITBuild
echo "Configuring the EMEA IT Build" >> /var/log/jamf.log

### Install IT Build ###
elif [[ $DEPT == [Tt][Tt] ]] && [[ $region == EMEA ]]; then
$jamf_binary policy -event EMEATTBuild
echo "Configuring the EMEA Technical Trainer Build" >> /var/log/jamf.log

fi


########## AMER Build #############
### Install Admin Build ###
if [[ $DEPT == [Aa][Dd] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event GlobalAdminBuild
echo "Configuring the EMEA Admin Build" >> /var/log/jamf.log

### Install Broadcast Build ###
elif [[ $DEPT == [Bb][Rr] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERBroadcastNonSANBuild &&
echo "Configuring the AMER Broadcast Build" >> /var/log/jamf.log

### Install Broadcast SAN Build ###
elif [[ $DEPT == [Bb][Ss] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERBroadcastSANBuild &&
echo "Configuring the AMER Broadcast SAN Build" >> /var/log/jamf.log

### Install Digital Build ###
elif [[ $DEPT == [Dd][Ii] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERDigitalBuild &&
echo "Configuring the AMER Digital Build" >> /var/log/jamf.log

### Install Tech Build ###
elif [[ $DEPT == [Ii][Tt] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERITBuild &&
echo "Configuring the AMER Tech Build" >> /var/log/jamf.log

### Install Print Non-Production Build ###
elif [[ $DEPT == [Pp][Nn] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERPrintNonProductionBuild &&
echo "Configuring the AMER Print Non-Production Build" >> /var/log/jamf.log

### Install Print Production Build ###
elif [[ $DEPT == [Pp][Pp] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERPrintProductionBuild &&
echo "Configuring the AMER Print Production Build" >> /var/log/jamf.log

### Install Producers Build ###
elif [[ $DEPT == [Pp][Rr] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERBroadcastProducersBuild &&
echo "Configuring the AMER Producers Build" >> /var/log/jamf.log

### Install Transcreation Build ###
elif [[ $DEPT == [Tt][Rr] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERTranscreationBuild &&
echo "Configuring the AMER Transcreation Build" >> /var/log/jamf.log

### Install Element Build ###
elif [[ $DEPT == [Ee][Ll] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERElementBuild &&
echo "Configuring the AMER Element Build" >> /var/log/jamf.log

### Install Audio Build ###
elif [[ $DEPT == [Aa][Uu] ]] && [[ $region == AMER ]]; then
$jamf_binary policy -event AMERAudioBuild &&
echo "Configuring the AMER Audio Build" >> /var/log/jamf.log

fi


########## APAC Build #############
### Install Admin Build ###
if [[ $DEPT == [Aa][Dd] ]] && [[ $region == APAC ]]; then
$jamf_binary policy -event GlobalAdminBuild
echo "Configuring the EMEA Admin Build" >> /var/log/jamf.log

### Install Broadcast Build ###
elif [[ $DEPT == [Bb][Rr] ]] && [[ $region == APAC ]]; then
$jamf_binary policy -event APACBroadcastBuild &&
echo "Configuring the APAC Broadcast Build" >> /var/log/jamf.log

### Install Print Production Build ###
elif [[ $DEPT == [Pp][Pp] ]] && [[ $region == APAC ]]; then
$jamf_binary policy -event APACPrintProductionBuild &&
echo "Configuring the APAC Print Production Build" >> /var/log/jamf.log

### Install Element General Build ###
elif [[ $DEPT == [Ee][Gg] ]] && [[ $region == APAC ]]; then
$jamf_binary policy -event APACElementGeneralBuild &&
echo "Configuring the APAC Element General Build" >> /var/log/jamf.log

### Install Element Developers Build ###
elif [[ $DEPT == [Ee][Dd] ]] && [[ $region == APAC ]]; then
$jamf_binary policy -event APACElementDevelopersBuild &&
echo "Configuring the APAC Element Developers Build" >> /var/log/jamf.log

### Install Tech Build ###
elif [[ $DEPT == [Ii][Tt] ]] && [[ $region == APAC ]]; then
$jamf_binary policy -event APACITBuild &&
echo "Configuring the APAC Tech Build" >> /var/log/jamf.log

fi

# *** Change below

# Set Volume in Menu bar
#cat <<\EOF > /Library/LaunchDaemons/com.hogarthww.Volume.plist
#<?xml version="1.0" encoding="UTF-8"?>
#<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
#<plist version="1.0">
#<dict>
#	<key>AbandonProcessGroup</key>
#	<true/>
#	<key>Label</key>
#	<string>com.Hogarthww.volume.agent</string>
#	<key>Nice</key>
#	<integer>20</integer>
#	<key>ProgramArguments</key>
#	<array>
#		<string>/bin/bash</string>
#		<string>-c</string>
#		<string>currentuser=`ls -l /dev/console | awk '{ print $3 }'` ; defaults write /Users/$currentuser/Library/Preferences/com.apple.systemuiserver menuExtras -array-add "/System/Library/CoreServices/Menu Extras/Volume.menu";killall SystemUIServer -HUP</string>
#	</array>
#	<key>RunAtLoad</key>
#	<true/>
#</dict>
#</plist>
#EOF

#chown root:wheel /Library/LaunchDaemons/com.hogarthww.Volume.plist
#chmod 644 /Library/LaunchDaemons/com.hogarthww.Volume.plist

# Set screensaver
defaults write /System/Library/User\ Template/Non_localized/Library/Preferences/com.apple.screensaver askForPassword -int 1
defaults write /System/Library/User\ Template/Non_localized/Library/Preferences/com.apple.screensaver askForPasswordDelay -int 1
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.screensaver askForPasswordDelay -int 24
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.screensaver askForPassword -bool true
#set the delay in seconds, 0 = immediate
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.screensaver askForPasswordDelay 0
#set the screen saver delay in seconds, 300 = 5 minutes
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.screensaver idleTime 600
#set hot corners bottom right
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.dock "wvous-br-corner" -int 5
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.dock "wvous-br-modifier" -int 0

### Display sleep: 10 min
/usr/bin/sudo /usr/bin/pmset -c displaysleep 10

### Cleanup ####

# Tags Mac as new build for Smart Groups
sudo touch /Library/Application\ Support/JAMF/Receipts/DEPbuildversion2018.pkg

networksetup -removepreferredwirelessnetwork $WirelessPort "Hogarth BUILD"

#/usr/sbin/softwareupdate -ia >> /var/log/jamf.log 2>&1

#wait 10

#/usr/sbin/softwareupdate -ia >> /var/log/jamf.log 2>&1

rm /Library/"Application Support"/Hogarth/lock.txt

echo "Hogarth Baseprep finished... Rebooting now!" >> /var/log/jamf.log

#Create TM snapshot for instantaneous restore to role base build state
tmutil snapshot

shutdown -r now

==end

#Change permissions on files
/usr/sbin/chown root:wheel /usr/local/scripts/
/usr/sbin/chown root:wheel /usr/local/scripts/DEP_Setup.sh
/bin/chmod 755 /usr/local/scripts/DEP_Setup.sh
EOF

sh /usr/local/scripts/DEP_Setup.sh