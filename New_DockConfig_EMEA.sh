#!/bin/bash

######################################
# Generic Configuration for the Dock #
######################################

cat <<\EOF > /Library/LaunchAgents/com.hogarthww.EMEA_Docks.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>com.hogarthww.EMEA_Docks</string>
	<key>Nice</key>
	<integer>1</integer>
	<key>ProgramArguments</key>
	<array>
		<string>sh</string>
		<string>-c</string>
		<string>/usr/local/scripts/EMEA_All_Docks_Config.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>
EOF

cat <<\EOF > /usr/local/scripts/EMEA_ALL_Docks_Config.sh
#!/bin/bash

# Set Var
DEPT=`echo $HOSTNAME | cut -c 9-10`
CUSER=`stat -f "%Su" /dev/console`

# Ref: https://www.jamf.com/jamf-nation/discussions/28294/dockutil-cannot-get-rid-of-siri-ibooks-or-maps#responseChild178617

breadcrumb="$HOME/Library/Preferences/org.company.hogarthww.plist"
log="$HOME/Library/Logs/DockBuilder.log"
scriptName=$(basename "$0")
icon="/System/Library/CoreServices/Dock.app/Contents/Resources/Dock.icns"

# Array to hold all the items we need to add
# Note: if you need to add options you must seperate the item from the options with a @ symbol
# Example: "'/Library/Connect to...'@--view list --display folder --sort name"
declare -a itemsToAdd=(
    "/Applications/System Preferences.app"
    )

function writelog () {
    DATE=$(date +%Y-%m-%d\ %H:%M:%S)
    /bin/echo "${1}"
    /bin/echo "$DATE" " $1" >> "$log"
}

function finish () {
    kill "$jamfHelperPID" 2>/dev/null; wait "$jamfHelperPID" 2>/dev/null
    writelog "======== Finished $scriptName ========"
    exit "$1"
}

function modify_dock () {
    for item in "${itemsToAdd[@]}"; do
        if [[ "$item" =~ @ ]]; then
            params=${item##*@}
            item=${item%@*}
            /usr/local/bin/dockutil --add "$item" $params --no-restart "$HOME/Library/Preferences/com.apple.dock.plist" 2>&1 | while read -r LINE; do writelog "$LINE"; done;
        else
            /usr/local/bin/dockutil --add "$item" --no-restart "$HOME/Library/Preferences/com.apple.dock.plist" 2>&1 | while read -r LINE; do writelog "$LINE"; done;
        fi
    done
}

create_breadcrumb () {
    # Create a breadcrumb to track the creation of the Dock
    writelog "Creating DockBuilder user breadcrumb."
    /usr/bin/defaults write "$breadcrumb" build-date "$(date +%m-%d-%Y)"
    /usr/bin/defaults write "$breadcrumb" build-time "$(date +%r)"
}

writelog " "
writelog "======== Starting $scriptName ========"

# Make sure DockUtil is installed
if [[ ! -f "/usr/local/bin/dockutil" ]]; then
    writelog "DockUtil does not exist, exiting."
    finish 1
fi

# We need to wait for the Dock to actually start
until [[ $(pgrep -x Dock) ]]; do
    wait
done

# Check to see if the Dock was previously set up for the user
if [[ -f "$breadcrumb" ]]; then
    writelog "DockBuilder ran previously on $(defaults read "$breadcrumb" build-date) at $(defaults read "$breadcrumb" build-time)."
    finish 0
fi

# Display a dialog box to user informing them that we are configuring their Dock (in background)
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "DockBuilder" -icon "$icon" -description "Your Mac's Dock is being built for the first time." &
jamfHelperPID=$!

# Hide the Dock while it is being updated
/usr/bin/defaults write "$HOME/Library/Preferences/com.apple.dock.plist" autohide -bool TRUE
/usr/bin/killall Dock
/bin/sleep 2

writelog "Clearing Dock."
/usr/local/bin/dockutil --remove all --no-restart "$HOME/Library/Preferences/com.apple.dock.plist" 2>&1 | while read -r LINE; do writelog "$LINE"; done;
/bin/sleep 5

### Config Admin Dock ###
if [[ -f $HOME/Library/Preferences/org.company.hogarthww.plist ]]; then
        exit
                elif [[ "$CUSER" = locadmin ]]; then
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Utilities/Activity Monitor.app/")
						itemsToAdd+=("/Applications/Utilities/Console.app/")
						itemsToAdd+=("/Applications/Utilities/Disk Utility.app/")
						itemsToAdd+=("/Applications/Utilities/Keychain Access.app/")
						itemsToAdd+=("/Applications/Utilities/System Information.app/")
						itemsToAdd+=("/Applications/Utilities/Terminal.app/")
						itemsToAdd+=("/System/Library/CoreServices/Applications/Directory Utility.app/")
						itemsToAdd+=("/Applications/System Preferences.app/")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Recon.app/")
						itemsToAdd+=("/Applications/Self Service.app/")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
fi

### Config Administration Dock ###
if [[ "$DEPT" == [Aa][Dd] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f $HOME/Library/Preferences/org.company.hogarthww.plist ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Microsoft Outlook.app/")
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/Microsoft OneNote.app")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Config Broadcast Dock ###
if [[ "$DEPT" == [Bb][Rr] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Path Finder.app/")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Farmers WIFE.app")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/MAXON/CINEMA 4D R18/CINEMA 4D TeamRender Client.app")
						itemsToAdd+=("/Applications/Utilities/QuickTime Player 7.app")
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Digital Build ###
if [[ "$DEPT" == [Dd][Ii] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Microsoft Outlook.app/")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/Adobe Photoshop CC 2017/Adobe Photoshop CC 2017.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2017/Adobe InDesign CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Animate CC 2017/Adobe Animate CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Dreamweaver CC 2017/Adobe Dreamweaver CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2017/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2017/Adobe After Effects CC 2017.app")
						itemsToAdd+=("/Applications/Cyberduck.app")
						itemsToAdd+=("/Applications/Spotify.app")
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Element Build ###
if [[ "$DEPT" == [Ee][Ll] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Microsoft Outlook.app/")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2017/Adobe InDesign CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Photoshop CC 2017/Adobe Photoshop CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2017/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2017/Adobe After Effects CC 2017.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2018/Adobe InDesign CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2018/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe Adobe After Effects CC 2018/Adobe After Effects CC 2018.app")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Spotify.app")
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

## Install Print Production Build ###
if [[ "$DEPT" == [Pp][Pp] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app/")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Automation Engine Client 16.0/Shuttle.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")
						itemsToAdd+=("/Applications/Adobe Bridge CC 2017/Adobe Bridge CC 2017.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2017/Adobe InDesign CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Photoshop CC 2017/Adobe Photoshop CC 2017.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2017/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/PDF Comparator 4.app")
						itemsToAdd+=("/Applications/Microsoft Remote Desktop.app/")
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Cyberduck.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Audio Build ###
if [[ "$DEPT" == [Aa][Uu] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")
						itemsToAdd+=("/Applications/Microsoft Teams.app")
						itemsToAdd+=("/Applications/OneDrive.app")
						itemsToAdd+=("/Applications/Farmers WIFE.app")
						itemsToAdd+=("/Applications/iTunes.app")
						itemsToAdd+=("/Applications/iLok License Manager.app")
						itemsToAdd+=("/Applications/Pro Tools.app")
						itemsToAdd+=("/Applications/SMHD+.app/")
						itemsToAdd+=("/Applications/Source-Connect Pro.app")
						itemsToAdd+=("/Applications/Izotope RX 6 Audio Editor.app")
						itemsToAdd+=("/Applications/Waves Central.app")
						itemsToAdd+=("/Applications/Adobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2018/Adobe Premiere Pro CC 2018.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2018/Adobe After Effects CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Media Encoder CC 2018/Adobe Media Encoder CC 2018.app")
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Subtitling Build ###
if [[ "$DEPT" == [Ss][Tt] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
							itemsToAdd+=("/Applications/Safari.app")
							itemsToAdd+=("/Applications/Microsoft Outlook.app")
							itemsToAdd+=("/Applications/Skype for Business.app")
							itemsToAdd+=("/Applications/Cyberduck.app")
							itemsToAdd+=("/Applications/GoPro Studio.app")
							itemsToAdd+=("/Applications/Adobe Bridge CC 2017/Adobe Bridge CC 2017.app")
							itemsToAdd+=("/Applications/Adobe InDesign CC 2017/Adobe InDesign CC 2017.app")
							itemsToAdd+=("/Applications/Adobe Photoshop CC 2017/Adobe Photoshop CC 2017.app")
							itemsToAdd+=("/Applications/Adobe Illustrator CC 2017/Adobe Illustrator.app")
							itemsToAdd+=("/Applications/Adobe After Effects CC 2017/Adobe After Effects CC 2017.app")
							itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2017/Adobe Premiere Pro CC 2017.app")
							itemsToAdd+=("/Applications/Spotify.app")
							itemsToAdd+=("/Applications/System Preferences.app")
							itemsToAdd+=("/Applications/Self Service.app")
		fi
fi

### Install Technical Trainer Build ###
if [[ "$DEPT" == [Tt][Tt] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
							itemsToAdd+=("/Applications/Safari.app")
							itemsToAdd+=("/Applications/Microsoft Outlook.app")
							itemsToAdd+=("/Applications/Skype for Business.app")
							itemsToAdd+=("/Applications/Adobe InDesign CC 2018/Adobe InDesign CC 2018.app")
							itemsToAdd+=("/Applications/Adobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
							itemsToAdd+=("/Applications/Adobe Illustrator CC 2018/Adobe Illustrator.app")
							itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2018/Adobe Premiere Pro CC 2018.app")
							itemsToAdd+=("/Applications/Camtasia 3.app")
							itemsToAdd+=("/Applications/Jing.app")
							itemsToAdd+=("/Applications/OmniPlan.app")
							itemsToAdd+=("/Applications/Spotify.app")
							itemsToAdd+=("/Applications/System Preferences.app")
							itemsToAdd+=("/Applications/Self Service.app")
		fi
fi

### Config Tech Dock ###
if [[ "$DEPT" == [Ii][Tt] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
							itemsToAdd+=("/Applications/Safari.app")
							itemsToAdd+=("/Applications/Microsoft Outlook.app")
							itemsToAdd+=("/Applications/Microsoft Word.app")
							itemsToAdd+=("/Applications/Microsoft Excel.app")
							itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
							itemsToAdd+=("/Applications/Skype for Business.app")
							itemsToAdd+=("/Applications/Microsoft OneNote.app")
							itemsToAdd+=("/Applications/Skype for Business.app")
							itemsToAdd+=("/Applications/Utilities/Activity Monitor.app")
							itemsToAdd+=("/Applications/Utilities/Console.app")
							itemsToAdd+=("/Applications/Utilities/Disk Utility.app")
							itemsToAdd+=("/Applications/Utilities/Keychain Access.app")
							itemsToAdd+=("/Applications/Utilities/System Information.app")
							itemsToAdd+=("/Applications/Utilities/Terminal.app")
							itemsToAdd+=("/System/Library/CoreServices/Applications/Directory Utility.app/")
							itemsToAdd+=("/Applications/Jamf Pro Recon Switch.app")
							itemsToAdd+=("/Applications/Jamf Pro Remote Switch.app")
							itemsToAdd+=("/Applications/System Preferences.app")
							itemsToAdd+=("/Applications/Self Service.app")
		fi
fi

modify_dock
create_breadcrumb

# Reset the Dock and kill the jamfHelper dialog box
writelog "Resetting Dock."
/usr/bin/defaults write "$HOME/Library/Preferences/com.apple.dock.plist" autohide -bool FALSE
/usr/bin/killall Dock

finish 0

EOF

# Change permissions
# plist to 644, script to 755

/usr/sbin/chown root:wheel /Library/LaunchAgents/com.hogarthww.EMEA_Docks.plist
/bin/chmod 644 /Library/LaunchAgents/com.hogarthww.EMEA_Docks.plist
/usr/sbin/chown root:wheel /usr/local/scripts/EMEA_ALL_Docks_Config.sh
/bin/chmod 755  /usr/local/scripts/EMEA_ALL_Docks_Config.sh