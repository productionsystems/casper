#! /bin/bash

DATE=`date`

set -x

# This script will rsync the Netboot nbi to all avaiable global NetBoot servers
# Ensure when run that the exact name for the .nbi is used 

JSSserver=https://10.252.32.160:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiadmin # this user requires TBC access privs in API settings on JSS 
JSSapipw=LBG-EaV-eDH-qg2

curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/distributionpoints

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

#Get NB ID's from xml
cat /tmp/all.xml | grep "<id>" | sort -t ">" -k2 -g | sed "s/^[ \t]*//" | sed 's*<id>**g' | sed 's*</id>**g' | sed '/59/d' > /tmp/nbid.txt

#echo "Hello $USER, please enter the full name of the new NetBoot image [ENTER]: "
#read 

#NEWNBILOC="/opt/hogarth/CasperShareIXLL2/"

#Checks for RSYNC Lock file, if file exists, rsync will not run. If file is not there, rsync will run.
if [ -e /home/cssadmin/rsyncjob.lock ]
then
        echo "RSYNC job already running.....Exiting"
exit 0
fi

#Create Rsync Lock File
touch /home/cssadmin/rsyncjob.lock

#Start Script
while read id 
do
curl -H "Accept: application/xml" -s -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/distributionpoints/id/$id | xmllint --format - > /tmp/nb.txt 
IP=`grep "<ip_address>" /tmp/nb.txt | sed 's*<ip_address>**g' | sed 's*</ip_address>**g' | sed 's/^.\{2\}//'`
name=`grep "<name>" /tmp/nb.txt | sed 's*<name>**g' | sed 's*</name>**g' | sed 's/^.\{2\}//'`

#Syncs Casper DP's with London's Main IXLLVMVA28 server
ping -c 1 10.143.32.133
if [ $? -eq 0 ]; then
        rsync -rauPH > /var/log/JamfProDistributionGSWS"_RSYNC-Log_"$(date +%Y%m%d).txt --delete-excluded /opt/hogarth/CasperShareIXLL2/ cssadmin@"10.143.32.133":/opt/hogarth/JamfProDistributionGSWS/
        echo "RSYNC London DP to $name Completed" | mutt -a /var/log/JamfProDistributionGSWS"_RSYNC-Log_"$(date +%Y%m%d).txt -s "RSYNC London DP to $name Completed" -- andrew.hayward@hogarthww.com, jason.flory$
fi


done </tmp/nbid.txt

#Find logs older than 5 days and delete
find /var/log/CasperShare* -mtime +1 -exec rm {} \;

#Remove Lock File
rm /home/cssadmin/rsyncjob.lock

exit 0