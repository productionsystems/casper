#!/bin/bash

#create dialog to request from list of jss
#change plist to correct url
#open correct version of casperadmin

CurrentUser=`echo $USER`

`/usr/bin/osascript << EOT
tell application "Finder"
activate
display dialog "Hogarth have two different JSS Servers, one of which is for current production work and the other which is for DEP enrolment. This App has been created to make administration of the two enviroments easier.

When you select 'Continue' you will be presented with two options 'JSS' and 'Dep_JSS', selecting either will launch the correct version for your server.

Please ensure you have both versions of Casper Admin installed. Use the naming conventions listed below: 

Applications/Casper Suite 9.96/Casper Admin
Applications/Casper Suite 9.97/Casper Admin

Once the above has been done please press Continue!" buttons {"Continue"} with icon {"/Library/Application Support/Hogarth/hogarth.jpeg"}
-- choose from list cancel button returns false, not -128, as do other choose syntax do.
if Reagion is false then error number -128 -- user canceled
end tell
return (posix path of Reagion)
EOT
exit`

Server=`/usr/bin/osascript << EOT
tell application "Finder"
activate
set Server to (choose from list {"JSS", "Dep_JSS"} with prompt "Please select the JSS you want to work with, Casper Admin will quit and relaunch to you selected server" default items "OK" OK button name {"OK"})
--- choose from list cancel button returns false, not -128, as do other choose syntax do.
if Server is false then error number -128 -- user canceled
end tell
return (posix path of Server)
EOT
exit`      

osascript -e 'tell app "Casper Admin" to quit'

Server=`echo ${Server:1}`

if [[ $Server = JSS ]]; then
		/usr/libexec/PlistBuddy -c "delete :url https://dep_jss.hogarthww.com:8443/" "/Users/$CurrentUser/Library/Preferences/com.jamfsoftware.jss.plist"
		/usr/libexec/PlistBuddy -c "add :url string https://casper.hogarthww.com:8443/" "/Users/$CurrentUser/Library/Preferences/com.jamfsoftware.jss.plist"
		/usr/bin/open /Applications/Casper\ Suite\ 9.96/Casper\ Admin.app
	exit
else
	if [[ $Server = Dep_JSS ]]; then
		/usr/libexec/PlistBuddy -c "delete :url https://casper.hogarthww.com:8443/" "/Users/$CurrentUser/Library/Preferences/com.jamfsoftware.jss.plist"
		/usr/libexec/PlistBuddy -c "add :url string https://dep_jss.hogarthww.com:8443/" "/Users/$CurrentUser/Library/Preferences/com.jamfsoftware.jss.plist"
		/usr/bin/open /Applications/Casper\ Suite\ 9.97/Casper\ Admin.app
	fi
fi

exit 0