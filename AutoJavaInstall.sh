#!/bin/bash



# Expect commands
expect <<- DONE
set timeout -1
spawn /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 
expect "Press RETURN to continue or any other key to abort:"
send "\r"
expect EOF
DONE

brew cask install java

exit 0