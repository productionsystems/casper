#!/bin/bash

set -x

AdminPW="Mash2bash!"
CONSOLE_USER=`who |grep console| awk '{print $1}'`

if [ "$CONSOLE_USER" = "locadmin" ]; then
	
#Notification to state fix in progress for IT
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "This tool will sync your Hogarth password with FileVault.

Please input the users AD name and password in the next two fields." -button1 'OK' > /dev/null 2>&1

ADUser=`osascript -e 'Tell application "System Events" to display dialog "Please enter the users AD Credentials:" default answer ""' -e 'text returned of result'`

TokenStatus=`sysadminctl -secureTokenStatus locadmin > 2>$1 > /tmp/STS.txt | awk '{print &7}'`

if [ "$TokenStatus" = "DISABLED" ]; then
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "Locadmin dosen't have a Secure Token. 
	
	Please backup the the users data to OneDrive and rebuilt the machine to Mojave. Without a Secure Token for locadmin being present the machine is in a broken state, this is a bug in 10.13." -button1 'OK' 
	exit
fi


while true; 
do
	UsersPW1=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter the users password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
	UsersPW2=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please confirm the users password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
		if [ "$UsersPW1" = "$UsersPW2" ]; then
			UsersPW=`echo "$UsersPW2"`
			break
		else
			/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "Your passwords do not match, please try again" -button1 'OK' > /dev/null 2>&1
		fi 
done

# Expect commands
fdesetup remove -user "$ADUser"
expect <<- DONE
set timeout -1
spawn fdesetup add -usertoadd "$ADUser"
expect "Enter the user name:"
send "locadmin\r"
expect "Enter the password for user 'locadmin':"
send "$AdminPW\r"
expect "Enter the password for the added user '$ADUser':"
send "$UsersPW\r"
expect EOF
DONE

ps axco pid,command | grep jamfHelper | awk '{ print $1; }' | xargs kill -9

else

#Notification to state fix in progress for Users
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "This tool will sync your Hogarth password with FileVault.

Please input your password in the next field." -button1 'OK' > /dev/null 2>&1

TokenStatus=`sysadminctl -secureTokenStatus locadmin > 2>$1 > /tmp/STS.txt | awk '{print &7}'`

if [ "$TokenStatus" = "DISABLED" ]; then
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "Locadmin dosen't have a Secure Token. 
	
	Please backup the your data to OneDrive and contact IT Support to arrange for your machine to be rebuilt to Mojave. 
	
	Without a Secure Token being present for the main admin account your machine is in a broken state, this is a bug in 10.13." -button1 'OK' 
	exit
fi

while true; 
do
	UsersPW1=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter your Hogarth email password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
	UsersPW2=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please confirm your Hogarth email password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
		if [ "$UsersPW1" = "$UsersPW2" ]; then
			UsersPW=`echo "$UsersPW2"`
			break
		else
			/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "Your passwords do not match, please try again" -button1 'OK' > /dev/null 2>&1
		fi 
	done

# Expect commands
fdesetup remove -user "$CONSOLE_USER"
expect <<- DONE
set timeout -1
spawn fdesetup add -usertoadd "$CONSOLE_USER"
expect "Enter the user name:"
send "locadmin\r"
expect "Enter the password for user 'locadmin':"
send "$AdminPW\r"
expect "Enter the password for the added user '$CONSOLE_USER':"
send "$UsersPW\r"
expect EOF
DONE

ps axco pid,command | grep jamfHelper | awk '{ print $1; }' | xargs kill -9

fi