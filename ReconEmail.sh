#!/bin/sh

CurrentUser=`ls -l /dev/console | awk '{ print $3 }'`
if [ "($CurrentUser)" = "root" ]; then echo "Running as root which means no logged in user. Exit now" 
	1>&2 echo "<result>NA</result>" exit 1
else 
	email=`dscl "/Active Directory/HOGARTHWW/All Domains/" -read /Users/$CurrentUser | grep EMailAddress | awk '{ print $2 }'`
	echo "<result>$email</result>"
fi

jamf recon -email $email