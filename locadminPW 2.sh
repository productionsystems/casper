#!/bin/bash

set -x

cat <<\EOF > /Library/LaunchDaemons/net.Hogarthww.locadminPW.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>net.Hogarthww.locadminPW</string>
	<key>ProgramArguments</key>
	<array>
		<string>/usr/local/scripts/locadminPW.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>
EOF



cat <<EOF > /usr/local/scripts/locadminPW.sh
#!/bin/bash
touch /usr/local/scripts/locadminPW.log
sysadminctl -adminUser locadmin -adminPassword Bash2mash -resetPasswordFor locadmin -newPassword Pish2tosh 2>&1 /usr/local/scripts/locadminPW.log

#Destroy itself after activation
rm -f /Library/LaunchDaemons/net.Hogarthww.locadminPW.plist
rm -f /usr/local/scripts/locadminPW.sh

EOF

/usr/sbin/chown root:wheel /Library/LaunchDaemons/net.Hogarthww.locadminPW.plist
/bin/chmod 644 /Library/LaunchDaemons/net.Hogarthww.locadminPW.plist
/usr/sbin/chown root:wheel /usr/local/scripts/locadminPW.sh
/bin/chmod 710 /usr/local/scripts/locadminPW.sh
/bin/chmod +x /usr/local/scripts/locadminPW.sh

launchctl load -w /Library/LaunchDaemons/net.Hogarthww.locadminPW.plist
