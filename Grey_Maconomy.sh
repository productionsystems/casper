#!/bin/bash

set -x

FF_PS=`ps aux | grep Firefox.app | grep -v grep | grep -v plugin | awk '{ print $2 }'`

kill -9 "$FF_PS"

#Variables
CurrentUser=`ls -la /dev/console | cut -d " " -f 4`
ProfDef=`cat /Users/$CurrentUser/Library/Application\ Support/Firefox/profiles.ini | grep Path | cut -d'/' -f2 | cut -d'.' -f1`
prefsfile=`echo /Users/$CurrentUser/Library/Application\ Support/Firefox/Profiles/$ProfDef.default/prefs.js`

#Preserve current configuration
cp /Users/$CurrentUser/Library/Application\ Support/Firefox/Profiles/$ProfDef.default/prefs.js /Users/$CurrentUser/Library/Application\ Support/Firefox/Profiles/$ProfDef.default/prefs.bak.js

#Set updates to false and define Proxy settings
echo "user_pref(\"app.update.enabled\", false);" >> "$prefsfile"
echo "user_pref(\"app.update.auto\", false);" >> "$prefsfile"
echo "user_pref(\"network.proxy.ftp\", \"agency.erp.wpp.com\");" >> "$prefsfile"
echo "user_pref(\"network.proxy.ftp_port\", 443);" >> "$prefsfile"
echo "user_pref(\"network.proxy.http\", \"agency.erp.wpp.com\");" >> "$prefsfile"
echo "user_pref(\"network.proxy.http_port\", 443);" >> "$prefsfile"
echo "user_pref(\"network.proxy.no_proxies_on\", \"localhost, 127.0.0.1, agency.erp.wpp.com\");" >> "$prefsfile"
echo "user_pref(\"network.proxy.share_proxy_settings\", true);" >> "$prefsfile"
echo "user_pref(\"network.proxy.socks\", \"agency.erp.wpp.com\");" >> "$prefsfile"
echo "user_pref(\"network.proxy.socks_port\", 443);" >> "$prefsfile"
echo "user_pref(\"network.proxy.ssl\", \"agency.erp.wpp.com\");" >> "$prefsfile"
echo "user_pref(\"network.proxy.ssl_port\", 443);" >> "$prefsfile"
echo "user_pref(\"network.proxy.type\", 1);" >> "$prefsfile"

#Open the correct URL
/Library/Application\ Support/Firefox/Firefox.app/Contents/MacOS/firefox --no-default-browser-check --private --no-first-run "https://agency.erp.wpp.com/cgi-bin/Maconomy/MaconomyPortal.wpp02mac.MASWPP.exe/Framework/framework/main.msc?sessionid=Seeb7"

#Recover users original configuration
mv /Users/$CurrentUser/Library/Application\ Support/Firefox/Profiles/$ProfDef.default/prefs.bak.js /Users/$CurrentUser/Library/Application\ Support/Firefox/Profiles/$ProfDef.default/prefs.js

exit 0
