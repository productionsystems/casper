#!/bin/bash

set -x

cat <<\EOF > /Library/LaunchDaemons/net.Hogarthww.SecurePipes.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>net.Hogarthww.SecurePipes</string>
		<key>ProgramArguments</key>
		<array>
			<string>/bin/sh</string>
			<string>/usr/local/scripts/SecurePipes.sh</string>
		</array>
		<key>RunAtLoad</key>
		<true/>
	</dict>
</plist>
EOF

cat <<\EOF > /usr/local/scripts/SecurePipes.sh
#!/bin/bash

cd /Users/
for path in * ;
do
cp /tmp/fSecurePipe/oxyproxylist.json $path/Desktop/ > /dev/null 2>&1
cp /tmp/SecurePipe/net.edgeservices.connections.plist $path/Library/Preferences > /dev/null 2>&1
cp /tmp/SecurePipe/net.edgeservices.Secure-Pipes.plist $path/Library/Preferences > /dev/null 2>&1
cp /tmp/SecurePipe/net.edgeservices.sp-config.plist $path/Library/Preferences > /dev/null 2>&1

done
EOF

/usr/sbin/chown root:wheel /Library/LaunchDaemons/net.Hogarthww.SecurePipes.plist
/bin/chmod 644 /Library/LaunchDaemons/net.Hogarthww.SecurePipes.plist
/usr/sbin/chown root:wheel /usr/local/scripts/SecurePipes.sh
/bin/chmod 755 /usr/local/scripts/SecurePipes.sh

/bin/chmod +x /usr/local/scripts/SecurePipes.sh

sh /usr/local/scripts/SecurePipes.sh