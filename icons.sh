#!/bin/sh

#Tested with 10.7.4 on 6/15/2012
#Written by Kyle Bareis
#This script has no warranty. Use at your own risk!

#######################################################################################
################################### Read this part! ###################################
#######################################################################################

# Specifiy what type of icns files you are looking for files will be saved to the root 
# of your drive. Only uncomment one below at a time. This script must be run in sudo!

# The folder will be created at the root of your local hard drive with all files in it
# Example: /Applications_icnsDump/
# Options are applications, system, or library.

#iconType=Applications
#iconType=Library
#iconType=System

#######################################################################################
############################ DON'T CHANGE BELOW THIS POINT ############################
#######################################################################################

#Makes directory for ICNS files
mkdir /"$iconType"_icnsDump/

#Creates file listing icon locations so that we can copy them
sudo find /"$iconType" -name "*.icns" > /"$iconType"_icnsDump/"$iconType"_DumpList.txt

#Sets the IFS to new lines so that we get proper returns in data
IFS=$'\n'

#Loads file into a variable
fileList=( `cat "/"$iconType"_icnsDump/"$iconType"_DumpList.txt" `)

#Variaible for Counting the files
int=0

#For do loop to crank over each line of data and copy it into the folder made above
for i in "${fileList[@]}"
    do
        sudo cp "$i" /"$iconType"_icnsDump/
        int=$(($int+1))
done

#Coverting ICNS to PNG

#Creating folder to put the PNG files
mkdir /"$iconType"_pngDump/

#SIPS command to convert the files and resize.
#Sending Warnings to dev/null
sudo sips -s format png --resampleHeight 128 /"$iconType"_icnsDump/*.icns --out /"$iconType"_pngDump  > /dev/null 2>&1

echo "Congrats! you have new ICNS and PNG files"
echo "You found $int Files that have been converted"
echo "ICNS files are in /"$iconType"_icnsDump/"
echo "PNG files are in /"$iconType"_pngDump"