#!/bin/bash

######################################
# Generic Configuration for the Dock #
######################################

cat <<\EOF > /Library/LaunchAgents/com.hogarthww.EMEA_Docks.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>com.hogarthww.EMEA_Docks</string>
	<key>Nice</key>
	<integer>1</integer>
	<key>ProgramArguments</key>
	<array>
		<string>sh</string>
		<string>-c</string>
		<string>/usr/local/scripts/EMEA_All_Docks_Config.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>
EOF

cat <<\EOF > /usr/local/scripts/EMEA_ALL_Docks_Config.sh
#!/bin/bash

# Set Var
DEPT=`echo $HOSTNAME | cut -c 9-10`
CUSER=`stat -f "%Su" /dev/console`

# Ref: https://www.jamf.com/jamf-nation/discussions/28294/dockutil-cannot-get-rid-of-siri-ibooks-or-maps#responseChild178617

breadcrumb="$HOME/Library/Preferences/org.company.hogarthww.plist"
log="$HOME/Library/Logs/DockBuilder.log"
scriptName=$(basename "$0")
icon="/System/Library/CoreServices/Dock.app/Contents/Resources/Dock.icns"

# Array to hold all the items we need to add
# Note: if you need to add options you must seperate the item from the options with a @ symbol
# Example: "'/Library/Connect to...'@--view list --display folder --sort name"
declare -a itemsToAdd=(
    "/Applications/System Preferences.app"
    )

function writelog () {
    DATE=$(date +%Y-%m-%d\ %H:%M:%S)
    /bin/echo "${1}"
    /bin/echo "$DATE" " $1" >> "$log"
}

function finish () {
    kill "$jamfHelperPID" 2>/dev/null; wait "$jamfHelperPID" 2>/dev/null
    writelog "======== Finished $scriptName ========"
    exit "$1"
}

function modify_dock () {
    for item in "${itemsToAdd[@]}"; do
        if [[ "$item" =~ @ ]]; then
            params=${item##*@}
            item=${item%@*}
            /usr/local/bin/dockutil --add "$item" $params --no-restart "$HOME/Library/Preferences/com.apple.dock.plist" 2>&1 | while read -r LINE; do writelog "$LINE"; done;
        else
            /usr/local/bin/dockutil --add "$item" --no-restart "$HOME/Library/Preferences/com.apple.dock.plist" 2>&1 | while read -r LINE; do writelog "$LINE"; done;
        fi
    done
}

create_breadcrumb () {
    # Create a breadcrumb to track the creation of the Dock
    writelog "Creating DockBuilder user breadcrumb."
    /usr/bin/defaults write "$breadcrumb" build-date "$(date +%m-%d-%Y)"
    /usr/bin/defaults write "$breadcrumb" build-time "$(date +%r)"
}

writelog " "
writelog "======== Starting $scriptName ========"

# Make sure DockUtil is installed
if [[ ! -f "/usr/local/bin/dockutil" ]]; then
    writelog "DockUtil does not exist, exiting."
    finish 1
fi

# We need to wait for the Dock to actually start
until [[ $(pgrep -x Dock) ]]; do
    wait
done

# Check to see if the Dock was previously set up for the user
if [[ -f "$breadcrumb" ]]; then
    writelog "DockBuilder ran previously on $(defaults read "$breadcrumb" build-date) at $(defaults read "$breadcrumb" build-time)."
    finish 0
fi

# Display a dialog box to user informing them that we are configuring their Dock (in background)
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "DockBuilder" -icon "$icon" -description "Your Mac's Dock is being built for the first time." &
jamfHelperPID=$!

# Hide the Dock while it is being updated
/usr/bin/defaults write "$HOME/Library/Preferences/com.apple.dock.plist" autohide -bool TRUE
/usr/bin/killall Dock
/bin/sleep 2

writelog "Clearing Dock."
/usr/local/bin/dockutil --remove all --no-restart "$HOME/Library/Preferences/com.apple.dock.plist" 2>&1 | while read -r LINE; do writelog "$LINE"; done;
/bin/sleep 5

### Config Admin Dock ###
if [[ -f $HOME/Library/Preferences/org.company.hogarthww.plist ]]; then
        exit
                elif [[ "$CUSER" = locadmin ]]; then
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app/")
						itemsToAdd+=("/Applications/Self Service.app/")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Utilities/Activity Monitor.app/")
						itemsToAdd+=("/Applications/Utilities/Console.app/")
						itemsToAdd+=("/Applications/Utilities/Disk Utility.app/")
						itemsToAdd+=("/Applications/Utilities/Keychain Access.app/")
						itemsToAdd+=("/Applications/Utilities/System Information.app/")
						itemsToAdd+=("/Applications/Utilities/Terminal.app/")
						itemsToAdd+=("/System/Library/CoreServices/Applications/Directory Utility.app/")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Recon.app/")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
fi

### Config Administration Dock ###
if [[ "$DEPT" == [Aa][Dd] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f $HOME/Library/Preferences/org.company.hogarthww.plist ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app/")
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/Microsoft OneNote.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Audio Build ###
if [[ "$DEPT" == [Aa][Uu] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")
						itemsToAdd+=("/Applications/Pro Tools.app")
						itemsToAdd+=("/Applications/Utilities/QuickTime Player 7.app")
						itemsToAdd+=("/Applications/MPEG Streamclip.app")
						itemsToAdd+=("/Applications/VLC.app")
						itemsToAdd+=("/Applications/Soundminer v4Pro.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Config Broadcast non-SAN Dock ###
if [[ "$DEPT" == [Bb][Rr] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2018/Adobe After Effects CC 2018.app/")
						itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2018/Adobe Premiere Pro CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Media Encoder CC 2018/Adobe Media Encoder CC 2018.app")
						itemsToAdd+=("/Applications/dobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2018/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2018/Adobe InDesign CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Acrobat DC/Adobe Acrobat.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Config Broadcast SAN Dock ###
if [[ "$DEPT" == [Bb][Ss] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2018/Adobe After Effects CC 2018.app/")
						itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2018/Adobe Premiere Pro CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Media Encoder CC 2018/Adobe Media Encoder CC 2018.app")
						itemsToAdd+=("/Applications/dobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2018/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2018/Adobe InDesign CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Acrobat DC/Adobe Acrobat.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Digital Build ###
if [[ "$DEPT" == [Dd][Ii] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")	
						itemsToAdd+=("/Applications/Skype for Business.app")
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2018/Adobe After Effects CC 2018.app/")
						itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2018/Adobe Premiere Pro CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Media Encoder CC 2018/Adobe Media Encoder CC 2018.app")
						itemsToAdd+=("/Applications/dobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2018/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2018/Adobe InDesign CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Acrobat DC/Adobe Acrobat.app")
					fi 
fi					
						
						
										

### Install Element Build ###
if [[ "$DEPT" == [Ee][Ll] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")	
						itemsToAdd+=("/Applications/Mail.app")
						itemsToAdd+=("/Applications/Pages.app")
						itemsToAdd+=("/Applications/Numbers.app")
						itemsToAdd+=("/Applications/Keynote.app")
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Adobe After Effects CC 2018/Adobe After Effects CC 2018.app/")
						itemsToAdd+=("/Applications/Adobe Premiere Pro CC 2018/Adobe Premiere Pro CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Media Encoder CC 2018/Adobe Media Encoder CC 2018.app")
						itemsToAdd+=("/Applications/dobe Photoshop CC 2018/Adobe Photoshop CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Illustrator CC 2018/Adobe Illustrator.app")
						itemsToAdd+=("/Applications/Adobe InDesign CC 2018/Adobe InDesign CC 2018.app")
						itemsToAdd+=("/Applications/Adobe Acrobat DC/Adobe Acrobat.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi

### Install Print Non-Production Build ###
if [[ "$DEPT" == [Pp][Nn] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Microsoft Outlook.app")	
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/Keynote.app")
						itemsToAdd+=("/Applications/Cyberduck.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi


### Install Print Production Build ###
if [[ "$DEPT" == [Pp][Pp] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
						# Add the items to the Dock
						writelog "Adding items to Dock."
						itemsToAdd+=("/Applications/System Preferences.app")
						itemsToAdd+=("/Applications/Self Service.app")
						itemsToAdd+=("/Applications/Preview.app")
						itemsToAdd+=("/Applications/Safari.app/")
						itemsToAdd+=("/Applications/Google Chrome.app")
						itemsToAdd+=("/Applications/Firefox.app/")
						itemsToAdd+=("/Applications/FontExplorer X Pro.app")
						itemsToAdd+=("/Applications/Maconomy.app")
						itemsToAdd+=("/Applications/Automation Engine Client 16.0/Shuttle.app")	
						itemsToAdd+=("/Applications/Microsoft Outlook.app")	
						itemsToAdd+=("/Applications/Microsoft Word.app")
						itemsToAdd+=("/Applications/Microsoft Excel.app")
						itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
						itemsToAdd+=("/Applications/Cyberduck.app")
						itemsToAdd+=("/Applications/@--view grid --display stack --sort name")
						itemsToAdd+=("$HOME/Downloads")
					fi 
fi


### Config Producers Dock ###
if [[ "$DEPT" == [Pp][Rr] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
							# Add the items to the Dock
							writelog "Adding items to Dock."
							itemsToAdd+=("/Applications/System Preferences.app")
							itemsToAdd+=("/Applications/Self Service.app")
							itemsToAdd+=("/Applications/Preview.app")
							itemsToAdd+=("/Applications/Safari.app/")
							itemsToAdd+=("/Applications/Google Chrome.app")
							itemsToAdd+=("/Applications/Firefox.app/")
							itemsToAdd+=("/Applications/Microsoft Outlook.app")	
							itemsToAdd+=("/Applications/Microsoft Word.app")
							itemsToAdd+=("/Applications/Microsoft Excel.app")
							itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
							itemsToAdd+=("/Applications/Maconomy.app")
		fi
fi

### Config Transcreation Dock ###
if [[ "$DEPT" == [Tt][Rr] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
							# Add the items to the Dock
							writelog "Adding items to Dock."
							itemsToAdd+=("/Applications/System Preferences.app")
							itemsToAdd+=("/Applications/Self Service.app")
							itemsToAdd+=("/Applications/Preview.app")
							itemsToAdd+=("/Applications/Safari.app/")
							itemsToAdd+=("/Applications/Google Chrome.app")
							itemsToAdd+=("/Applications/Firefox.app/")
							itemsToAdd+=("/Applications/Maconomy.app")
							itemsToAdd+=("/Applications/Pages.app")
							itemsToAdd+=("/Applications/Numbers.app")
							itemsToAdd+=("/Applications/Keynote.app")
							itemsToAdd+=("/Applications/Microsoft Outlook.app")	
							itemsToAdd+=("/Applications/Microsoft Word.app")
							itemsToAdd+=("/Applications/Microsoft Excel.app")
							itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
							itemsToAdd+=("/Applications/Skype for Business.app")
							itemsToAdd+=("/Applications/FontExplorer X Pro.app")
		fi
fi

### Config Tech Dock ###
if [[ "$DEPT" == [Ii][Tt] ]] && [[ "$CUSER" != locadmin ]]; then
        if [[ -f /usr/local/scripts/""$CUSER"DockDone.txt" ]]; then
                exit
                        else
							# Add the items to the Dock
							writelog "Adding items to Dock."
							itemsToAdd+=("/Applications/System Preferences.app")
							itemsToAdd+=("/Applications/Self Service.app")
							itemsToAdd+=("/Applications/Preview.app")
							itemsToAdd+=("/Applications/Safari.app/")
							itemsToAdd+=("/Applications/Google Chrome.app")
							itemsToAdd+=("/Applications/Firefox.app/")
							itemsToAdd+=("/Applications/Maconomy.app")
							itemsToAdd+=("/Applications/Microsoft Outlook.app")
							itemsToAdd+=("/Applications/Microsoft Word.app")
							itemsToAdd+=("/Applications/Microsoft Excel.app")
							itemsToAdd+=("/Applications/Microsoft PowerPoint.app")
							itemsToAdd+=("/Applications/Skype for Business.app")
							itemsToAdd+=("/Applications/Utilities/Terminal.app")
							itemsToAdd+=("/Applications/Jamf Pro Recon Switch.app")
							itemsToAdd+=("/Applications/Jamf Pro Remote Switch.app")
							itemsToAdd+=("/Applications/Remote Desktop.app")
							itemsToAdd+=("/Applications/Microsoft Remote Desktop.app")
		fi
fi

modify_dock
create_breadcrumb

# Reset the Dock and kill the jamfHelper dialog box
writelog "Resetting Dock."
/usr/bin/defaults write "$HOME/Library/Preferences/com.apple.dock.plist" autohide -bool FALSE
/usr/bin/killall Dock

finish 0

EOF

# Change permissions
# plist to 644, script to 755

/usr/sbin/chown root:wheel /Library/LaunchAgents/com.hogarthww.EMEA_Docks.plist
/bin/chmod 644 /Library/LaunchAgents/com.hogarthww.EMEA_Docks.plist
/usr/sbin/chown root:wheel /usr/local/scripts/EMEA_ALL_Docks_Config.sh
/bin/chmod 755  /usr/local/scripts/EMEA_ALL_Docks_Config.sh