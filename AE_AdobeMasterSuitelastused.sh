#!/bin/bash

#Adobe Master Suite last used

USERNAME=`who |grep console| awk '{print $1}'`

CCUsage=`cat /Users/$USERNAME/Library/logs/amt3.log | grep -E 'Initializing /Applications/Adobe Illustrator|Initializing /Applications/Adobe Photoshop|Initializing /Applications/Adobe AfterEffects|Initializing /Applications/Adobe Animate|Initializing /Applications/Adobe Dreamweaver|Initializing /Applications/Adobe InDesign|Initializing /Applications/Adobe Bridge' | tail -1 | awk '{ print $1 }'`

if [ $? = 0 ]; then
		echo "$CCUsage"
	elif [ `$CCUsage | wc -c` -eq 0 ] || [ ! -d /Applications/Adobe*/Adobe*.app ]; then
	CCUsage="Null"
	fi
echo "<result>"$CCUsage"</result>"