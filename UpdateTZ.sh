#!/bin/bash

#This script will write two files; The plist will watch for network changes and then the trigger will run the script to reload the Time Zone Daemon.

#This will need to be tested on OS10.13

#This should only be applicable to laptops
model=`echo $hostname | cut -c 7-8`
if [ $model == 'ws' ]; then
	exit
else

cat <<EOF > /Library/LaunchAgents/networkchange.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" \
 "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>networkchange</string>
  <key>LowPriorityIO</key>
  <true/>
  <key>ProgramArguments</key>
  <array>
    <string>/usr/local/scripts/UpdateTimeZone.sh</string>
  </array>
  <key>WatchPaths</key>
  <array>
    <string>/etc/resolv.conf</string>
    <string>/var/run/resolv.conf</string>
    <string>/private/var/run/resolv.conf</string>
  </array>
  <key>RunAtLoad</key>
  <true/>
</dict>
</plist>
EOF

cat <<EOF > /usr/local/scripts/UpdateTimeZone.sh
#!/bin/sh

#Get device interface for wireless
WiFiDI=$(/usr/sbin/networksetup -listallhardwareports | grep -A1 Wi-Fi | grep Device | awk '{print $2}')

#Get the version of Mac OS
OS_Version=$(sw_vers -productVersion)

if [[ $OS_Version == 10.11* ]]; then
  uuid=$(/usr/sbin/system_profiler SPHardwareDataType | grep "Hardware UUID" | cut -c22-57)
  #Enable location services and enforce ownership
  /usr/bin/defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd."$uuid" LocationServicesEnabled -int 1
  /usr/bin/defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd.notbackedup."$uuid" LocationServicesEnabled -int 1
  /usr/sbin/chown -R _locationd:_locationd /var/db/locationd

  #Update Time Zone preference to auto-adjust based upon location
  /usr/bin/defaults write /Library/Preferences/com.apple.timezone.auto Active 1
  /usr/bin/defaults write /Library/Preferences/com.apple.locationmenu ShowSystemServices 1

elif [[ $OS_Version == 10.12* ]]; then
  #Enable location services and enforce ownership; path is no longer UUID dependent in Sierra
  /usr/bin/defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd LocationServicesEnabled -int 1
  /usr/bin/defaults write /var/db/locationd/Library/Preferences/ByHost/com.apple.locationd.notbackedup. LocationServicesEnabled -int 1
  /usr/sbin/chown -R _locationd:_locationd /var/db/locationd

  #Update Time Zone preference to auto-adjust based upon location
  /usr/bin/defaults write /Library/Preferences/com.apple.timezone.auto Active -bool YES
  /usr/bin/defaults write /Library/Preferences/com.apple.locationmenu ShowSystemServices -bool YES

else
  echo "not running macOS 10.11 or 10.12"
fi

#Wi-Fi must be powered on to determine current location
/usr/sbin/networksetup -setairportpower $WiFiDI on

#Pause to enable location services to load properly
sleep 3

#Re-enable network time
/usr/sbin/systemsetup -setusingnetworktime on

#Python code snippet to reload AutoTimeZoneDaemon
/usr/bin/python << EOF
from Foundation import NSBundle
TZPP = NSBundle.bundleWithPath_("/System/Library/PreferencePanes/DateAndTime.prefPane/Contents/Resources/TimeZone.prefPane")
TimeZonePref          = TZPP.classNamed_('TimeZonePref')
ATZAdminPrefererences = TZPP.classNamed_('ATZAdminPrefererences')

atzap  = ATZAdminPrefererences.defaultPreferences()
pref   = TimeZonePref.alloc().init()
atzap.addObserver_forKeyPath_options_context_(pref, "enabled", 0, 0)
result = pref._startAutoTimeZoneDaemon_(0x1)
EOF

sleep 1

#Get the time from time server
/usr/sbin/systemsetup -getnetworktimeserver

#Detect the newly set timezone
/usr/sbin/systemsetup -gettimezone

exit 0
EOF

#Change permissions on files
/usr/sbin/chown root:wheel /Library/LaunchAgents/networkchange.plist
/bin/chmod 644 /Library/LaunchAgents/networkchange.plist

# make the shell script executable
/bin/chmod +x /usr/local/scripts/UpdateTimeZone.sh

fi
