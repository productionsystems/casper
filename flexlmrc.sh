#!/bin/bash


#Create the script flexlm
cat <<\EOF > /usr/local/scripts/flexlmrc.sh

#!/bin/bash

# test script to make felxlmrc file

#Current user
USERNAME=`who |grep console| awk '{print $1}'`

if [ -f /Users/$USERNAME/Library/Application\ Support/Hogarth/$USERNAME.flexlmrc.done.txt ]; then
exit
else

/bin/echo "ADSKFLEX_LICENSE_FILE=/var/flexlm" > /Users/$USERNAME/.flexlmrc

touch /Users/$USERNAME/Library/Application\ Support/Hogarth/$USERNAME.flexlmrc.done.txt

fi

EOF


#Create launchd item to call at login
cat <<\EOF > /Library/LaunchAgents/com.flexlmrc.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>com.flexlmrc</string>
	<key>ProgramArguments</key>
	<array>
		<string>/usr/local/scripts/flexlmrc.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>

EOF


chown root:wheel /Library/LaunchAgents/com.flexlmrc.plist
chmod 644 /Library/LaunchAgents/com.flexlmrc.plist
chown root:wheel /usr/local/scripts/flexlmrc.sh
chmod 755 /usr/local/scripts/flexlmrc.sh
