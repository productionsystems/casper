# Echo function
echoFunc ()
{
    # Date and Time function for the log file
    fDateTime () { echo $(date +"%a %b %d %T"); }

    # Title for beginning of line in log file
    Title="MachineCert:"

    # Header string function
    fHeader () { echo $(fDateTime) $(hostname) $Title; }

    # Check for the log file, and write to it
    if [ -e "/Library/Logs/MachineCert.log" ]; then
        echo $(fHeader) "$1" >> "/Library/Logs/MachineCert.log"
    else
        cat > "/Library/Logs/MachineCert.log" &
        cat "/Library/Logs/MachineCert.log"
        if [ -e "/Library/Logs/MachineCert.log" ]; then
            echo $(fHeader) "$1" >> "/Library/Logs/MachineCert.log"
        else
            echo "Failed to create log file, writing to JAMF log"
            echo $(fHeader) "$1" >> "/var/log/jamf.log"
        fi
    fi

    # Echo out
    echo $(fHeader) "$1"
}

echoFunc "======================== Starting Script ========================"

IFS='
'

# Attempt to ping an internal server to make sure we're on the internal network
pingHost=hogarthww.prv
if ( ping -c1 $pingHost &>/dev/null )
then
    echoFunc "Successfully pinged $pingHost, proceeding with profile installation!"

    # Install the network profile
    echoFunc "Installing new profile"
    profiles -I -F "/Library/Application\ Support/Hogarth/Hogarth Enterprise Wired & Wireless EAP-TLS.mobileconfig"
    echoFunc "Profile installation result: $?"

    # Count the number of machine certificates currently on the device
    CertCount=$(security find-certificate -apZ -c $(hostname) "/Library/Keychains/System.keychain" | grep SHA-1 | wc | awk '{print $1}')

    # Search for and clean up extra machine certificates until only the newest remains
    if [ $CertCount == 0 ]; then
            # No machine certificate was found
        echoFunc "Computer has no machine certificate!"
    elif [ $CertCount == 1 ]; then
            # One machine certificate was found

            # Find the certificate expiration date
            CertExp=$(security find-certificate -apZ -c $(hostname) "/Library/Keychains/System.keychain" | sed -n 'H; /^SHA-1/h; ${g;p;}' | openssl x509 -noout -enddate 2>/dev/null | cut -f2 -d=)
            dateformat=$(date -j -f "%b %d %T %Y %Z" "$CertExp" "+%b %d %Y")
    else
            # Multiple machine certificates were found
            echoFunc "Number of machine certificates found: $CertCount"

            # Delete the old certificates
            Tail=$CertCount
            while [ $Tail -ge 2 ]
        do
            CertsCount=$(security find-certificate -apZ -c $(hostname) "/Library/Keychains/System.keychain" | grep SHA-1 | awk '{print $3}' | tail -r | tail "+$Tail" | tail -r)
            echoFunc "Deleting certificate with SHA-1: $CertsCount"
            security delete-certificate -Z $CertsCount /Library/Keychains/System.keychain &2>/dev/null
            Tail=$(expr $Tail - 1)
        done

        # Find the new certificate expiration date
            CertExp=$(security find-certificate -apZ -c $(hostname) "/Library/Keychains/System.keychain" | sed -n 'H; /^SHA-1/h; ${g;p;}' | openssl x509 -noout -enddate 2>/dev/null | cut -f2 -d=)
            dateformat=$(date -j -f "%b %d %T %Y %Z" "$CertExp" "+%b %d %Y")
    fi

    # Update the configurations for the network connections
    networksetup -listallnetworkservices | tail -n +2 |
    while read CURRENT_SERVICE
    do
        echoFunc "Configuring Current Service: ${CURRENT_SERVICE}"
        SERVICE_CONFIG_TYPE=`networksetup -getinfo ${CURRENT_SERVICE} | grep "DHCP Configuration"`
        if [[ "${SERVICE_CONFIG_TYPE}" == "DHCP Configuration" ]]; then
            MAC_ADDRESS=`networksetup -getinfo ${CURRENT_SERVICE} | grep "Ethernet Address" | awk '{ print $3 }'`
            if [[ -z ${MAC_ADDRESS} ]]; then
                MAC_ADDRESS=`networksetup -getinfo ${CURRENT_SERVICE} | grep "Wi-Fi ID" | awk '{ print $3 }'`
            fi
            if [[ ${MAC_ADDRESS} != "(null)" && ${MAC_ADDRESS} != "" ]]; then
                echoFunc "Setting DHCP Client ID to: ${MAC_ADDRESS}"
                networksetup -setdhcp "${CURRENT_SERVICE}" "${MAC_ADDRESS}"
            fi
            fi
    done
else
    echoFunc "Failure to ping $pingHost, profile installation NOT attempted!"
fi

unset IFS

# Clean up the network profile
rm -rf "/pvt/tmp/Wired_&_Wireless_Configuration_Signed.mobileconfig"

echoFunc "Exit code: $?"
echoFunc "======================== Script Complete ========================"
exit $?