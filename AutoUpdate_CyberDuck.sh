#!/bin/bash

#set -x

log() {
    echo "$1"
    /usr/bin/logger -t "CyberDuck Installer:" "$1"
}
log "Installing Zoom.app"

CurrentVer=$(curl --head https://update.cyberduck.io/Cyberduck-6.8.3.29107.zip | grep Location | cut -d "/" -f4 | cut -d "-" -f2 | rev | cut -c6- | rev)

LocalVer=$(defaults read /Applications/Cyberduck.app/Contents/Info.plist | grep CFBundleShortVersionString | awk '{print $3}' | cut -d'"' -f2)

packageDownloadUrl="https://update.cyberduck.io/Cyberduck-6.8.3.29107.zip"

if [ "$CurrentVer" =  "$LocalVer" ]; then
	echo "Zoom is already the latest version"
	exit
fi

log "Downloading Zoom.pkg..."
/usr/bin/curl -L "$packageDownloadUrl" -o /tmp/zoom.pkg
if [ $? -ne 0 ]; then
    log "curl error: The package did not successfully download"; exit 1
fi



log "Installing Zoom.app..."
/usr/sbin/installer -pkg "/tmp/zoom.pkg" -target /
if [ $? -ne 0 ]; then
    log "installer error: The package did not successfully install"; exit 1
fi