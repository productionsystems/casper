#!/bin/bash

############################################################################
# Script to capture the current Network Switches on all local active ports #
############################################################################

# Change Log
# V1.0 JF 02/2017 - Testing

# Create launchd plist
cat <<EOF > /Library/LaunchDaemons/org.hogarthww.port_config.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>org.hogarthww.port_config</string>
    <key>ProgramArguments</key>
    <array>
        <string>sh</string>
        <string>-c</string>
        <string>/usr/local/scripts/PortChecker.sh</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
</dict>
</plist>
EOF

if [[ ! /usr/local/scripts/ ]]; then
mkdir /usr/local/scripts/
fi

# Create PortChecker script
cat <<\EOF > /usr/local/scripts/PortChecker.sh
#!/bin/bash

if [[ /tmp/en* ]]; then
	rm -f /tmp/en*
fi

# Timer function
function Timer {
n=$1
shift #remove first arg from $@ 
tcpdump $@ & x=$!
sleep $n
kill $x
}

# Function to define which ports are active
function CurrentPorts {
services=$(networksetup -listnetworkserviceorder | grep 'Hardware Port')

while read line; do
    sname=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $2}')
    sdev=$(echo $line | awk -F  "(, )|(: )|[)]" '{print $4}')
    #echo "Current service: $sname, $sdev, $currentservice"
    if [ -n "$sdev" ]; then
        ifconfig $sdev  `2>/dev/null` | grep 'status: active' `2>/dev/null`
        rc="$?"
        if [ "$rc" -eq 0 ]; then
            currentservice="active"
        fi
    fi
	echo "Current service: $sname,$sdev, $currentservice"
done <<< "$(echo "$services")"

}

wait

CurrentPorts > /tmp/activeports.txt

# Run tcpdump LLDP & CDP to collect information on port connections
if [[ `cat "/tmp/activeports.txt" | grep 'en0'` =~ "en0" ]]; then
	Timer 60 -i en0 -v -s 1500 -c 1 '(ether[12:2]=0x88cc)' > /tmp/en0LLDPdump.txt  # LLDP Command
	Timer 60 -i en0 -v -s 1500 -c 1 '(ether[20:2]=0x2000)' > /tmp/en0CDPdump.txt  # CDP Command
fi

wait

if [[ `cat "/tmp/activeports.txt" | grep 'en1'` =~ "en1" ]]; then
	Timer 60 -i en1 -v -s 1500 -c 1 '(ether[12:2]=0x88cc)' > /tmp/en1LLDPdump.txt  # LLDP Command
	Timer 60 -i en1 -v -s 1500 -c 1 '(ether[20:2]=0x2000)' > /tmp/en1CDPdump.txt  # CDP Command
fi

wait

if [[ `cat "/tmp/activeports.txt" | grep 'en2'` =~ "en2" ]]; then
	Timer 60 -i en2 -v -s 1500 -c 1 '(ether[12:2]=0x88cc)' > /tmp/en2LLDPdump.txt  # LLDP Command
	Timer 60 -i en2 -v -s 1500 -c 1 '(ether[20:2]=0x2000)' > /tmp/en2CDPdump.txt  # CDP Command
fi

wait

if [[ `cat "/tmp/activeports.txt" | grep 'en3'` =~ "en3" ]]; then
	Timer 60 -i en3 -v -s 1500 -c 1 '(ether[12:2]=0x88cc)' > /tmp/en3LLDPdump.txt  # LLDP Command
	Timer 60 -i en3 -v -s 1500 -c 1 '(ether[20:2]=0x2000)' > /tmp/en3CDPdump.txt  # CDP Command
fi

sleep 20

#cat en0CDPdump.txt; cat en0LLDPdump.txt

# Get info for en0
if [[ `cat /tmp/en0CDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en0CDPdump.txt | grep "Device-ID" > /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0LLDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en0LLDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0CDPdump.txt | grep Platform` ]]; then
		cat /tmp/en0CDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0LLDPdump.txt | grep Platform` ]]; then
		cat /tmp/en0LLDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0CDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en0CDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0LLDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en0LLDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0CDPdump.txt | grep Address` ]]; then
		cat /tmp/en0CDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi
if [[ `cat /tmp/en0LLDPdump.txt | grep Address` ]]; then
		cat /tmp/en0LLDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en0Ports.txt
fi


# Get info for en1
if [[ `cat /tmp/en1CDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en1CDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1LLDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en1LLDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1CDPdump.txt | grep Platform` ]]; then
		cat /tmp/en1CDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1LLDPdump.txt | grep Platform` ]]; then
		cat /tmp/en1LLDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1CDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en1CDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1LLDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en1LLDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1CDPdump.txt | grep Address` ]]; then
		cat /tmp/en1CDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi
if [[ `cat /tmp/en1LLDPdump.txt | grep Address` ]]; then
		cat /tmp/en1LLDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en1Ports.txt
fi


# Get info for en2
if [[ `cat /tmp/en2CDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en2CDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2LLDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en2LLDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2CDPdump.txt | grep Platform` ]]; then
		cat /tmp/en2CDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2LLDPdump.txt | grep Platform` ]]; then
		cat /tmp/en2LLDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2CDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en2CDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2LLDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en2LLDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2CDPdump.txt | grep Address` ]]; then
		cat /tmp/en2CDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi
if [[ `cat /tmp/en2LLDPdump.txt | grep Address` ]]; then
		cat /tmp/en2LLDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en2Ports.txt
fi


# Get info for en3
if [[ `cat /tmp/en3CDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en3CDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3LLDPdump.txt | grep "Device-ID"` ]]; then
		cat /tmp/en3LLDPdump.txt | grep "Device-ID" >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3CDPdump.txt | grep Platform` ]]; then
		cat /tmp/en3CDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3LLDPdump.txt | grep Platform` ]]; then
		cat /tmp/en3LLDPdump.txt | grep Platform >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3CDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en3CDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3LLDPdump.txt | grep "Port-ID"` ]]; then
		cat /tmp/en3LLDPdump.txt | grep "Port-ID" >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3CDPdump.txt | grep Address` ]]; then
		cat /tmp/en3CDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi
if [[ `cat /tmp/en3LLDPdump.txt | grep Address` ]]; then
		cat /tmp/en3LLDPdump.txt | grep Address >> /Library/Application\ Support/Hogarth/en3Ports.txt
fi

exit 0

EOF

/usr/sbin/chown root:wheel /Library/LaunchAgents/org.hogarthww.port_config.plist
/bin/chmod 644 /Library/LaunchAgents/org.hogarthww.port_config.plist
/usr/sbin/chown root:wheel /usr/local/scripts/PortChecker.sh
/bin/chmod 755 /usr/local/scripts/PortChecker.sh
/bin/chmod +x /usr/local/scripts/PortChecker.sh

exit 0