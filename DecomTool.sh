#!/bin/bash
#v1.0 Jason Flory 23/08/16


#This Script removes all data and partitions on disk0 and Sets it back up with 1 jhfs+
#called "Macintosh HD". This removes Filevault2. Captures all Machine data from API.
#Emails Data to Engineer decommisoning Mac. Removes Machine record from JSS

#!/bin/bash

### This script will fully decommison a Mac from the Pearson's environment, and report via email and !!!!update the EUS Mac Decomissioning spreadsheet!!!!. 

#Check were running as root
if [ $EUID != 0 ] ; then
logger "`basename "$0"` MUST run as root..."
exit 1
fi

# API call for machine data required - Hostname, Model, SN, OS version, Building, EMail address, full name, phone number, Position, Mac Address, IP Address, Date, Decommed by, INC Ticket no.

JSSserver=https://casper:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiuser # this user requires TBC access privs in API settings on JSS 
JSSapipw=PnZ-Nv5-E2P-CNR
UDID=`system_profiler SPHardwareDataType | awk '/Hardware/ {print $3}' | grep -`
install_dir=`dirname $0`
loggedInUser=`/bin/ls -l /dev/console | /usr/bin/awk '{ print $3 }'`
DATE=`date`

# old password, api call
curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/computers/udid/"$UDID"

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

Hostname=`grep -i -A 3 "Computer" /tmp/all.xml | awk -F '[<>]' '/name/{print $3}' | head -1`
Model=`grep "<model>" /tmp/all.xml | sed 's*<model>**g' | sed 's*</model>**g' | head -1`
SN=`grep "<serial_number>" /tmp/all.xml | sed 's*<serial_number>**g' | sed 's*</serial_number>**g' | head -1`
OS=`grep "<os_version>" /tmp/all.xml | sed 's*<os_version>**g' | sed 's*</os_version>**g'`
MacAddress=`grep "<mac_address>" /tmp/all.xml | sed 's*<mac_address>**g' | sed 's*</mac_address>**g'`
IPAddress=`grep "<ip_address>" /tmp/all.xml | sed 's*<ip_address>**g' | sed 's*</ip_address>**g'`
Building=`grep "<building>" /tmp/all.xml | sed 's*<building>**g' | sed 's*</building>**g'`

Engineer=`osascript -e 'Tell application "System Events" to display dialog "Please enter your AD Credentials:" default answer ""' -e 'text returned of result'`
INCTicket=`osascript -e 'Tell application "System Events" to display dialog "Please enter Servicedesk Ticket Number for the Mac to be Decommissioned:" default answer ""' -e 'text returned of result'`

#ok up until this point!

cat <<EOF > /tmp/MachineData.html
<p>&nbsp;</p>
<h1>Decommisioned record for: $Hostname</h1>
<table style="height: 384px; width: 563px;">
<tbody>
<tr>
<td style="width: 219.515625px;">Decommisioned By:</td>
<td style="width: 328.484375px;">$Engineer</td>
</tr>
<tr>
<td style="width: 219.515625px;">Hostname:</td>
<td style="width: 328.484375px;">$Hostname</td>
</tr>
<tr>
<td style="width: 219.515625px;">Model:</td>
<td style="width: 328.484375px;">$Model</td>
</tr>
<tr>
<td style="width: 219.515625px;">Serial Number:</td>
<td style="width: 328.484375px;">$SN</td>
</tr>
<tr>
<td style="width: 219.515625px;">OS Version:</td>
<td style="width: 328.484375px;">$OS</td>
</tr>
<tr>
<td style="width: 219.515625px;">Building:</td>
<td style="width: 328.484375px;">$Building</td>
</tr>
<tr>
<td style="width: 219.515625px;">Mac Address:</td>
<td style="width: 328.484375px;">$MacAddress</td>
</tr>
<tr>
<td style="width: 219.515625px;">Current IP Address:</td>
<td style="width: 328.484375px;">$IPAddress</td>
</tr>
<tr>
<td style="width: 219.515625px;">Date Decommissioned:</td>
<td style="width: 328.484375px;">$DATE</td>
</tr>
<tr>
<td style="width: 219.515625px;">ServiceNow Ticket No:</td>
<td style="width: 328.484375px;">$INCTicket</td>
</tr>
</tbody>
</table>
<pre>&nbsp;</pre>


EOF

#Email config required
#"$install_dir"/sendEmail -f jason.flory@hogarthww.com -t $EngineerEmail -u Decommisoning of $Hostname -s 10.150.161.23 -o username=emailsender -o password=scutter #-o message-content-type=html -o message-file=/tmp/MachineData.html
#eucmac@pearson.com

#Declare Function
#function remove_jss {
#Set variables
#computeruuid=$(system_profiler SPHardwareDataType | grep -i UUID | awk '{ print $3 }')
#jssname="jss_comp_remover"
#jsspwd="crabmeat_furlong_neoprene"
#jssurl="https://csp.pearson.com:8443/"

#Get computer record from JSS
#curl -H "Accept: application/xml" -s -o /tmp/computer.xml -k -u "$jssname":"$jsspwd" "$jssurl"JSSResource/computers/udid/"$computeruuid"
#xmllint --format /tmp/computer.xml > /tmp/computer2.xml
#mv /tmp/computer2.xml /tmp/computer.xml
#computerjssid=$(cat /tmp/computer.xml | grep -i '<id>' | head -n 1 | sed 's/[^0-9]//g' )
#Logger "erase_drive : Attempting to remove $computeruuid , JSS ID $computerjssid from JSS $jssurl"
#curl -H "Accept: application/xml" -s -k -u "$jssname":"$jsspwd" "$jssurl"JSSResource/computers/id/"$computerjssid" -X DELETE &> /dev/null
#}

#Find if we have a Fusion Drive
#diskutil cs list | grep "Index:" | grep "1"
#if [ $? -eq 0 ] ; then
#logger "erase_drive : Fusion Drive Detected..."

#Find LVGUUID and destroy it
#LVGUUID=`diskutil coreStorage list | awk '/Logical Volume Group/{print $NF}'`
#diskutil cs delete $LVGUUID

#Format disk0 and disk1, 0 is the HDD and 1 is the SDD
#diskutil eraseDisk jhfs+ "SPIN" disk1
#diskutil eraseDisk jhfs+ "Macintosh HD" disk0

#Unmount the Spin Drive to prevent Imaging using it
#diskutil unmountDisk force /Volumes/SPIN

#Tell User about smashing the Fusion
#osascript -e 'display dialog "A Fusion drive was detected. This has now been destoryed and split into its separate drives. After imaging please check they have been fused back together." buttons "OK" default button 1 with title "Fusion Drive Detected" with icon stop' &>/dev/null

#remove_jss

#exit 0
#fi

#logger "erase_drive : Fusion Drive NOT Detected..."


#ls /dev/disk* | grep -v s[0-9] | sort -r | while read drv
#do
#diskutil eject "$drv"
#done

#gpt destroy /dev/disk0
#diskutil eraseDisk jhfs+ "Macintosh HD" disk0

#remove_jss

exit
