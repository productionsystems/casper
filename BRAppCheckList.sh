#!/bin/bash

set -x

cat <<EOF > /tmp/MasterAppList.txt

002J8.pkg
164LicenseServer_Maxon_Cinema4D_&_Team_Render_R18.dmg
AE_CC_2014_Presets_Scrips.pkg
AE_CC_2015.3_Presets_Scrips.pkg
Avid_Codecs_LE_2.7.1.pkg
Blackmagic Desktop Video 10.8.2.pkg
Cyberduck-4.7.3.pkg
Dockutil.pkg
Drivers_164_230216_NO_C60.pkg
EMEA_FirstBootScript.pkg
Fiery Printer Driver.pkg
Font Explorer Pro 6 Client Install - Connect to Master.dmg
Font Explorer Pro 6 Plugin Install.dmg
Free_Java_For_OSX_2014_001.pkg
HGSLXerox7100N.pkg
HogarthSystemFiles_V001.pkg
Lic_EMEA_Eyeheight_Plugins.pkg
Lic_EMEA_RedGiant_EffectsSuite_11.1.7.pkg
Lic_EMEA_RedGiant_Keying Suite for AE_2015.3.pkg
Lic_EMEA_RedGiant_KeyingSuite_11.1.5.pkg
Lic_EMEA_RedGiant_Looks for AE_2015.3.pkg
Lic_EMEA_RedGiant_MagicBullet Suite for AE_2015.3.pkg
Lic_EMEA_RedGiant_MagicBulletSuite_12.1.4.pkg
Lic_EMEA_RedGiant_Pluraleyes for AE_2015.3.pkg
Lic_EMEA_RedGiant_Suite for AE_2015.3.pkg
Lic_EMEA_RedGiant_Trapcode Suite for AE_2015.3.pkg
Lic_EMEA_RedGiant_Trapcode_v12.1.19.pkg
Lic_EMEA_VideoCopilot_ColorVibrance_Mediacore.pkg
Lic_EMEA_VideoCopilot_Element3D_Star_Pack_Rooftop_Toolkit_CC_Mediacore.dmg
Lic_EMEA_VideoCopilot_OpticalFlares_v1.3.5_MediaCore.pkg
Lic_EMEA_VideoCopilot_Reflect_Mediacore.pkg
Lic_EMEA_VideoCopilot_Twitch_1.3_MediaCore.pkg
Lic_VRAY3.4_C4D_R18_164.pkg
Nuke10.5v1-mac-x86-release-64.pkg
Path_Finder.dmg
Quicktime 7.dmg
Skype-7.18.0.341.pkg
TheFoundry_Nuke_Plugin_config_HGSL_init.py.dmg
buildversion2015.pkg
config_IPInMenuBar_with_LaunchAgent.dmg
config_JAMF_JSS_URL.dmg
driver_VideoIO_Blackmagic_Desktop_Video_10.3.7.pkg
duti_1.5.3.dmg
fcp7_templates_hogarth_clocks_2012-05.dmg
free_Adobe_Flash_Player_21.0.0.197.pkg
free_Adobe_Pepper_Flash_Player.pkg
free_Apple_QuickTime_ProApps_1.0.5.pkg
free_Avid_QuickTime_Codecs_LE_2.5.pkg
free_CocoaDialog.pkg
free_EasyFind_4.9.3.dmg
free_Final_Cut_Server_for_10.10.pkg
free_Firefox_45.0.pkg
free_Flip4Mac_3.2.0.16_for_10.7_10.8.pkg
free_Google_Chrome_49.0.2623.87.pkg
free_IPInMenuBar_4_4.dmg
free_MPEG_Streamclip_1.9.2.dmg
free_Microsof_Remote_8.0.9.dmg
free_Microsoft_OneNote_15.3.2.pkg
free_Microsoft_Silverlight.pkg
free_MisterHorse_AnimationComposer2.pkg
free_Perian_1.2.3.dmg
free_Spotify.pkg
free_Theora_OGG_WebM_CC_Plugins.pkg
free_VLC_Player_2.2.2.pkg
free_VideoCopilot_Saber_1.0.39_MediaCore.pkg
free_VideoCopilot_Sure_Target_2.0.5_MediaCore.pkg
free_soundflower_1_6_6_lion.dmg
lic_Adobe_Acrobat_Pro_XI_Install.pkg
lic_Adobe_CC_2014_2015.4_Oct16_Broadcast_Install.pkg
lic_Apple_Keynote_2015_6.6.1.dmg
lic_Apple_Numbers_2015_3.6.1.dmg
lic_Apple_Pages_2015_5.6.1.dmg
lic_Casper_Recon_v9.96.dmg
lic_EMEA_Eyeheight_CC.pkg
lic_FEX.dmg
lic_Farmers_Wife_6_SP4_And_Config_Feb2016.dmg
lic_Font_Explorer_4.2.3_CJ_UK.pkg
lic_HGSL_Thinkbox_Deadline_8.0.14.1.pkg
lic_HGSL_Thinkbox_Deadline_8.0.14.1_Submission.dmg
lic_Lenscare_AFX_CC2015_Plugin.pkg
lic_Maconomy_Work_Space_Client_live_v1.pkg
lic_Maxon_Cinema4D_R18SP2_licensefile_HGSL.pkg
lic_Maxon_Cinema4D_R18_Plugins_Bundle.pkg
lic_Maxon_Cinema4D_R18_licensefile_HGSL.pkg
lic_Microsoft_Office_2016_15_18.pkg
lic_Microsoft_OneDrive_for_Business.pkg
lic_Microsoft_lync_14.2.1.pkg
lic_ReVision_RSMB_Pro_v5_Floating_Plugin_and_ServerClient.pkg
lic_Sophos_AV_9.2.9_OSX10.11_EMEA_21.01.16.pkg
lic_zPlane_PPMulatorXL_Standalone_AAX_3.3.1_AU_3.3.2_No_VST_RTAS.dmg
plugin_free_Oracle_Java.pkg
plugin_free_RED_Final_Cut_Studio_30.4.mpkg
script_Rift_2.0.5_AECC14.dmg
unlic_Maxon_Cinema4D_&_Team_Render_R16.038.pkg
unlic_Vray_for_Cinema_4D_R16_&_Team_Render.pkg
EOF

ls /Library/Application\ Support/JAMF/Receipts/ > /tmp/NewAppList.txt

diff /tmp/MasterAppList.txt /tmp/NewAppList.txt | grep "<" | sed 's/^<//g' > /Library/Application\ Support/Hogarth/MissingApps.txt

#Need a self distructing script for .sh and .plist files

wait

# Create launchd plist
cat <<EOF > /Library/LaunchDaemons/org.BRAppChecklist_config.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>org.BRAppChecklist_config</string>
    <key>ProgramArguments</key>
    <array>
        <string>sh</string>
        <string>-c</string>
        <string>/usr/local/scripts/BRAppChecklist.sh</string>
		</array>
		    <key>StartInterval</key>
		   	<integer>360</integer>
</dict>
</plist>
EOF

# 7 days in seconds 604800

wait

# Create PortChecker script
cat <<\EOF > /Library/Application\ Support/Hogarth/BRAppChecklist.sh
#!/bin/bash

if [[ /Library/Application\ Support/Hogarth/MissingApps.txt == 0 ]]; then
	rm -f /Library/Application\ Support/Hogarth/MissingApps.txt
	rm -f /Library/LaunchDaemons/org.BRAppChecklist_config.plist
	rm -f /Library/Application\ Support/Hogarth/BRAppChecklist.sh
else 
	echo "File does not exsist"
fi

exit 0

EOF 


/usr/sbin/chown root:wheel /Library/LaunchDaemons/org.BRAppChecklist_config.plist
/bin/chmod 644 /Library/LaunchDaemons/org.BRAppChecklist_config.plist
/usr/sbin/chown root:wheel /Library/Application\ Support/Hogarth/BRAppChecklist.sh
/bin/chmod u=rwx,go=rx /Library/Application\ Support/Hogarth/BRAppChecklist.sh

launchctl load /Library/LaunchDaemons/org.BRAppChecklist_config.plist

exit 0
