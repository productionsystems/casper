#!/bin/bash

set -x

# create a timed admin elervator for Self Service - Ref: https://www.jamf.com/jamf-nation/discussions/6990/temporary-admin-using-self-service#responseChild64340

##############
# TempAdmin.sh
# This script will give a user 5 minutes of Admin level access.
# It is designed to create its own offline self-destruct mechanism.
#
# To do:
# * User notification of usage via Jamf - done
# * Limit to 5 minutes - done
# * Limit amount of times it can be run in any one day - done
# * Ensure that they cannot elervate their own account perminently - done
# * Ensure they cannot change the locadmim password, hide it! - done
# * snapshot & revover: sudoers file, host file and Applesetup - done
# * delete any account over 500 - done
# * Ensure a reboot doesn't allow admin to presist, use launchd - done
# * All logged info to be sent to a logging server via API
# * Compliance to define additional criteria
##############

CONSOLE_USER=`who |grep console| awk '{print $1}'`

### Create day limiter ###
RunDate=$(date | awk '{print $1}')
if [ ! -f /usr/local/scripts/daylimiter.txt ]; then
	touch /usr/local/scripts/daylimiter.txt
fi
DateRun=`cat /usr/local/scripts/daylimiter.txt`

if [ $RunDate == $DateRun ]; then
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "Temporary Admin Rights" -description  "This tool has been used once today, please contact IT support if Admin privilages are still required." -button1 "OK"
	exit
else




### Activate Admin access ###

# Hide Syetems Prefs, so the password cannot be changed 
pkill -f "System Preferences.app"
sudo dscl . create /Users/locadmin IsHidden 1

# Backup important files for restore
cp -p /etc/sudoers /usr/local/scripts/sudoers
cp -p /etc/hosts /usr/local/scripts/hosts
cp -p /var/db/.applesetupdone /usr/local/scripts/.applesetupdone


# Time Machine Snapshot
tmutil snapshot

# Add user to Admin group
/usr/sbin/dseditgroup -o edit -a $CONSOLE_USER -t user admin
echo "$RunDate" > /usr/local/scripts/daylimiter.txt

# Notification
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "Temporary Admin Rights Granted." -description "
This tool will enable Admin access for a period of 5 minutes once per day.

Please use responsibly!

All administrative activity is logged, please be advised that missuse could result in disaplinary action.

Access expires in 5 minutes." -button1 'OK' > /dev/null 2>&1 &

#run logger for 5 minutes
timeout -sHUP 5m logger
	



### Create structure for disableing admin rights ###

# Create admin rights removal script
cat <<\EOF > /usr/local/scripts/removeTempAdmin.sh
#!/bin/bash

### Define current user
CONSOLE_USER=`who |grep console| awk '{print $1}'`

### Remove admin access
/usr/sbin/dseditgroup -o edit -d $CONSOLE_USER -t user admin

### Hide locadmin account
sudo dscl . create /Users/locadmin IsHidden 0

### Close System Preferences
pkill -f "System Preferences.app"

### Check and delete additional local Admin accounts
adminUsers=$(dscl . -read Groups/admin GroupMembership | cut -c 18-)

iIsGroupMember=$(/usr/sbin/dseditgroup -o checkmember -u "${CONSOLE_USER}" "admin" 2>/dev/null | /usr/bin/grep -c '^yes .*')
if [[ ${iIsGroupMember} -eq 1 ]]; then
    /usr/sbin/dseditgroup -o edit -n /Local/Default -d "${CONSOLE_USER}" -t "user" "admin" \
        && echo "Removed user ${CONSOLE_USER} from group admin." \
        || echo "Error removing user ${CONSOLE_USER} from group admin."
else
    echo "User ${CONSOLE_USER} is not a member of group admin."
fi

# Restore importtant files
mv -f /usr/local/scripts/sudoers /etc/
mv -f /usr/local/scripts/hosts /etc/
mv -f /usr/local/scripts/.applesetupdone /var/db/

# Cleanup files
rm -f /Library/LaunchDaemons/com.hogarthww.adminremove.plist
rm -f /Library/LaunchDaemons/com.Hogarthww.AdminCheck.plist
rm -f /usr/local/scripts/runTempAdmin.sh
rm -f /usr/local/scripts/removeTempAdmin.sh

exit 0
EOF




### Create login check to disable unauthorised admin access ###

# Plist for login Admin account check
cat <<\EOF > /Library/LaunchDaemons/com.Hogarthww.AdminCheck.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>net.Hogarthww.AdminCheck</string>
	<key>LaunchOnlyOnce</key>
	<true/>
	<key>RunAtLoad</key>
	<true/>
	<key>ProgramArguments</key>
	<array>
		<string>/usr/local/scripts/AdminCheck.sh</string>
	</array>
</dict>
</plist>
EOF



/usr/sbin/chown root:wheel /Library/LaunchDaemons/com.Hogarthww.AdminCheck.plist
/bin/chmod 744 /Library/LaunchDaemons/com.Hogarthww.AdminCheck.plist
launchctl load com.Hogarthww.AdminCheck.plist

# Disable Admin access for all none whitelisted accounts
cat <<\EOF > /usr/local/scripts/AdminCheck.sh
#!/bin/bash

# Notification
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "Temporary Admin Rights." -description "Admin rights have been removed due to a restart." -button1 'OK' > /dev/null 2>&1 &

### Define current user
CONSOLE_USER=`who |grep console| awk '{print $1}'`

iIsGroupMember=$(/usr/sbin/dseditgroup -o checkmember -u "${CONSOLE_USER}" "admin" | awk '{print $1}')
if [[ ${iIsGroupMember} -eq yes ]]; then
    /usr/sbin/dseditgroup -o edit -n /Local/Default -d "${CONSOLE_USER}" -t "user" "admin" \
else
    echo "User ${CONSOLE_USER} is not a member of group admin."
fi

### Hide locadmin account
sudo dscl . create /Users/locadmin IsHidden 0

EOF

chmod +x /usr/local/scripts/AdminCheck.sh


### Disable admin access after 5 minutes ###

# Create timed and notification script
cat <<\EOF > /usr/local/scripts/runTempAdmin.sh
#!/bin/bash

# Capture logs to server
tee -a /etc/syslog.conf << END
*.*							@10.252.42.42:?????
END

sleep 300
sh /usr/local/scripts/removeTempAdmin.sh
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "Temporary Admin Rights." -description "
Temporary Admin Rights have been removed and will not be available again until tomorrow.

Please contact IT support with any urgent issues." -button1 'OK' > /dev/null 2>&1 &
exit 0
EOF


chmod +x /usr/local/scripts/runTempAdmin.sh
sh /usr/local/scripts/runTempAdmin.sh &


fi

exit 0
