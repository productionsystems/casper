#!/bin/bash

cat <<EOF > /Library/LaunchDaemons/networkchange.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.org.Hogarthww</string>
    <key>ProgramArguments</key>
    <array>
        <string>/usr/local/scripts/USBAdaptors.sh</string>
    </array>
    <key>WatchPaths</key>
    <array>
        <string>/Library/Preferences/SystemConfiguration</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
</dict>
</plist>
EOF

cat <<EOF > /usr/local/scripts/USBAdaptors.sh
#!/bin/bash
# Fix for USBC Network Adaptors
# Checks to see if the Mac is either a MacBook, MacBook Pro Retina or MacBook Air.
# If it's any of these machines, the script will then check for external USB
# or Thunderbolt network adapters. If an adapter is present, it will add the 
# adapter to network services.
#
# Original script by Allen Golbig:
# https://github.com/golbiga/Scripts/tree/master/enable_external_network_adapter
# REF https://github.com/rtrouton/rtrouton_scripts/tree/master/rtrouton_scripts/enable_external_network_adapter
# Modified: 2017-01-27 to included USB-C adapters from Belkin and Startech - Jhalvorson


##########################################################################################
# Create time date stamped log to record actions taken by this script.
##########################################################################################

macbook_check=`/usr/sbin/system_profiler SPHardwareDataType | awk '/Model Name/' | awk -F': ' '{print substr($2,1,7)}'`

usbcstartechus1fc30a=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: USB 10/100/1000 LAN"`
usbcbelkinf2cu040=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: Belkin USB-C LAN"`

usbGigAdapter=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: USB Gigabit Ethernet"`
usbAdapter=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: USB Ethernet"`
usbAppleAdapter=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: Apple USB Ethernet Adapter"`

tbAdapter=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: Thunderbolt Ethernet"`
tbDisplay=`/usr/sbin/networksetup -listallhardwareports | grep "Hardware Port: Display Ethernet"`

/usr/sbin/networksetup -detectnewhardware

if [ "$macbook_check" = "MacBook" ]; then
    if [ "$usbcstartechus1fc30a" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice USB\ 10/100/1000\ LAN 'USB 10/100/1000 LAN'
    else
        echo "No USB-C Startech connected"
    fi
        if [ "$usbcbelkinf2cu040" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice Belkin\ USB-C\ LAN 'Belkin USB-C LAN'
    else
        echo "No USB-C Belkin connected"
    fi
    if [ "$usbAdapter" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice USB\ Ethernet 'USB Ethernet'
    else
        echo "No USB Adapter connected"
    fi
    if [ "$usbAdapter" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice Apple\ USB\ Ethernet\ Adapter 'Apple USB Ethernet Adapter'
    else
        echo "No Apple USB Ethernet Adapter connected"
    fi
    if [ "$usbGigAdapter" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice USB\ Gigabit\ Ethernet 'USB Gigabit Ethernet'
    else
        echo "No USB Gigabit Ethernet Adapter connected"
    fi
    if [ "$tbAdapter" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice Thunderbolt\ Ethernet 'Thunderbolt Ethernet'
    else
        echo "No Thunderbolt Adapter connected"
    fi
    if [ "$tbDisplay" != "" ]; then
        /usr/sbin/networksetup -createnetworkservice Display\ Ethernet 'Display Ethernet'
    else
        echo "No Display Ethernet connected"
    fi
else
    echo "This machine does not use external network adapters"
fi

log "Completed 010_Network_enable_external_adapters.sh"
EOF


#Change permissions on files
/usr/sbin/chown root:wheel /Library/LaunchAgents/networkchange.plist
/bin/chmod 644 /Library/LaunchAgents/networkchange.plist

# make the shell script executable
/bin/chmod +x /usr/local/scripts/USBAdaptors.sh

exit 0