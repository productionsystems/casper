#!/bin/bash

#script to repair hostname for when a user changes the hostname 
#at build time the hostname get written into a file
#once a week a launch agent will check that the hostname and the dat in the file are the same and rectify if different.

NEWHOSTNAME=`cat /usr/local/scripts/originalhostname.txt`

if ! [ `echo $NEWHOSTNAME` = `hostname` ]; then
	echo "hostname needs changing"

scutil --set ComputerName $NEWHOSTNAME

scutil --set LocalHostName $NEWHOSTNAME

scutil --set HostName $NEWHOSTNAME

jamf recon

dsconfigad -force -remove hogarthww.prv -username casper.jss -password Jasper99
dsconfigad -force -add hogarthww.prv -username casper.jss -password Jasper99 -ou "OU=NewComputers,OU=Computers,OU=HogarthWW,DC=hogarthww,DC=prv" -localhome enable -useuncpath enable -groups "Domain Admins,Enterprise Admins" -alldomains enable -mobile enable -mobileconfirm disable

echo "The Hostname has been changed and the Mac has been rebound to AD" 

exit 0

else 
	echo "Hostname OK"
	exit
fi
