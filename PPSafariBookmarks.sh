#!/bin/bash


CUSER=`stat -f "%Su" /dev/console`

rm -f /Users/$CUSER/Library/Safari/Bookmarks.plist

cat <<EOF > /Users/$CUSER/Library/Safari/Bookmarks.plist 
{
    Children =     (
                {
            Title = History;
            WebBookmarkIdentifier = History;
            WebBookmarkType = WebBookmarkTypeProxy;
            WebBookmarkUUID = "58091398-E5D0-42FB-ABC4-C9C62CBF562F";
        },
                {
            Children =             (
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = "Adsend \\U2014 Dubsat";
                    };
                    URLString = "http://www.dubsat.com/adsend";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "490A57BB-DEB7-4449-AB19-EECA6DBDAD39";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = AdGate;
                    };
                    URLString = "https://adgate.hogarthww.com/login;jsessionid=hnko65ajji97";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "AB2B97B5-032F-46CB-8AF0-D1B1C9E067DE";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = Transfer;
                    };
                    URLString = "https://transfer.hogarthww.com/courier/web/1000@/wmLogin.html?";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "EE2B50C4-1681-45F4-BCDF-BCE7FC502087";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = ServiceNow;
                    };
                    URLString = "https://coretech.service-now.com/";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "CB2983B7-5EE3-496C-B601-E888119DFD6E";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = HSBC;
                    };
                    URLString = "https://mymarketingcentre.net/signin/";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "24A6721C-014E-4A31-8BF1-5900F525C59C";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = Hogarth;
                    };
                    URLString = "https://uniadapt.hogarthww.com/hogarth/common/login/ebms;jsessionid=C20C0DA0D0BBA889819FA94E0C8AC3DD";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "D5C2D3D5-9944-4AB0-A7AC-B28EF56C2DFC";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = ZONZA;
                    };
                    URLString = "https://draeger.zonza.tv/authentication/login/";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "CA19A5B5-3626-46B6-BE2A-7D7D8CD8F1D8";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = Pfesa;
                    };
                    URLString = "https://pfesaplus.hogarthww.com/";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "D631E7A0-9232-45AD-A721-8404F4B4146A";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = FIDO;
                    };
                    URLString = "https://fido.hogarthww.com/hogarth/common/login/ebms?LoginPage=user_defined_9";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "7EC3F318-6645-4BC5-8E95-73FBBDCB3ACE";
                },
                                {
                    ReadingListNonSync =                     {
                        neverFetchMetadata = 0;
                    };
                    URIDictionary =                     {
                        title = "MS 365";
                    };
                    URLString = "https://login.microsoftonline.com/";
                    WebBookmarkType = WebBookmarkTypeLeaf;
                    WebBookmarkUUID = "0D8860F7-99E8-410B-A60C-F173C6BEDDAD";
                }
            );
            Title = BookmarksBar;
            WebBookmarkType = WebBookmarkTypeList;
            WebBookmarkUUID = "EC0CA749-2490-44E1-85F1-69BDDA54E6DC";
        },
                {
            Title = BookmarksMenu;
            WebBookmarkType = WebBookmarkTypeList;
            WebBookmarkUUID = "3DD9A399-AB41-46F5-81C7-3722AE8B4CA1";
        }
    );
    WebBookmarkFileVersion = 1;
    WebBookmarkType = WebBookmarkTypeList;
    WebBookmarkUUID = "45165D48-D765-441F-8D9A-F02BF3A0DC13";
}
EOF

/usr/sbin/chown $CUSER:"HOGARTHWW\Domain Users" /Users/$CUSER/Library/Safari/Bookmarks.plist
/usr/sbin/chmod 755 /Users/$CUSER/Library/Safari/Bookmarks.plist

exit 0