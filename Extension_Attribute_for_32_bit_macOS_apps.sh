#!/bin/sh

tempFile=/tmp/.32bitApps_unsorted.txt
outputFile=/Library/COMPANYNAME/SearchResults/32bitApps.txt

# Create list
/usr/sbin/system_profiler SPApplicationsDataType | grep -B 6 -A 2 "(Intel): No" | grep "Location:" | sed -e 's/^[ \t]*//' | sed -e 's/Location\: //g' > $tempFile

# Sort list
/usr/bin/sort $tempFile > $outputFile

exit 0








#AE
#!/bin/sh

outputFile=/Library/COMPANYNAME/SearchResults/32bitApps.txt
fileContent=$( cat $outputFile )

if [[ -e $outputFile ]]; then
    echo "<result>$fileContent</result>"
else
    echo "<result>FileDoesNotExist</result>"
fi