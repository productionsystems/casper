#!/bin/bash

# Need to get JSS user information populated.
# Get last active user and recon this info to jss

jamf_binary="/usr/local/jamf/bin/jamf"

# API call for machine data required - EMail address.
JSSserver=https://casper:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiuser # this user requires TBC access privs in API settings on JSS 
JSSapipw=PnZ-Nv5-E2P-CNR
UDID=`system_profiler SPHardwareDataType | awk '/Hardware/ {print $3}' | grep -`

# old password, api call
curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/computers/udid/"$UDID"

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

# Check if JSS Username field has data
jssuser=`grep "<username>" /tmp/all.xml | awk -F "<" '{print $2}' | awk -F ">" '{print $2}'`

Last_User=$( find /Users/ -mindepth 1 -maxdepth 1 -type d -group "HOGARTHWW\Domain Users" | head -1 | cut -d'/' -f4 )

if [ $jssuser ]; then
	echo "Active user in the jSS"
else 
	$jamf_binary recon -endUsername $Last_User
fi







