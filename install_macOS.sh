#!/bin/bash

###################################################################################################
# Script Name:  install_macOS.sh
# By:  Zack Thompson / Created:  9/15/2017
# Version:  2.1.2 / Updated:  11/5/2018 / By:  ZT
#
# Description:  This script handles in-place upgrades or clean installs of macOS.
#
# Ref: https://github.com/MLBZ521/install_macOS
#
# Changes History:
# JF - Removed all OS option except 10.14
# JF - Hard coded Jamf options
# JF - Var for appName not worrking properly so hard coded path.
# JF - Test removing fuction "modernFeatures"
# 
# Build issues:
# JF - Rebuild to 10.13 build and login with AD account and then run SS upgrade, ensure AD still bound after update.
###################################################################################################

set -x

echo "*****  install_macOS process:  START  *****"

##################################################
# Define Environmental Variables

mkdir -p /usr/local/logos/

# Jamf Pro Server URL
	jamfPS=$(/usr/bin/defaults read /Library/Preferences/com.jamfsoftware.jamf.plist jss_url | /usr/bin/rev | /usr/bin/cut -c 2- | /usr/bin/rev)
# Download Icon IDs
	mojaveIconID="/usr/local/logos/MojaveDLicon.png"
# Custom Trigger used for FileVault Authenticated Reboot
	authRestartFVTrigger="AuthenticatedRestart"
# Custom Trigger used for Downloading Installation Media
	mojaveDownloadTrigger="macOSUpgrade_Mojave"

##################################################
# Define Variables

# jamfHelper location
	jamfHelper="/Library/Application Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper"
# Check if machine is FileVault enabled
	statusFV=$(/usr/bin/fdesetup isactive)
# Check if machine supports authrestart
	authRestartFV=$(/usr/bin/fdesetup supportsauthrestart)
# Define array to hold the startosinstall arguments
	installSwitch=()
# Define the value for the --newvolumename Switch
	volumeName="Macintosh HD"
# Define the value for the --installpackage Switch
	packageName="/tmp/Jamf_QuickAdd.pkg"
# Reassign passed parameters
	macOSVersion="Mojave"
	methodType="Self Service"
# Define path to installer
	upgradeOS="/Applications/Install macOS Mojave.app"

##################################################
# Setup Functions

# Setup jamfHelper Windows
inform() {

	## Title for all jamfHelper windows
	title="macOS Install"

	# Messages are based on the Workflow method chosen...
	case "${methodType}" in
		"Forced" )
			case "${1}" in
				"Installing" )
					## Setup jamfHelper window for Installing message
					windowType="hud"
					Heading="Initializing macOS..."
					Description="Your machine has been scheduled to for a macOS upgrade, please save all open work and close all applications.  This process may take some time depending on the configuration of your machine.
Your computer will reboot and begin the upgrade process shortly."
					Icon="$mojaveIconID"
					extras=""
					waitOrGo="Go"
				;;
				"Reboot" )
					## Setup jamfHelper window for a non-FileVaulted Restart message
					windowType="hud"
					Heading="Rebooting System...                      "
					Description="This machine will reboot in one minute..."
					Icon="$mojaveIconID"
					extras=""
					waitOrGo="Go"
				;;
			esac
		;;
		"Classroom" )
			case "${1}" in
				"Download" )
					## Setup jamfHelper window for Installing message
					windowType="fs"
					Heading="Initializing macOS..."
					Description="This process may take some time depending on the configuration of the machine.
This computer will reboot and begin the install process shortly."
					Icon="$mojaveIconID"
					extras=""
					waitOrGo="Go"
				;;
				"Reboot" )
					## Setup jamfHelper window for a non-FileVaulted Restart message
					windowType="hud"
					Heading="Rebooting System...                      "
					Description="This machine will reboot in one minute..."
					Icon="$mojaveIconID"
					extras=""
					waitOrGo="Go"
				;;
			esac
		;;
		"Self Service" | "" )
			case "${1}" in
				"Download" )
					## Setup jamfHelper window for Downloading message
					windowType="hud"
					Heading="Downloading macOS installation media...                               "
					Description="This process may potentially take 40 minutes or more depending on your connection speed.
Once downloaded, you will be prompted to continue."
					Icon="$mojaveIconID"
					extras="-button1 OK"
					waitOrGo="Go"
				;;
				"DownloadComplete" )
					## Setup jamfHelper window for Download Complete message
					windowType="hud"
					Heading="Download Complete!                                         "
					Description="Before continuing, please complete the following actions:
	- Save all open work and close all applications.
	- A power adapter is required to continue, connect it now if you are running on battery.

Click OK when you are ready to continue; once you do so, the install process will begin."
					Icon="/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/ToolbarInfo.icns"
					extras="-button1 OK"
					waitOrGo="Wait"
				;;
				"PowerMessage" )
					## Setup jamfHelper window for AC Power Required message
					windowType="hud"
					Heading="AC Power Required                      "
					Description="To continue, please plug in your Power Adapter.

Press 'OK' when you have connected your power adapter."
					Icon="/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/AlertCautionIcon.icns"
					extras="-button1 \"OK\" -button2 \"Cancel\" -defaultButton 1"
					waitOrGo="Wait"
				;;
				"Installing" )
					## Setup jamfHelper window for Installing message
					windowType="fs"
					Heading="Initializing macOS..."
					Description="This process may take some time depending on the configuration of your machine.
Your computer will reboot and begin the install process."
					Icon="$mojaveIconID"
					extras=""
					waitOrGo="Go"
				;;
				"ManualFV" )
					## Setup jamfHelper window for Manual FileVault Restart message
					windowType="hud"
					Heading="Reboot Required                      "
					Description="For the install to continue, you will need to unlock the FileVault encrypted disk on this machine after the pending reboot."
					Icon="/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/FileVaultIcon.icns"
					extras="-timeout 58"
					waitOrGo="Wait"
				;;
				"Reboot" )
					## Setup jamfHelper window for a non-FileVaulted Restart message
					windowType="hud"
					Heading="Rebooting System...                      "
					Description="This machine will reboot in one minute..."
					Icon="/$mojaveIconID"
					extras="-timeout 58 -button1 \"OK\" -defaultButton 1"
					waitOrGo="Go"
				;;
				"Failed" )
					## Setup jamfHelper window for Failed message
					windowType="hud"
					Heading="Failed to install macOS..."
					Description="If you continue to have issues, please contact your Deskside Support for assistance."
					Icon="/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/AlertStopIcon.icns"
					extras="-button1 \"OK\" -defaultButton 1"
					waitOrGo="Go"
				;;
			esac
		;;
	esac

	jamfHelperType "${waitOrGo}"
}

# Function that calls JamfHelper and either puts it in the background or wait for user interaction.
	jamfHelperType() {
		if [[ "${1}" == "Go" ]]; then
			"${jamfHelper}" -windowType "${windowType}" -title "${title}" -icon "${Icon}" -heading "${Heading}" -description "${Description}" -iconSize 300 $extras & 2>&1 > /dev/null
		else
			"${jamfHelper}" -windowType "${windowType}" -title "${title}" -icon "${Icon}" -heading "${Heading}" -description "${Description}" -iconSize 300 $extras 2>&1 > /dev/null
		fi
	}

# Function to Download the installer if needed.
	downloadInstaller() {
		# jamfHelper Download prompt
			inform "Download"

		# Call jamf Download Policy
			echo "Downloading the macOS insatllation package from Jamf..."
				/usr/local/jamf/bin/jamf policy -event $downloadTrigger
			echo "Download complete!"

		# jamfHelper Download Complete prompt
			inform "DownloadComplete"
	}

# Function to check if the device is on AC or battery power first
	powerCheck() {
		until [[ $powerStatus == "PASSED" ]]; do

			powerSource=$(/usr/bin/pmset -g ps)

			if [[ ${powerSource} == *"AC Power"* ]]; then
				powerStatus="PASSED"
				echo "Power Status:  PASSED - AC Power Detected"
			else
				powerStatus="FAILED"
				echo "Power Status:  FAILED - AC Power Not Detected"
				# jamfHelper Plug in Power Adapter prompt
					inform "PowerMessage"
					userCanceled=$?

				if [[ $userCanceled == 0 ]]; then
					echo "User clicked OK"
				elif [[ "${methodType}" != "Self Service" ]]; then
					echo "This system is not on AC Power.  Aborting..."
					echo "*****  install_macOS process:  ABORTED  *****"
					exit 2
				else
					echo "User canceled the process.  Aborting..."
					echo "*****  install_macOS process:  CANCELED  *****"
					exit 3
				fi

				# Give user a few seconds to connect the power adapter before checking again...
				/bin/sleep 3

			fi
		done
	}

# Function for the Install Process
	installProcess() {

		# Confirm the Installation Bundle still exists...
		if [[ -d "${upgradeOS}" ]]; then

			# macOS Mojave 10.14 no longer needs the "--applicationpath" switch, including it only if it's not being installed
			if [ "${macOSVersion}" == "Mojave" ]; then
				installSwitch+=("--applicationpath" \'"${upgradeOS}"\')
			fi

			# jamfHelper Install prompt
				inform "Installing"
				# Get the PID of the Jamf Helper Process incase the installation fails
				installInformPID=$!

			# Setting this key prevents the 'startosinstall' binary from rebooting the machine.
			/usr/bin/defaults write -globalDomain IAQuitInsteadOfReboot -bool YES

			echo "Calling the startosinstall binary..."
			exitOutput=$(eval '"${upgradeOS}"'/Contents/Resources/startosinstall --nointeraction "${installSwitch[@]}" 2>&1)

			# Grab the exit value.
			exitStatus=$?
			echo "*****  startosinstall exist status was:  ${exitStatus}  *****"
			echo "Exit Output was:  ${exitOutput%%$'\n'*}"

			# Cleaning up, don't want to leave this key set as it's not documented.
			/usr/bin/defaults delete -globalDomain IAQuitInsteadOfReboot
		else
			# jamfHelper Install Failed
				inform "Failed"

			echo "ERROR:  A cached macOS installation package was not found.  Aborting..."
			echo "*****  install_macOS process:  FAILED  *****"
			exit 4
		fi
	}

# Function for the Reboot Process
	rebootProcess() {
		if [[ $exitStatus == 127 || $exitStatus == 255 || $exitOutput == *"Preparing reboot..."* ]]; then
				# Exit Code of '255' = Results on Sierra --> High Sierra
				# Exit Code of '127' = Results on Yosemite --> El Capitan
			echo "*****  The macOS install has been successfully staged.  *****"

			if [[ $statusFV == "true" ]]; then
				echo "Machine is FileVaulted."

				if [[ $authRestartFV == "true" ]]; then
					echo "Attempting an Authenticated Reboot..."
					checkAuthRestart=$(/usr/local/jamf/bin/jamf policy -event $authRestartFVTrigger)

					if [[ $checkAuthRestart == *"No policies were found for the \"${authRestartFVTrigger}\" trigger."* ]]; then
						# Function manualFileVaultReboot
							manualFileVaultReboot
					else
						echo "*****  install_macOS process:  SUCCESS  *****"
						exit 0
					fi
				else
					# Function manualFileVaultReboot
						manualFileVaultReboot
				fi
			else
				echo "Machine is not FileVaulted."
					inform "Reboot"

				# Function scheduleReboot
					scheduleReboot

				echo "*****  install_macOS process:  SUCCESS  *****"
				exit 0
			fi
		else

			# Kill the Full Screen Install Window
			/bin/kill $installInformPID
			wait $! 2>/dev/null

			# jamfHelper Install Failed
				inform "Failed"

			echo "*****  install_macOS process:  FAILED  *****"
			exit 5
		fi
	}

# Function for unsupported FileVault Authenticated Reboot.
	manualFileVaultReboot() {
		echo "Machine does not support FileVault Authenticated Reboots..."

		# jamfHelper Unsupported FileVault Authenticated Reboot prompt
			inform "ManualFV"

		# Function scheduleReboot
		scheduleReboot

		echo "*****  install_macOS process:  SUCCESS  *****"
		exit 0
	}

# Function to Schedule a Reboot in one minute.
	scheduleReboot() {
		echo "Scheduling a reboot one minute from now..."
		rebootTime=$(/bin/date -v "+1M" "+%H:%M")

		echo "Rebooting at $rebootTime"
		/sbin/shutdown -r $rebootTime
	}

##################################################
# Now that we have our work setup...

if [[ -z "${macOSVersion}" || -z "${methodType}" ]]; then
	echo "Failed to provide required options!"
	echo "*****  install_macOS process:  FAILED  *****"
	exit 6
fi

# Turn on case-insensitive pattern matching
shopt -s nocasematch

# Set the variables based on the version that is being provided.
if [ "${macOSVersion}" = "Mojave" ]; then
		downloadIcon=${mojaveIconID}
		appName="Install macOS Mojave.app"
		downloadTrigger="${mojaveDownloadTrigger}"
fi


# Turn off case-insensitive pattern matching
shopt -u nocasematch


# Check if the install .app is already present on the machine (no need to redownload the package).
if [ -d "/Applications/Install macOS Mojave.app" ]; then
	echo "Using installation files found in /Applications"
else
	# Function downloadInstaller
	echo "Downloading Installer"
		downloadInstaller
fi


# Function powerCheck
	powerCheck
# Function installProcess
	installProcess
# Function rebootProcess
	rebootProcess
