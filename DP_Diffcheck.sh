#!/bin/bash

set -x

#DP data check

diff /opt/hogarth/JamfProDistributionHGUG/Packages/Masterfilelist.txt /opt/hogarth/JamfProDistributionHGUG/Packages/HGUGfilelist.txt | grep -e .pkg -e .dmg | awk '{$1=""}1' > /home/cssadmin/diffresult.txt

while read file
do
	find /opt/hogarth/JamfProDistributionHGUG//download.india.hogarthww.com -name "$file" -exec rm {} \;
done < /home/cssadmin/diffresult.txt

