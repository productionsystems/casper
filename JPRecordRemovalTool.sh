#!/bin/sh

# Variables
JSSserver=https://10.252.32.160:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiadmin # this user requires TBC access privs in API settings on JSS 
JSSapipw=LBG-EaV-eDH-qg2

SERIAL=$(system_profiler SPHardwareDataType | grep 'Serial Number (system)' | awk '{print $NF}')

# Notification
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /System/Library/CoreServices/Problem\ Reporter.app/Contents/Resources/ProblemReporter.icns -heading "Jamf Pro Record Removal Tool" -description "This tool will delete this machines record from Jamf Pro. 

Please only continue if you are intending to rebuild this machine." -button1 "OK" -button2 "Cancel" -cancelButton "2"

if [ $? -eq 0 ]; then

/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /System/Library/CoreServices/Problem\ Reporter.app/Contents/Resources/ProblemReporter.icns -heading "Jamf Pro Record Removal Tool" -description "By confirming this, you understand that you are deleting this machines computer record from Jamf pro. 

This action cannot be undone, please cancel if you are not sure and contact @JamfProAdmins for advise." -button1 "OK" -button2 "Cancel" -cancelButton "2"

curl -ksu ${JSSapiuser}:${JSSapipw} "$JSSserver/JSSResource/computers/serialnumber/$SERIAL" -X DELETE

/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /System/Library/CoreServices/Problem\ Reporter.app/Contents/Resources/ProblemReporter.icns -heading "Jamf Pro Record Removal Tool" -description "The Jamf Pro machine record has been removed, please rebuild the machine now." -button1 "OK" -button2 "Cancel" -cancelButton "2"

else
	exit
fi

