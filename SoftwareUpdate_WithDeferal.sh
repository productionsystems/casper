#!/bin/sh

##############################################################################################################################################
# This script checks to see if there is a logged in user, if no user is logged in software update will run and install all udpates.			 #
# If a user is logged in the script checks if the updates need a restart - if so the user is prompted whether this is ok before proceeding.  #
# Where no restart is required software update will run without interrupting the user.														 #
# Orgiginal Ref below:														 																 #
# https://jamfnation.jamfsoftware.com/discussion.html?id=5404#responseChild27123                                                             #
##############################################################################################################################################

# Change Log
# V1.0 JF 03/2017 - Testing
# V1.1 JF 03/2017 - Added deferral option
# V1.2 JF 03/2017 - Added run once per month structure
# V1.3 JF 04/2017 - Added "Current Status" to notification

set -x

# Clean SUS
jamf removeSWUSettings
softwareupdate --schedule on
defaults write /Library/Preferences/com.apple.commerce AutoUpdate -bool TRUE
defaults write /Library/Preferences/com.apple.commerce AutoUpdateRestartRequired -bool TRUE
defaults write /Library/Preferences/com.apple.commerce AutoUpdate -bool TRUE
defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticCheckEnabled -bool TRUE
defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticDownload -bool NO

function SUS {

# Set Date Variables
#RunDate=$(date | awk '{print $2}')
Date=$(date '+%B')

# Create dir and file for monthly timer
if [ ! -d /Library/Application\ Support/Hogarth ]; then
	/bin/mkdir /Library/"Application Support"/Hogarth
	/usr/bin/chown root:wheel /Library/"Application Support"/Hogarth
	/bin/chmod 755 /Library/"Application Support"/Hogarth
fi

if [[ ! -f /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist ]]; then
	/usr/bin/touch /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	chown root:wheel /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	/bin/chmod 755 /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	echo FirstRun > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
fi

# Create deferral counter file
if [[ ! -f /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist ]]; then
touch /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
chown root:wheel /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
chmod 755 /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
fi

defer=/Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
if [[ $(/bin/cat /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist) -gt "3" ]]; then
	echo "$(/bin/cat /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist)"
	/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
fi

# Test if this has been run once this month
if [[ $(/bin/cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist) = $Date ]]; then
	echo "$(/bin/cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist)"
	/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	echo "Updates already run this month"
	exit 0
elif [[ $(/bin/cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist) != $Date ]]; then
	echo "Running SUS Update"
	
	COUNTER=$[$(/bin/cat "$defer") + 1]
	echo "$COUNTER" > "$defer"

	if [[ $(/bin/cat "$defer") -lt "4" ]]; then
		#see if user logged in
		USER=$(/usr/bin/who | /usr/bin/grep console | /usr/bin/cut -d " " -f 1);
		echo "logged in user is $USER...";

			#check if user logged in
			if [[ -n "$USER" ]]; then

	   		 	#Check if restarts required
	   		 	AVAILABLEUPDATES=$(/usr/sbin/softwareupdate -l);
	   		 	NOUPDATES=$(echo $AVAILABLEUPDATES | wc -l | /usr/bin/cut -d " " -f 8);
	   		 	RESTARTREQUIRED=$(echo $AVAILABLEUPDATES | /usr/bin/grep restart | /usr/bin/cut -d "," -f 1);

	   		 		if [[ -n "$RESTARTREQUIRED" ]]; then
	      			  	echo "$RESTARTREQUIRED needs a restart";

	      			  	#ask user whether ok to restart
	      				OKTORESTART=$(/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility  -title "Apple Software updates are available" -description "Your computer will need to restart, would you like to install the updates now?
      
			$AVAILABLEUPDATES" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "Yes" -button2 "No" -cancelButton "2")

		  	  	echo "ok to restart was $OKTORESTART";

	      	  			if [[ "$OKTORESTART" = "0" ]]; then
			  			  	while :
			  				do
			  				Status_Message=`awk 'NF{p=$0}END{print p}' /tmp/SUS.log`
			  				if [[ $Status_Message = "" ]]; then
								Status_Message="Preparing..."
							/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
Your Mac will restart once all of recommend software has been installed, please save your work now.

Current Status: "$Status_Message"" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD -timeout 10 &
			  			    else
								/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
	Your Mac will restart once all of recommend software has been installed, please save your work now.

	Current Status: "$Status_Message"" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD -timeout 10 &
			  			    fi
			  				sleep 9
		  			  		done &
							/usr/sbin/softwareupdate -iar > /tmp/SUS.log
							echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
							/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
							/sbin/reboot
	      					exit 0
	      			  else
	         			  echo "User said NO to updates";
			 			  /Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Deferral Request" -description "You are allowed to defer for 3 days, after that it will be mandatory! You have deferred `cat "$defer"` times." -button1 "OK"
	     	   			  exit 1
	      			fi
	   	  	  		else
	      				echo "No restart is needed";
					    echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
					    cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist

	      	  			if [[ "$NOUPDATES" = "1" ]]; then
	         			   echo "Running Software Update"; 
	         			  /usr/sbin/softwareupdate -iar
						  echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
						  /bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	         			 exit 0
	      		   else
	         		  echo "because there were no updates";
					  echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
					  /bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	         		 exit 0
	      	   fi
	   	fi

		else
	   	 	#No logged in user
	   		/usr/sbin/softwareupdate -iar
			echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
			/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
			/sbin/reboot
		fi
	
	elif [[ $(cat "$defer") -gt 3 ]]; then

		#see if user logged in
		USER=$(/usr/bin/who | /usr/bin/grep console | /usr/bin/cut -d " " -f 1);
		echo "logged in user is $USER...";

			#check if user logged in
			if [[ -n "$USER" ]]; then

	   		 	#Check if restarts required
	   	  		AVAILABLEUPDATES=$(/usr/sbin/softwareupdate -l);
	   		 	NOUPDATES=$(echo $AVAILABLEUPDATES | wc -l | cut -d " " -f 8);
	   		 	RESTARTREQUIRED=$(echo $AVAILABLEUPDATES | /usr/bin/grep restart | /usr/bin/cut -d "," -f 1);

	   			if [[ -n "$RESTARTREQUIRED" ]]; then
	      	  		echo "$RESTARTREQUIRED needs a restart";

	      	  		#ask user whether ok to restart

	      	  		OKTORESTART=$(/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility  -title "Apple Software updates are available" -description "Your computer will need to restart, you are required to install the updates now.
      
	  	  			$AVAILABLEUPDATES" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "OK")

		  		echo "ok to restart was $OKTORESTART";

	      	  		if [[ "$OKTORESTART" = "0" ]]; then
			  		  	while :
			  		  	do
			  			Status_Message=`awk 'NF{p=$0}END{print p}' /tmp/SUS.log`
			  		  	if [[ $Status_Message = "" ]]; then
								Status_Message="Preparing..."
							/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
Your Mac will restart once all of recommend software has been installed, please save your work now.

Current Status: "$Status_Message"" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD -timeout 10 &
			  			    else
								/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
	Your Mac will restart once all of recommend software has been installed, please save your work now.

	Current Status: "$Status_Message"" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD -timeout 10 &
			  			    fi
			  			sleep 9
			  	  		done &
						echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
						/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
						/usr/sbin/softwareupdate -iar > /tmp/SUS.log
	        			/sbin/reboot
	      				exit 0
	      		  	fi
	   		 		else
	      			  	echo "No restart is needed";
						echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
						/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist

	      			if [[ "$NOUPDATES" != "1" ]]; then
	         		   	echo "So running Software Update";
	         		   	/usr/sbin/softwareupdate -iar
						echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
						/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
						exit 0
	      		  	else
	         		   	echo "because there were no updates";
						echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
						/bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	         		   	exit 0
	      		  	fi
	   		 fi

	else
	   #No logged in user
	   /usr/sbin/softwareupdate -iar
	   echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	   /bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	   /sbin/reboot
	fi
fi

elif [[ `cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist` = "$Date" ]]; then
	echo "No Update Required"
	echo $Date > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
    /bin/cat /dev/null > /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	exit 0
fi

/usr/sbin/softwareupdate --schedule off

exit 0

}

touch /var/log/SUS.log

SUS &> /var/log/SUS.log

