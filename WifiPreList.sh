#!/bin/bash

wifiDevice=`/usr/sbin/networksetup -listallhardwareports | awk '/^Hardware Port: Wi-Fi/,/^Ethernet Address/' | head -2 | tail -1 | cut -c 9-`

sudo networksetup -removepreferredwirelessnetwork $wifiDevice Hogarth

networksetup -addpreferredwirelessnetworkatindex $wifiDevice Hogarth 0 WPAE