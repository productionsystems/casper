#!/bin/bash


# local version check
# script to force with 5 day deferral
# align with SUS on the first of the month
# align all update on the first of the month
# all updates become mandetory on the day 5

JSSserver=https://10.252.32.155:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiuser # this user requires TBC access privs in API settings on JSS 
JSSapipw=PnZ-Nv5-E2P-CNR

#Local Outlook version
OutlookLocalVersion=`mdls -name kMDItemVersion "/Applications/Microsoft Outlook.app" | cut -d \" -f2`

#Get the latest version of Outlook info from JSS 
curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/patches/name/Microsoft%20Outlook%202016

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

OutlookLatestVersion=`cat /tmp/all.xml | grep "<software_version>" | sort -t ">" -k2 -g | sed "s/^[ \t]*//" | sed 's*</software_version>**g' | sort -nr | tail -n +2 | head -n1 | sed 's/^.\{18\}//g'`


# Set Date Variables
RunDate=$(date | awk '{print $3 $4}')
date=$(date | awk '{print $3 $4}')

# Create dir and file for monthly timer
if [[ ! /Library/"Application Support"/Hogarth ]]; then
	/bin/mkdir /Library/"Application Support"/Hogarth
	/usr/bin/chown root:wheel /Library/"Application Support"/Hogarth
	/bin/chmod 755 /Library/"Application Support"/Hogarth
fi

if [[ ! -f /Library/"Application Support"/Hogarth/com.hogarth.365RunDate.plist ]]; then
	/usr/bin/touch /Library/"Application Support"/Hogarth/com.hogarth.365RunDate.plist
	/bin/chmod 755 /Library/"Application Support"/Hogarth/com.hogarth.365RunDate.plist
	echo FirstRun > /Library/"Application Support"/Hogarth/com.hogarth.365RunDate.plist
fi

# Create deferral counter file
if [[ ! -f /Library/"Application Support"/Hogarth/com.hogarth.365deferral.plist ]]; then
touch /Library/"Application Support"/Hogarth/com.hogarth.365deferral.plist
chown root:wheel /Library/"Application Support"/Hogarth/com.hogarth.365deferral.plist
chmod 755 /Library/"Application Support"/Hogarth/com.hogarth.365deferral.plist
fi

defer=/Library/"Application Support"/Hogarth/com.hogarth.365deferral.plist
COUNTER=$[$(cat "$defer") + 1]
echo "$COUNTER" > "$defer"


# Test if local version of Outlook is older than current JSS version
if [ "$OutlookLocalVersion" -lt "$OutlookLatestVersion" ]; then

	# Test if this has been run once this month
	if [[ $(cat /Library/"Application Support"/Hogarth/com.hogarth.365RunDate.plist) != $date ]]; then
	echo "Running Office 365 Update"

		if [[ $(cat "$defer") -lt 5 ]]; then
			#see if user logged in
				USER=$(/usr/bin/who | /usr/bin/grep console | /usr/bin/cut -d " " -f 1);
				echo "logged in user is $USER...";

			#check if user logged in
			if [[ -n "$USER" ]]; then

				/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Office 365 updates are installing please wait..." -description  "Please do not interrupt or power off during this process. 
   		
please save your work now." -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "OK"
	    		sleep 60
				#how long before downlaod and install kicks off? 
				defaults write /Library/Preferences/com.microsoft.autoupdate2 HowToCheck AutomaticDownload
				/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -description  "All updates have now been downloaded and are ready to install, please ensure all of your work is saved, a reboot will commence once the installations are done." -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD &
				echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	       		exit 0
	      else
	         	echo "User said NO to updates";
			 	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Deferral Request" -description "You are allowed to defer for 7 days, after that it will be mandatory! You have deferred `cat "$defer"` times." -button1 "OK"
	     	   	exit 1
	      fi
	   	  else
	      		echo "No restart is needed";

	      if [[ "$NOUPDATES" != "1" ]]; then
	         echo "So running Software Update"; 
	         /usr/sbin/softwareupdate -ia
	         exit 0
	      else
	         echo "because there were no updates";
	         exit 0
	      fi
	   fi

	else
	   #No logged in user
	   /usr/sbin/softwareupdate -ia
	fi
	
elif [[ $(cat "$defer") = 5 ]]; then

	#see if user logged in
	USER=$(/usr/bin/who | /usr/bin/grep console | /usr/bin/cut -d " " -f 1);
	echo "logged in user is $USER...";

	#check if user logged in
	if [[ -n "$USER" ]]; then

	   #Check if restarts required
	   AVAILABLEUPDATES=$(/usr/sbin/softwareupdate -l);
	   NOUPDATES=$(echo $AVAILABLEUPDATES | wc -l | cut -d " " -f 8);
	   RESTARTREQUIRED=$(echo $AVAILABLEUPDATES | /usr/bin/grep restart | /usr/bin/cut -d "," -f 1);

	   if [[ -n "$RESTARTREQUIRED" ]]; then
	      echo "$RESTARTREQUIRED needs a restart";

	      #ask user whether ok to restart

	      OKTORESTART=$(/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility  -title "Apple Software updates are available" -description "Your computer will need to restart, you are required to install the updates now?
      
	  $AVAILABLEUPDATES" -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "Yes")

		  echo "ok to restart was $OKTORESTART";

	      if [[ "$OKTORESTART" = "0" ]]; then
	   		/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -title "Apple Software Installing Please Wait..." -description  "Please do not interrupt or power off during this process. 
   		
Your Mac will restart once all of recommend software has been installed, please save your work now." -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -button1 "OK"
	        sleep 60
			/usr/sbin/softwareupdate -d 
	        /Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -description  "All updates have now been downloaded and are ready to install, please ensure all of your work is saved, a reboot will commence once the installations are done." -icon /System/Library/CoreServices/Software\ Update.app/Contents/Resources/SoftwareUpdate.icns -iconsize 96 -lockHUD &
			echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
			/usr/sbin/softwareupdate -ia
	        /sbin/reboot
	      	exit 0
	      fi
	   else
	      echo "No restart is needed";

	      if [[ "$NOUPDATES" != "1" ]]; then
	         echo "So running Software Update";
			 echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist 
	         /usr/sbin/softwareupdate -ia
	         exit 0
	      else
	         echo "because there were no updates";
	         exit 0
	      fi
	   fi

	else
	   #No logged in user
	   echo $RunDate > /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist
	   /usr/sbin/softwareupdate -ia
	   rm /Library/"Application Support"/Hogarth/com.hogarth.deferral.plist
	fi
fi

elif [[ `cat /Library/"Application Support"/Hogarth/com.hogarth.SUSRunDate.plist` = "$date" ]]; then
	echo "No Update Required"
	exit 0
fi

exit 0
	