#!/bin/bash


if [ ! -d /Users/_openaudit ]; then

jamf createAccount -username _openaudit -realname _openaudit -password 56Rg34Nb57Hi -home /Users/_openaudit -shell /bin/bash

mkdir -p /Users/_openaudit/.ssh/
chmod 700 /Users/_openaudit/.ssh/
chown _openaudit:staff /Users/_openaudit/.ssh/


cat <<\EOF > /Users/_openaudit/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjbEeuCwkcu0hgs9E1e0iG3I93oVKhPlDTKZYjBI5JgaTc4Ksdmzt+fSPovj/ozBNlugCA0nLb2t+S6C1mncXwky2H60A9XeegNZhVF2ZWQYT8UYsDR65OVP4qWWx35P+4EPVxNHpRPLLqUmv7/ZTy7DciroRdmzkW3aGLByBACvg1Z7D/qZXlkhnLTUbV0cai6t6knL91H6NEHtUYvKf6Gd98NFPHTDIdIfqkKR1qyX0z04APcAXrzb9DzVX3dIjFY94k1Fe2mwdBKnD4xOPu+pCMx5SOA2mqhJHhl3V4C3pRZepYIF9R3CMNpkH9zEcxZ0HRHDNgFiwn9Etrk4mL djnadmin@gswlvmaud01
EOF

chown -R _openaudit:staff /Users/_openaudit/.ssh/authorized_keys
chmod 700 /Users/_openaudit/.ssh/authorized_keys

# Give ssh access
sudo dseditgroup -o edit -t user -a _openaudit com.apple.access_ssh


fi