#!/bin/bash

set -x

# Filevault keychain not getting updated during password change with Mobile Accounts.
# This must be run from within the locadmin account
# Dialog to request username to fix
# To be run by the usewr from SS.
# run this locally and see if it works.
# Need notification when user changes password via SSO
# Notification - we have noticed that you have changed your password please reconfirm to change the FileVault password.

cat <<\EOF > /Library/LaunchAgents/com.Hogarthww.expectFDE.sh.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>com.Hogarthww.expectFDE</string>
		<key>ProgramArguments</key>
		<array>
			<string>/bin/sh</string>
			<string>/usr/local/scripts/expectFDE.sh</string>
		</array>
		<key>StartCalendarInterval</key>
		<dict>
			<key>Hour</key>
			<integer>24</integer>
		</dict>
	</dict>
</plist>
EOF

/usr/sbin/chown root:wheel /Library/LaunchAgents/com.Hogarthww.expectFDE.sh.plist
/bin/chmod 644 /Library/LaunchAgents/com.Hogarthww.expectFDE.sh.plist
launchctl load com.Hogarthww.expectFDE.sh.plist

cat <<\EOF > /usr/local/scripts/expectFDE.sh
#!/bin/bash

set -x

# Variables
ldap_server=10.252.32.33
ldap_account=casper.jss
ldap_password=Jasper99
forest_domain=hogarthww.prv
dist_name='OU=Hogarthww,DC=hogarthww,DC=prv'
pwdPolicy=90
AdminPW="Mash2bash!"
CONSOLE_USER=`who |grep console| awk '{print $1}'`

# Check if on domain
ping -c 1 10.252.32.33 &>/dev/null || ping -c 1 10.252.32.32 &>/dev/null || ping -c 1 10.252.32.42 &>/dev/null || ping -c 1 10.252.32.43 &>/dev/null
	if [ $? = 0 ]; then
# LDAP lookup for pwdLastSet and then a little math to arrive at days until expiry.
lastpwdMS=`ldapsearch -x -H ldap://$ldap_server -D $ldap_account@$forest_domain -w "$ldap_password" -b "$dist_name" -L "anr=$CONSOLE_USER" pwdLastSet | /usr/bin/awk '/pwdLastSet:/{print $2}'`
lastpwdUNIX1=`expr $lastpwdMS / 10000000 - 1644473600`
lastpwdUNIX=`expr $lastpwdUNIX1 - 10000000000`
todayUNIX=`date +%s`
diffDays1=`expr $todayUNIX - $lastpwdUNIX`
diffDays=`expr $diffDays1 / 86400`
daysRemainingON=`expr $pwdPolicy - $diffDays`
echo "$daysRemainingON"

# Check if off Domain
ping -c 1 10.252.32.33 &>/dev/null || ping -c 1 10.252.32.32 &>/dev/null || ping -c 1 10.252.32.42 &>/dev/null || ping -c 1 10.252.32.43 &>/dev/null
	elif [ $? = 0 ]; then
	exit
else
lastpwdMS=`dscl . -read /Users/$CONSOLE_USER | grep -i SMBPasswordLastSet`
lastpwdUNIX1=`expr "$lastpwdMS" / 10000000 - 1644473600`
lastpwdUNIX=`expr "$lastpwdUNIX1" - 10000000000`
todayUNIX=`date +%s`
diffDays1=`expr "$todayUNIX" - "$lastpwdUNIX"`
diffDays=`expr "$diffDays1" / 86400`
daysRemainingOFF=`expr "$pwdPolicy" - "$diffDays"`
echo "$daysRemainingOFF"
fi

# Notify user if the expiry numbers are different
if [ $daysRemainingON ! -eq $daysRemainingOFF ]; then

#Notification to state fix in progress
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Missmatch" -description "
We've noticed that Hogarth email password is different from the one recorded in FileVault.

Please input your password in the next field." -button1 'OK' > /dev/null 2>&1 &

UsersPW=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter your Hogarth email password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`

#Notification to state fix in progress
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -title "Fixing password for FileVault" -description  "Please do not interrupt or power off during this process."&

# Expect commands
fdesetup remove -user "$CONSOLE_USER"
expect <<- DONE
set timeout -1
spawn fdesetup add -usertoadd "$CONSOLE_USER"
expect "Enter the user name:"
send "locadmin\r"
expect "Enter the password for user 'locadmin':"
send "$AdminPW\r"
expect "Enter the password for the added user '$CONSOLE_USER':"
send "$UsersPW\r"
expect EOF
DONE

ps axco pid,command | grep jamfHelper | awk '{ print $1; }' | xargs kill -9


EOF

/usr/sbin/chown root:wheel /usr/local/scripts/expectFDE.sh
/bin/chmod 710 /usr/local/scripts/expectFDE.sh
