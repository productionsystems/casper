#!/bin/bash

#set -x

log() {
    echo "$1"
    /usr/bin/logger -t "Zoom Installer:" "$1"
}
log "Installing Zoom.app"

CurrentVer=$(/usr/bin/curl --head https://zoom.us/client/latest/ZoomInstallerIT.pkg | grep Location | awk -v FS="(prod/|/Zoom)" '{print $2}')

LocalVer=$(defaults read /Applications/zoom.us.app/Contents/Info.plist | grep CFBundleShortVersionString | awk '{print $3}' | cut -d'"' -f2)

packageDownloadUrl="https://zoom.us/client/latest/ZoomInstallerIT.pkg"

if [ "$CurrentVer" =  "$LocalVer" ]; then
	echo "Zoom is already the latest version"
	exit
fi

log "Downloading Zoom.pkg..."
/usr/bin/curl -L "$packageDownloadUrl" -o /tmp/zoom.pkg
if [ $? -ne 0 ]; then
    log "curl error: The package did not successfully download"; exit 1
fi



log "Installing Zoom.app..."
/usr/sbin/installer -pkg "/tmp/zoom.pkg" -target /
if [ $? -ne 0 ]; then
    log "installer error: The package did not successfully install"; exit 1
fi