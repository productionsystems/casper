#!/bin/bash

#This will copy all package data to Akamai

rsync -rauv -e "ssh -2 -i /root/.ssh/jamfpro_netstorage_key" --delete-excluded /opt/hogarth/CasperShareIXLL2/Packages/ sshacs@jamfpro.upload.akamai.com:/596787/  > /var/log/Akamai"_rsync-Log_"$(date +%Y%m%d).txt

echo "Rsync London DP to Akamai Completed" | mutt -a /var/log/Akamai"_rsync-Log_"$(date +%Y%m%d).txt -s "rsync London DP to Akamai Completed" -- jason.flory@hogarthww.com, 
#frank.paternoster@hogarthww.com

#Find logs older than 5 days and delete
find /var/log/Akamai* -mtime +1 -exec rm {} \;

#exit 0