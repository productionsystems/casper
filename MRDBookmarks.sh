#!/bin/bash


CUSER=`stat -f "%Su" /dev/console`

rm -f /Users/$CUSER/Library/Containers/com.microsoft.rdc.mac/Data/Library/Preferences/com.microsoft.rdc.mac.plist

cat <<EOF > /Users/$CUSER/Library/Containers/com.microsoft.rdc.mac/Data/Library/Preferences/com.microsoft.rdc.mac.plist 

{
    "bookmarklist.expansionStates" =     {
        GENEREAL = 1;
    };
    "bookmarkorder.ids" =     (
        "{451db012-bbfa-4763-a84a-9039c72c93f3}",
        "{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}"
    );
    "bookmarks.bookmark.{451db012-bbfa-4763-a84a-9039c72c93f3}.fullscreen" = 0;
    "bookmarks.bookmark.{451db012-bbfa-4763-a84a-9039c72c93f3}.fullscreenMode" = "@Variant(";
    "bookmarks.bookmark.{451db012-bbfa-4763-a84a-9039c72c93f3}.hostname" = 164studio;
    "bookmarks.bookmark.{451db012-bbfa-4763-a84a-9039c72c93f3}.label" = 164Studio;
    "bookmarks.bookmark.{451db012-bbfa-4763-a84a-9039c72c93f3}.resolution" = "@Size(1600 900)";
    "bookmarks.bookmark.{451db012-bbfa-4763-a84a-9039c72c93f3}.username" = "";
    "bookmarks.bookmark.{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}.fullscreen" = 0;
    "bookmarks.bookmark.{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}.fullscreenMode" = "@Variant(";
    "bookmarks.bookmark.{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}.hostname" = "HGSL-RIP003";
    "bookmarks.bookmark.{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}.label" = "HGSL GMG RIP";
    "bookmarks.bookmark.{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}.resolution" = "@Size(1600 900)";
    "bookmarks.bookmark.{ff0c462c-0af0-47fd-a62d-ec6f6b5e064c}.username" = "HOGARTHWW\\\\gmgsystem";
    "connectWindow.geometry" = <01d9d0cb 00010000 00000179 00000017 0000037e 00000270 00000179 0000002d 0000037e 00000270 00000000 0000>;
    "connectWindow.windowState" = <000000ff 00000000 fd000000 00000002 06000002 44000000 04000000 04000000 08000000 08fc0000 00010000 00020000 00010000 000e0074 006f006f 006c0042 00610072 01000000 00ffffff ff000000 00000000 00>;
    "preferences.resolutions" =     (
        "@Size(640 480)",
        "@Size(800 600)",
        "@Size(1024 768)",
        "@Size(1280 720)",
        "@Size(1280 1024)",
        "@Size(1600 900)",
        "@Size(1920 1080)",
        "@Size(1920 1200)"
    );
    "show_whats_new_dialog" = 0;
    "stored_version_number" = "8.0.25073";
}
EOF

/usr/sbin/chown $CUSER:"HOGARTHWW\Domain Users" /Users/$CUSER/Library/Containers/com.microsoft.rdc.mac/Data/Library/Preferences/com.microsoft.rdc.mac.plist
/bin/chmod 755 /Users/$CUSER/Library/Containers/com.microsoft.rdc.mac/Data/Library/Preferences/com.microsoft.rdc.mac.plist