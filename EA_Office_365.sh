#!/bin/bash

app_path=/Applications/Microsoft\ Outlook.app

if [[ $app_path ]]; then
	Version=`defaults read /Applications/Microsoft\ Outlook.app/Contents/Frameworks/MicrosoftOffice.framework/Versions/A/Resources/Info.plist CFBundleVersion`
fi

if [[ -e $app_path ]]; then
	last_used=`mdls "$app_path" | grep kMDItemLastUsedDate | awk '{ $1 = $1 } { print }' | cut -c 23-41`

echo "<result>Office 2016 Installed V$Version | Last used $last_used</result>"

fi



