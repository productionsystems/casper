#!/bin/bash

set -x


log() {
    echo "$1"
    /usr/bin/logger -t "Wacom Installer:" "$1"
}
log "Installing Wacom.app"

CurrentVer=$(curl -Ls https://www.jamf.com/jamf-nation/third-party-products/674/wacom-driver | grep "Current Version" | awk '{print $3}' | rev | cut -c -8 | rev)

LocalVer=$(defaults read /Applications/Wacom\ Tablet.localized/Wacom\ Desktop\ Center.app/Contents/Info.plist | grep CFBundleShortVersionString | awk '{print $3}' | cut -d'"' -f2)

packageDownloadUrl=`curl -Ls https://www.jamf.com/jamf-nation/third-party-products/674/wacom-driver | grep "cdn.wacom.com" | tail -1 | awk '{print $1}'`

packagename=`echo "$packageDownloadUrl" | cut -f9 -d "/"`

echo "$packagename"

if [ "$CurrentVer" =  "$LocalVer" ]; then
	echo "Wacom is already the latest version"
	exit
fi

log "Downloading Wacom.pkg..."
/usr/bin/curl -L "$packageDownloadUrl" -o /private/tmp/$packagename
if [ $? -ne 0 ]; then
    log "curl error: The package did not successfully download"; exit 1
fi

/usr/bin/hdiutil attach /private/tmp/$packagename -nobrowse -quiet
/usr/sbin/installer -pkg /Volumes/WacomTablet/Install\ Wacom\ Tablet.pkg -target / > /dev/null
/usr/bin/hdiutil detach /Volumes/WacomTablet

rm -rf /private/tmp/$packagename
