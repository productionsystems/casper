#!/bin/bash
#v1.0 Jason Flory 23/08/16

set -x 

# API call for machine data required - EMail address.
JSSserver=https://casper:8443 # ip or domain, port is assumed to be default
JSSapiuser=apiuser # this user requires TBC access privs in API settings on JSS 
JSSapipw=PnZ-Nv5-E2P-CNR
UDID=`system_profiler SPHardwareDataType | awk '/Hardware/ {print $3}' | grep -`

# old password, api call
curl -H "Accept: application/xml" -s -o /tmp/all.xml -k -u "$JSSapiuser":"$JSSapipw" "$JSSserver"/JSSResource/computers/udid/"$UDID"

xmllint --format /tmp/all.xml > /tmp/all2.xml
mv /tmp/all2.xml /tmp/all.xml

char="@"
oldemail=`grep "<email_address>" /tmp/all.xml | sed 's*<email_address>**g' | sed 's*</email_address>**g'`
NOA=`grep -o "$char" <<< "$oldemail" | wc -l`

if [ $NOA = "2" ]; then
	newemail=`echo $oldemail | sed 's/.\{14\}$//'`
	echo "Email needs changing"
	jamf recon -email $newemail
else
	if [ $NOA = !2 ]; then
		echo "Email OK"
	exit 0	
	fi
fi

