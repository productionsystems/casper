#!/bin/bash

############################################
# Add FV for new & existing users at login #
############################################

# Create launchd to add user at login 

cat <<\EOF > /Library/LaunchAgents/com.Hogarthww.expectFDE.sh.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>com.Hogarthww.expectFDE</string>
		<key>ProgramArguments</key>
		<array>
			<string>/bin/sh</string>
			<string>/usr/local/scripts/expectFDE.sh</string>
		</array>
		<key>RunAtLoad</key>
		</dict>
	</dict>
</plist>
EOF

# Set permissions
/usr/sbin/chown root:wheel /Library/LaunchAgents/com.Hogarthww.expectFDE.sh.plist
/bin/chmod 644 /Library/LaunchAgents/com.Hogarthww.expectFDE.sh.plist
launchctl load com.Hogarthww.expectFDE.sh.plist

# Create script for user activation
cat <<\EOF > /usr/local/scripts/expectFDE.sh
#!/bin/bash

#set -x
AdminPW="$4"
userName=`stat -f%Su /dev/console`

UsersPW=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter your Hogarth email password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`

#Notification to state fix in progress
/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -title "Fixing password for FileVault" -description  "Please do not interrupt or power off during this process."&

### S: We need a check to see if the users has already been added

## This first user check sees if the logged in account is already authorized with FileVault 2
userCheck=$(sudo fdesetup list | grep -F $userName)
if [ "$userCheck" != "" ]; then
echo "This user is already added to the FileVault 2 list."
exit 1
fi

# Expect commands

expect <<- DONE
set timeout -1
spawn fdesetup add -usertoadd "$CONSOLE_USER"
expect "Enter the user name:"
send "locadmin\r"
expect "Enter the password for user 'locadmin':"
send "$AdminPW\r"
expect "Enter the password for the added user '$CONSOLE_USER':"
send "$UsersPW\r"
expect EOF
DONE

ps axco pid,command | grep jamfHelper | awk '{ print $1; }' | xargs kill -9
fi

EOF

/usr/sbin/chown root:wheel /usr/local/scripts/expectFDE.sh
/bin/chmod 710 /usr/local/scripts/expectFDE.sh
