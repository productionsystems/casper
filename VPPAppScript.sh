#!/bin/bash

# Do nothing if open
# when inactive remove and place stub for SG ref
# scope to stub, for "Mac Store Apps" ref. Q: how long before the MSA has been installed?
# Might be better to only run when the user is logged out?!?!

# See if app is open, exit if active

if [ ! -d /usr/local/userdata ]; then
	mkdir /usr/local/userdata/
fi

if [ ! -f /usr/local/userdata/Keynote_removed.txt ]; then
	if [ `ps aux | grep -i Keynote.app | awk '{print $2;}'` = 0 ]; then
		echo "Keynote active"
	exit
	else
		rm -rf /Applications/Keynote.app | touch /usr/local/userdata/Keynote_removed.txt
fi
fi	

if [ ! -f /usr/local/userdata/Numbers_removed.txt ]; then
	if [ `ps aux | grep -i Numbers.app | awk '{print $2;}'` = 0 ]; then
		echo "Numbers active"
	exit
	else
	rm -rf /Applications/Numbers.app | touch /usr/local/userdata/Numbers_removed.txt
	fi
fi	

if [ ! -f /usr/local/userdata/Pages_removed.txt ]; then	
	if [ `ps aux | grep -i Pages.app | awk '{print $2;}'` = 0 ]; then
		echo "Pages active"
	exit
	else
	rm -rf /Applications/Pages.app | touch /usr/local/userdata/Pages_removed.txt
	fi
fi	

