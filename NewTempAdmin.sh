#!/bin/bash

function TempAdmin {
    
    set +x
    
if [ $(dscl . -read /Users/admin | grep RecordName: | awk '{print $2}') == "admin" ]; then
	echo "Local Admin account is active"
	exit 0
else
	/usr/local/jamf/bin/jamf policy -event CreateAdminAccount
	/usr/local/jamf/bin/jamf displayMessage -message "Admin access is avalible for five minutes"
	date
	secs=$((5 * 60))
while [ $secs -gt 0 ]; do
	   	echo -ne "$secs\033[0K\r"
	   	sleep 1
	   	: $((secs--))
	   	if [ $secs == 60 ]; then
	   	    /usr/local/jamf/bin/jamf displayMessage -message "Admin access has one minute remaining"
			date
	   	fi
        if [ $secs == 0 ]; then
	        /usr/local/jamf/bin/jamf deleteAccount -username admin -deleteHomeDirectory
	        echo "Admin account deleted"
	        /usr/local/jamf/bin/jamf displayMessage -message "Admin access has now been disabled"
			date
   fi   
done
fi
}

TempAdmin &