#!/bin/bash

# Ref: https://www.jamf.com/jamf-nation/discussions/17152/assign-user-location

# Info:
# Does this need to run automatically or be part of the Self Service 




jamf_binary="/usr/local/jamf/bin/jamf"

#Working directory for script to reference resources
declare -x install_dir=`dirname $0`

#Enter in the URL of the JSS we are are pulling info from. (NOTE: We will need https:// and :8443 (or whatever port you use). Example: https://jss.company.com:8443 )
jssURL="https://casper:8443"

#Enter in a username and password that has the correct permissions to the JSS API for what data we need. Read Access to Buildings, Departments, Sites and Users.
jssUser="apiuser"
jssPass="PnZ-Nv5-E2P-CNR"

#Default file path we will use to place XML file for JSS API submission
#Feel free to edit these to the location of your choice
declare -x FirstbootXML="/tmp/location.xml"
declare -x UsernameTxtFile="/tmp/user.txt"
declare -x DeptTxtFile="/tmp/dept.txt"
declare -x BuildingTxtFile="/tmp/building.txt"


#Name for XML files pulled from JSS
declare -x BuildingXML="/tmp/buildings.xml"
declare -x DeptXML="/tmp/departments.xml"
declare -x UserXML="/tmp/users.xml"
declare -x SiteXML="/tmp/sites.xml"

#Create directory for firstboot upload
#Feel free to edit these to the location of your choice
#/bin/mkdir -p "$3/Library/Scripts/Firstboot/"


##You shouldn't have to edit anything below. Proceed with caution.

#Get list of departments from JSS
/usr/bin/curl -k -v -u "$jssUser":"$jssPass" "$jssURL"/JSSResource/departments -X GET -o "$DeptXML"

#Get list of users from JSS
/usr/bin/curl -k -v -u "$jssUser":"$jssPass" "$jssURL"/JSSResource/users -X GET -o "$UserXML"

#Get list of buildings from JSS
/usr/bin/curl -k -v -u "$jssUser":"$jssPass" "$jssURL"/JSSResource/buildings -X GET -o "$BuildingXML"

#Get list of buildings from JSS
/usr/bin/curl -k -v -u "$jssUser":"$jssPass" "$jssURL"/JSSResource/sites -X GET -o "$SiteXML"


######################
######################

# Branded message to users

`/usr/bin/osascript << EOT
tell application "Finder"
activate
display dialog "As part of Hogarths user enrolment policy you will now be asked a few questions to start the configuration of your new machine.

Please ensure you complete this process, as failure to do so will result in none compliance with Hogarths policies." buttons {"Continue"} with icon {"/Library/Application Support/Hogarth/hogarth.jpeg"}
-- choose from list cancel button returns false, not -128, as do other choose syntax do.
#if Reagion is false then error number -128 -- user canceled
end tell
return #(posix path of Reagion)
EOT
exit`

######################
######################

# Get building info from the JSS

#install_dir=`dirname $0`
#BuildingXML="/tmp/buildings.xml"
tmpBuildingsFile="/tmp/BuildingsList.txt"

#Generate a variable from information in XML to use in an array
Buildings=$(xpath "$BuildingXML" '/buildings/building/name' 2>&1| sed 's/--\ NODE\ --//g' | sed 's/<name>//g' | sed 's/<\/name>//g' | sed 's/Found.*nodes://g' | sort)

IFS=$'\n'

#Iterate through XML array
for i in $Buildings; do
    BuildingsArray+=($i)
    echo $i >> "$tmpBuildingsFile"
    chmod 777 "$tmpBuildingsFile"
done

building=`/usr/bin/osascript <<EOT
    tell application "System Events"
    with timeout of 43200 seconds
    activate
--  Create an empty list called BuildingsList
    set BuildingsList to {}

--  Populate list with contents read from a file
    set BuildingsFile to paragraphs of (read POSIX file "$tmpBuildingsFile")

--  Iterate through each line in file to add to BuildingsList
    repeat with i in BuildingsFile
        if length of i is greater than 0 then
            copy i to the end of BuildingsList
        end if
    end repeat

--  For testing to make sure the right number of items are counted in the list
--  display dialog count of BuildingsList
    choose from list BuildingsList with title "Building List" with prompt "Please select a building to associate to computer:"
    end timeout
    end tell
EOT`

######################
######################

# Request users Role type from list

if [ -f "$tmpBuildingsFile" ]; then
    rm -f "$tmpBuildingsFile"
fi

Role=`/usr/bin/osascript << EOT
tell application "Finder"
activate
set Role to (choose from list {"Accounts", "Broadcast", "BroadcastSAN", "Digital", "Computer Graphics", "Facilities", "Finance", "HR", "Management", "Print Non-Production", "Print PM", "Print Production", "Producers", "Traffic", "Transcreation", "Administrative", "Elements", "Element Developers", "Element General", "Subtitling"} with prompt "Please select your role type, this will define the applications you get" default items "OK" OK button name {"OK"})
--- choose from list cancel button returns false, not -128, as do other choose syntax do.
if Role is false then error number -128 -- user canceled
end tell
return (posix path of Role)
EOT
exit`

# Define Role code
Role=$(echo $Role | sed 's/^.//')
SiteCode=`echo $building | sed 's/.*(//' | sed s'/.$//'`

if [[ $Role = "Accounts" ]]; then
	RoleCode=ac
elif [[ $Role = "Broadcast" ]]; then
	RoleCode=br
elif [[ $Role = "Digital" ]]; then
	RoleCode=di
elif [[ $Role = "Facilities" ]]; then
	RoleCode=fa
elif [[ $Role = "Finance" ]]; then
	RoleCode='fi'
elif [[ $Role = "HR" ]]; then
	RoleCode=hr
elif [[ $Role = "Management" ]]; then
	RoleCode=ma
elif [[ $Role = "Print Non-Production" ]]; then
	RoleCode=pn
elif [[ $Role = "Print PM" ]]; then
	RoleCode=pm
elif [[ $Role = "Print Production" ]]; then
	RoleCode=pp
elif [[ $Role = "Producers" ]]; then
	RoleCode=pr
elif [[ $Role = "Traffic" ]]; then
	RoleCode=tf
elif [[ $Role = "Transcreation" ]]; then
	RoleCode=tr
elif [[ $Role = "Administrative" ]]; then
	RoleCode=ad
elif [[ $Role = "Computer Graphics" ]]; then
	RoleCode=cg
elif [[ $Role = "Elements" ]]; then
	RoleCode=el
elif [[ $Role = "Subtitling" ]]; then
	RoleCode=st
elif [[ $Role = "Element Developers" ]]; then
	RoleCode=ed
elif [[ $Role = "Element General" ]]; then
	RoleCode=eg
fi 


# Asset number will not work need other method of identification, so potentially we can use the HW UUID - migh need this for external provisioning
#UUIDCode=`ioreg -rd1 -c IOPlatformExpertDevice | grep -E '(UUID)' | sed 's/.*\(.......\)/\1/' | sed 's/..$//'`

AssetNo=`osascript -e 'Tell application "System Events" to display dialog "Please enter the 5 digit asset number:" default answer ""' -e 'text returned of result'`

# define hardware type
hw=`system_profiler SPHardwareDataType | grep "Model Name" | awk '{print $3}'`

if [[ $hw = MacBook ]]; then
	hwtype=mb
else
	hwtype=ws
fi

NEWHOSTNAME=`echo "$SiteCode""ap""$hwtype""$RoleCode""$AssetNo" | tr '[:upper:]' '[:lower:]'`

scutil --set ComputerName $NEWHOSTNAME
scutil --set LocalHostName $NEWHOSTNAME
scutil --set HostName $NEWHOSTNAME
dscacheutil -flushcache

### Inventory information
echo "This is the Hostname for the machine:$NEWHOSTNAME" >> /var/hogarth/buildinfo.log

# define the date the machine was built
echo "This machine was built on `date`" >> /var/hogarth/buildinfo.log

# define were the machine is located
echo "This machine is located in $building" >> /var/hogarth/buildinfo.log

# define asset number
echo "This is the asset number of this machine: $AssetNo" >> /var/hogarth/buildinfo.log

# get users AD name from engineer
endUsername=`osascript -e 'Tell application "System Events" to display dialog "Please enter the users AD username:" default answer ""' -e 'text returned of result'`

# get users full name from engineer
realname=`osascript -e 'Tell application "System Events" to display dialog "Please enter the users full name:" default answer ""' -e 'text returned of result'`

# get users email address from engineer
email=`osascript -e 'Tell application "System Events" to display dialog "Please enter the users email address:" default answer ""' -e 'text returned of result'`

# define which engineer built the machine
engineer=`osascript -e 'Tell application "System Events" to display dialog "Engineer, please state your full name:" default answer ""' -e 'text returned of result'`
echo "This machine was built by: $engineer " >> /var/hogarth/buildinfo.log

$jamf_binary recon -endUsername $endUsername -realname $realname -email $email

$jamf_binary policy -id 18679  #reference dep setup script!

exit 0