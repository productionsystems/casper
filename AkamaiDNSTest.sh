#!/bin/bash

set -x

AkamaiDNS=AkamaiDNS=`dig a1899.d.akamai.net | grep a1899.d.akamai.net. | head -2 | tail -1 | awk '{print $5}' | cut -d"." -f1-2`

while [ ! `echo $AkamaiDNS` = 104.124 ]; do
	AkamaiDNS=`dig a1899.d.akamai.net | grep a1899.d.akamai.net. | head -2 | tail -1 | awk '{print $5}' | cut -d"." -f1-2` 
sleep 1

if [ `echo $AkamaiDNS` = 104.124 ]; then
	date
		rsync -rltgoDuv -e "ssh -2 -i /root/.ssh/jamfpro_netstorage_key" --delete-excluded sshacs@jamfpro.upload.akamai.com:/596787/ /opt/hogarth/JamfProDistributionHGUG/Packages/ &
fi
done

