#!/usr/bin/env bash

###########################################################
# create bootable tails usb-stick https://tails.boum.org/ #
# !!! Read the comments !!! CHANGE <diskN> placeholder    #
###########################################################

set -x

# get iso
/usr/bin/curl -s https://mirrors.edge.kernel.org/tails/stable/tails-amd64-3.8/tails-amd64-3.8.iso -o /tmp/tails-amd64-3.8.iso

# determine usb stick disk - <diskN> ex. disk3
# replace the following <diskN> with the corresponding disk, ie. disk3
diskutil list

# Unmount of all volumes on <diskN> was successful
# replace the following <diskN> with the corresponding disk, ie. disk3
# diskutil unmountDisk /dev/disk3
diskutil unmountDisk /dev/disk12

# make iso bootable, original file unchanged
cp /tmp/tails-amd64-3.8.iso /tmp/tails-amd64-3.8.bootable.iso
cd /Users/jasonflory/Desktop/Build_data/Git/ ; perl isohybrid.pl /tmp/tails-amd64-3.8.bootable.iso

# write bootable iso to usb stick
# replace the following <diskN> with the corresponding disk, ie. disk3
# ie. sudo dd if=tails-i386-0.21.bootable.iso  of=/dev/disk3 bs=16m
sudo dd if=/tmp/tails-amd64-3.8.bootable.iso  of=/dev/disk12 bs=16m