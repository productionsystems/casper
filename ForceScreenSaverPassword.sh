#!/bin/bash

CUSER=`stat -f "%Su" /dev/console`

cat <<EOF > /usr/local/scripts/setscreensaveroptions.sh
#!/bin/bash

#turn it on
defaults write /Users/&CUSER/Library/Preferences/com.apple.screensaver askForPassword -bool true
#set the delay in seconds, 0 = immediate
defaults write /Users/&CUSER/Library/Preferences/com.apple.screensaver askForPasswordDelay 0
#set the screen saver delay in seconds, 300 = 5 minutes
defaults write /Users/&CUSER/Library/Preferences/com.apple.screensaver idleTime 300
#set hot corners bottom right
defaults write /Users/&CUSER/Library/Preferences/com.apple.dock "wvous-br-corner" -int 5
defaults write /Users/&CUSER/Library/Preferences/com.apple.dock "wvous-br-modifier" -int 0
EOF

cat <<EOF > /Library/LaunchAgents/con.hogarthww.setscreensaveroptions.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>con.hogarthww.setscreensaveroptions</string>
  <key>ProgramArguments</key>
  <array>
    <string>/usr/local/scripts/setscreensaveroptions.sh</string>
  </array>
 <key>KeepAlive</key>
<dict>
    <key>SuccessfulExit</key>
    <false/>
</dict>
</dict>
</plist>

EOF

/usr/sbin/chown root:wheel /usr/local/scripts/setscreensaveroptions.sh
/bin/chmod 755 /usr/local/scripts/setscreensaveroptions.sh

/usr/sbin/chown root:wheel /Library/LaunchAgents/con.hogarthww.setscreensaveroptions.plist
/bin/chmod 644 /Library/LaunchAgents/con.hogarthww.setscreensaveroptions.plist