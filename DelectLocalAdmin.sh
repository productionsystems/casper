#!/bin/bash

#People with authorised local admin account swould need to be excluded
#IBM account may need to be excluded to allow access to TeamViewer.

if [ /Users/depadmin ]; then

cat <<EOF > /Library/LaunchDaemons/net.Hogarthww.DeleteAdminAccount.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>net.Hogarthww.DeleteAdminAccount</string>
	<key>ProgramArguments</key>
	<array>
		<string>/bin/sh</string>
		<string>/usr/local/scripts/DeleteAdminAccount.sh</string>
	</array>
	<key>RunAtLoad</key>
	<true/>
</dict>
</plist>
EOF

cat <<EOF > /usr/local/scripts/DeleteAdminAccount.sh
#!/bin/bash

#current user to be excluded
CurrentUser=`ls -la /dev/console | cut -d " " -f 4`

while read useraccount; do
    /usr/sbin/dseditgroup -o edit -d $useraccount -t user admin
done < <(dscl . list /Users UniqueID | awk '$2 <= 503 {print $1}' | grep -v _ | grep -v root | grep -v daemon | grep -v nobody | grep -v $CurrentUser)

EOF

/usr/sbin/chown root:wheel /Library/LaunchDaemons/net.Hogarthww.DeleteAdminAccount.plist
/bin/chmod 644 /Library/LaunchDaemons/net.Hogarthww.DeleteAdminAccount.plist

/bin/chmod +x /usr/local/scripts/DeleteAdminAccount.sh

fi

