#!/bin/bash

Nuke=`echo /Applications/Nuke* | cut -d '/' -f 3`

if [[ -f /Applications/"$Nuke"/"$Nuke".app/Contents/Info.plist ]]; then
NukeVersion=`defaults read /Applications/"$Nuke"/"$Nuke".app/Contents/Info.plist CFBundleVersion`
else
NukeVersion="Nuke Not Installed"
fi

echo "<result>$NukeVersion</result>"

