#!/bin/bash

# Test workflow for FV Password script.
# Test as locadmin and AD user

set -x

AdminPW="Mash2bash!"
CONSOLE_USER=`who |grep console| awk '{print $1}'`

if [ $CONSOLE_USER = locadmin ]; then
	#Notification to state fix in progress
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "This tool will sync the users Hogarth password with FileVault.

	Please input the users AD name and password in the next two fields." -button1 'OK' > /dev/null 2>&1
	
	ADUser=`osascript -e 'Tell application "System Events" to display dialog "Please enter the users AD Credentials:" default answer ""' -e 'text returned of result'`

	while [[ true ]]; 
	do
		UsersPW1=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter the users password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
		UsersPW2=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please confirm the users password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
			if [ $UsersPW1 = $UsersPW2 ]; then
				UsersPW=`echo $UsersPW2`
				break
			else
				/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "Your passwords do not match, please try again" -button1 'OK' > /dev/null 2>&1
			fi 
	done
else

	#Notification to state fix in progress
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "This tool will sync your Hogarth password with FileVault.

	Please input your password in the next field." -button1 'OK' > /dev/null 2>&1

	while [[ true ]]; 
	do
		UsersPW1=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please enter your Hogarth email password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
		UsersPW2=`/usr/bin/osascript -e 'Tell application "System Events" to display dialog "Please confirm your Hogarth email password :" default answer "" with title "Login Password" with text buttons {"Ok"} default button 1 with hidden answer' -e 'text returned of result'`
			if [ $UsersPW1 = $UsersPW2 ]; then
				UsersPW=`echo $UsersPW2`
				break
			else
				/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "FileVault Password Fix" -description "Your passwords do not match, please try again" -button1 'OK' > /dev/null 2>&1
			fi 
		done
fi