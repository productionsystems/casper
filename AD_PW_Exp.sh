#!/bin/bash

#set -x

################################################
# Created by: J Flory 20/01/19
# Ref: https://www.jamf.com/jamf-nation/discussions/18588/ad-password-expiration-as-jss-extension-attribute-no-ad-bind-necessary#responseChild122772
# To Do:
# Get days to exp on network - done
# Record this into a file for offline calcualtion - done
# Set to run once a day locally, launchd to run once every 24hrs - done
# While off network the passwordexp.txt need to count down daily - done
# To be scoped to all FV laptops

# Create launchd for daily timer.

cat <<\EOF > /Library/LaunchDaemons/net.Hogarthww.PWExpDailyCheck.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>net.Hogarthww.PWExpDailyCheck</string>
		<key>ProgramArguments</key>
		<array>
			<string>/bin/sh</string>
			<string>/usr/local/scripts/ADPWCheck.sh</string>
		</array>
		<key>StartCalendarInterval</key>
		<dict>
			<key>Hour</key>
			<integer>12</integer>
		</dict>
	</dict>
</plist>
EOF

/usr/sbin/chown root:wheel /Library/LaunchDaemons/net.Hogarthww.PWExpDailyCheck.plist
/bin/chmod 644 /Library/LaunchDaemons/net.Hogarthww.PWExpDailyCheck.plist
launchctl load net.Hogarthww.PWExpDailyCheck.plist


cat <<\EOF > /usr/local/scripts/ADPWCheck.sh
#!/bin/bash

set -x

# Create directory
if [[ ! -d /Users/Shared/Hogarth/ ]]; then
	mkdir /Users/Shared/Hogarth/
	chflags hidden /Users/Shared/Hogarth/
	chmod 755 /Users/Shared/Hogarth/
fi

# Check if on domain
ping -c 1 10.252.32.33 &>/dev/null || ping -c 1 10.252.32.32 &>/dev/null || ping -c 1 10.252.32.42 &>/dev/null || ping -c 1 10.252.32.43 &>/dev/null
	if [ $? = 0 ]; then
	
	# Define Variable
	ldap_server=10.252.32.33    # IP address of a dc within your org that allows directory lookups
	ldap_account=casper.jss   # AD account with limited privs, used to query the active directory server using ldapsearch
	ldap_password=Jasper99  # Password for the ldap_account user above
	forest_domain=hogarthww.prv  # i.e. forest.yourorg.com
	dist_name='OU=Hogarthww,DC=hogarthww,DC=prv'  # The distinguished name of your domain and forest in AD, quoted b/c of the characters; i.e. 'dc=forest,dc=yourorg,dc=com'
	pwdPolicy=90  # Days to password expiration since last set i.e.: 90
	
	if [[ ! -f /Users/Shared/Hogarth/PWexpCount.txt ]]; then
		touch /Users/Shared/Hogarth/PWexpCount.txt
	fi
	
    # Get name of user the computer was assigned to
	usernameINAD="`who |grep console| awk '{print $1}' 2> /dev/null`"
	
	# LDAP lookup for pwdLastSet and then a little math to arrive at days until expiry. 
	lastpwdMS=`ldapsearch -x -H ldap://$ldap_server -D $ldap_account@$forest_domain -w "$ldap_password" -b "$dist_name" -L "anr=$usernameINAD" pwdLastSet | /usr/bin/awk '/pwdLastSet:/{print $2}'`
    lastpwdUNIX1=`expr $lastpwdMS / 10000000 - 1644473600`
    lastpwdUNIX=`expr $lastpwdUNIX1 - 10000000000`
    todayUNIX=`date +%s`
    diffDays1=`expr $todayUNIX - $lastpwdUNIX`
    diffDays=`expr $diffDays1 / 86400`
    daysRemaining=`expr $pwdPolicy - $diffDays`
	echo $daysRemaining > /Users/Shared/Hogarth/PWexpCount.txt
fi


# Check if off Domain
ping -c 1 10.252.32.33 &>/dev/null || ping -c 1 10.252.32.32 &>/dev/null || ping -c 1 10.252.32.42 &>/dev/null || ping -c 1 10.252.32.43 &>/dev/null
	if [ $? = 0 ]; then 
	exit
else
	echo "can't see the domain, running counter"		
	# File to count down
	PWCount=`awk '{$1-=1}1' OFS='\t' /Users/Shared/Hogarth/PWexpCount.txt`
	echo "$PWCount" > /Users/Shared/Hogarth/PWexpCount.txt
fi

PWxepCount=`/bin/cat /Users/Shared/Hogarth/PWexpCount.txt`
if [ `echo "$PWxepCount"` -lt "15" && echo "$PWxepCount"` -gt "4" ]; then

	# Notification
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "Password Expiration Notification." -description "
	Your password is due to expire in "$PWxepCount" days, please change it as soon as possible. 
	
	To ensure that FileVault is updated with your new password, open Systems Preferences "/" Users and change the password there." -button1 'OK' &
	open /Applications/System\ Preferences.app
else
	exit 0
fi

PWxepCount=`/bin/cat /Users/Shared/Hogarth/PWexpCount.txt`
if [ `echo "$PWxepCount"` -lt "5" ]; then

	# Notification
	/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType utility -icon /Applications/Utilities/Keychain\ Access.app/Contents/Resources/AppIcon.icns -heading "Password Expiration Notification." -description "
	Your password is about to expire in "$PWxepCount" days, please change it now! 
	
	To ensure that FileVault is updated with your new password, open Systems Preferences "/" Users and change the password there." -button1 'OK' &
	open /Applications/System\ Preferences.app
else
	exit 0
fi

exit 0

EOF

/usr/sbin/chown root:wheel /usr/local/scripts/ADPWCheck.sh
/bin/chmod 755 /usr/local/scripts/ADPWCheck.sh

exit 0