#!/bin/bash

#This will copy all package data to Akamai

wput -vN /opt/hogarth/CasperShareIXLL2/Packages/ ftp://jamfpro:Qualified5@jamfpro.upload.akamai.com/596787/ > /var/log/Akamai"_wput-Log_"$(date +%Y%m%d).txt
echo "WPUT London DP to Akamai Completed" | mutt -a /var/log/Akamai"_wput-Log_"$(date +%Y%m%d).txt -s "wput London DP to Akamai Completed" -- jason.flory@hogarthww.com

#Find logs older than 5 days and delete
find /var/log/Akamai* -mtime +1 -exec rm {} \;

exit 0

### config file
casperadmin@ixllvmva26:/var/log$ cat /etc/exim4/update-exim4.conf.conf
# /etc/exim4/update-exim4.conf.conf
#
# Edit this file and /etc/mailname by hand and execute update-exim4.conf
# yourself or use 'dpkg-reconfigure exim4-config'
#
# Please note that this is _not_ a dpkg-conffile and that automatic changes
# to this file might happen. The code handling this will honor your local
# changes, so this is usually fine, but will break local schemes that mess
# around with multiple versions of the file.
#
# update-exim4.conf uses this file to determine variable values to generate
# exim configuration macros for the configuration file.
#
# Most settings found in here do have corresponding questions in the
# Debconf configuration, but not all of them.
#
# This is a Debian specific file

dc_eximconfig_configtype='smarthost'
dc_other_hostnames='ixllvmva26.hogarthww.prv'
dc_local_interfaces='127.0.0.1'
dc_readhost='ixllvmva26.hogarthww.prv'
dc_relay_domains=''
dc_minimaldns='false'
dc_relay_nets='localhost.localdomain;'
dc_smarthost='relay.hogarthww.com'
CFILEMODE='644'
dc_use_split_config='false'
dc_hide_mailname='true'
dc_mailname_in_oh='true'
dc_localdelivery='maildir_home'


## process
Install mutt
Install exim4
config update-exim4.conf.conf